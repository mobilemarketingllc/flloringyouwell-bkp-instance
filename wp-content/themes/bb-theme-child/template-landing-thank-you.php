<?php

/*
Template Name: Landing Page Coupon Thank You 

*/

//Retailer Landing Page thank you after coupon submitted

 $pagetype =  $_GET['pagetype'];

if($pagetype=='carpet'|| $pagetype=='waterproof-flooring' || $pagetype=='waterproof-coretec' || $pagetype=='vinyl-flooring' || $pagetype=='vinyl-coretec' || $pagetype=='hardwood-flooring' ){
    
  add_filter( 'fl_topbar_enabled', '__return_false' );
  add_filter( 'fl_fixed_header_enabled', '__return_false' );
  add_filter( 'fl_header_enabled', '__return_false' );
  add_filter( 'fl_footer_enabled', '__return_false' );
}





get_header(); ?>

<?php

 $locationid = $_GET['location'];

 $query_args_meta = array(
     'posts_per_page' => -1,
     'post_type' => 'wpsl_stores',
     'meta_query' => array(
         'relation' => 'AND',
         array(
             'key' => 'wpsl_retailer_id_api',
             'value' => $locationid,
             'compare' => '='
         )
         
     )
 );
 
 $my_posts = get_posts($query_args_meta);
 if( $my_posts ) :
 //	echo 'ID on the first post found ' . $my_posts[0]->ID;
     $post   = get_post( $my_posts[0]->ID );
 endif;
 
 global $post;

 $canada = get_post_meta( $post->ID, 'wpsl_retailer_canada', true );
 

  if($pagetype=='carpet' && $canada == ''){    
      echo do_shortcode('[fl_builder_insert_layout slug="carpet-coupon-thank-you"]');

  }else if($pagetype=='carpet' && $canada == '1'){
    
    echo do_shortcode('[fl_builder_insert_layout slug="canada-carpet-retailer-coupon-thank-you-2020"]');

  }else if($pagetype=='flooring' && $canada == ''){
    echo do_shortcode('[fl_builder_insert_layout slug="all-flooring-no-coretec-coupon-sem-thank-you"]');

  }else if($pagetype=='flooring' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-all-flooring-no-coretec-sem-thank-you-2020"]');

  }else if($pagetype=='flooring-all' && $canada == ''){
    echo do_shortcode('[fl_builder_insert_layout slug="flooring-all-sem-thank-you"]');

  }else if($pagetype=='flooring-all' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-flooring-all-sem-thank-you-2020"]');

  }else if($pagetype=='waterproof-flooring' && $canada == ''){
    echo do_shortcode('[fl_builder_insert_layout slug="waterproof-no-coretec-sem-thank-you"]');

  }else if($pagetype=='waterproof-flooring' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-waterproof-no-coretec-sem-thank-you-2020"]');

  }
  else if($pagetype=='waterproof-coretec' && $canada == ''){
    echo do_shortcode('[fl_builder_insert_layout slug="waterproof-with-coretec-sem-thank-you-sem"]');

  }else if($pagetype=='waterproof-coretec' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-waterproof-with-coretec-sem-thank-you-2020"]');

  }else if($pagetype=='vinyl-flooring' && $canada == null){
    echo do_shortcode('[fl_builder_insert_layout slug="vinyl-sem-thank-you"]');

  }else if($pagetype=='vinyl-flooring' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-vinyl-sem-thank-you-2020"]');

  }else if($pagetype=='vinyl-coretec' && $canada == ''){
    echo do_shortcode('[fl_builder_insert_layout slug="vinyl-coretec-sem-thank-you"]');

  }else if($pagetype=='vinyl-coretec' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-vinyl-coretec-sem-thank-you-2020"]');

  }else if($pagetype=='hardwood-flooring' && $canada == ''){
    echo do_shortcode('[fl_builder_insert_layout slug="hardwood-flooring-sem-thank-you"]');

  }else if($pagetype=='hardwood-flooring' && $canada == '1'){
    echo do_shortcode('[fl_builder_insert_layout slug="canada-hardwood-flooring-sem-thank-you-2020"]');
  }

  ?>

			


<?php get_footer(); ?>
 