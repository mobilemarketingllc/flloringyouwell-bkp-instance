<?php

/*
Template Name: Video Listing Template

*/

get_header();

?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">

		<h1 class="entry-title sfnstoretitle"><?php echo get_the_title(); ?></h1>
			<?php
			
			$terms = get_terms( array(
				'taxonomy' => 'inspiration_video_category',
				'orderby' => 'name',
				'order' => 'DESC',
				'hide_empty' => false,
			) );

			foreach ( $terms as $term ) {
				echo '<div class="videosection"><h3>' . $term->name . '</h3>';

				$args = array(
					'post_type' => 'inspiration_video',
					'tax_query' => array(
						array(
						'taxonomy' => 'inspiration_video_category',
						'field' => 'term_id',
						'terms' => $term->term_id
						 )
					  )
					);
					$query = new WP_Query( $args );
				
				print_r($query);
				
				echo '</div>';
			}
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
