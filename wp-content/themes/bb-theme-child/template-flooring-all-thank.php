<?php

/*
Template Name: Retailer Flooring all coupon thank you SEO

*/

//Retailer Landing Page WIth COntactus Form
//flooringyouwell.com/store-name/city/state/zipcode/

get_header(); ?>

<?php

$locationid = $_GET['location'];

$query_args_meta = array(
    'posts_per_page' => -1,
    'post_type' => 'wpsl_stores',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'wpsl_retailer_id_api',
            'value' => $locationid,
            'compare' => '='
        )
        
    )
);

$my_posts = get_posts($query_args_meta);
if( $my_posts ) :
//	echo 'ID on the first post found ' . $my_posts[0]->ID;
	$post   = get_post( $my_posts[0]->ID );
endif;

global $post;


?>
		<?php echo do_shortcode('[fl_builder_insert_layout slug="flooring-all-coupon-thank-you"]');?>


<?php get_footer(); ?>

<?php

