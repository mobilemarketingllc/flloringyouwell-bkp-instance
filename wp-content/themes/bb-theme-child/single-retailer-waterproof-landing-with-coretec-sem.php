
<?php
//Retailer Waterproof Landing With Coretec SEM
//flooringyouwell.com/store-name/city/state/zipcode/coretec 

add_filter( 'fl_topbar_enabled', '__return_false' );
add_filter( 'fl_fixed_header_enabled', '__return_false' );
add_filter( 'fl_header_enabled', '__return_false' );
add_filter( 'fl_footer_enabled', '__return_false' );
get_header();

?>
<?php

$url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

$the_slug = $url_path[0];
$args = array(
  'name'        => $the_slug,
  'post_type'   => 'wpsl_stores',
  'post_status' => 'publish',
  'numberposts' => 1
);
$my_posts = get_posts($args);
if( $my_posts ) :
//	echo 'ID on the first post found ' . $my_posts[0]->ID;
	$post   = get_post( $my_posts[0]->ID );
endif;

global $post;

?>

		<?php echo do_shortcode('[fl_builder_insert_layout slug="retailer-waterproof-landing-with-coretec-sem"]');?>

<?php get_footer(); ?>
