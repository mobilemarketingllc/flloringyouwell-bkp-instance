<?php
update_option( 'cloudinary', 'true' );
// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );
define( 'WPSL_MARKER_URI', dirname( get_bloginfo( 'stylesheet_url') ) . '/wpsl-markers/' );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';
//require_once 'plugin/grand_child.php';
require_once 'product-addon/grand-child.php';


require_once 'store-custom-functions.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_style("lightgallery-css",get_stylesheet_directory_uri()."/resources/lightgallery.min.css","","");
    wp_enqueue_script("lightgallery",get_stylesheet_directory_uri()."/resources/lightgallery-all.min.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("custom_script",get_stylesheet_directory_uri()."/resources/custom_script.js","","",1);
   

});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );
 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

add_action( 'widgets_init', 'footer_sale_sidebar' );
function footer_sale_sidebar() {
    register_sidebar( array(
        'name' => __( 'Footer Sale Sidebar' ),
        'id' => 'footer-sale-sidebar',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );
}


// Custom Template For Wp STORE Locator

add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}

function fl_builder_loop_query_args_filter( $query_args ) {

    if ( 'sfnproduct-module' == $query_args['settings']->id ) {

 $query_args = array(
    'post_type'        => $query_args['settings']->post_type,
    'posts_per_page' => $query_args['settings']->posts_per_page,
    'meta_query' => array(
        array(
            'key' => 'brand',
            'value' => 'Anderson Tuftex',
            'compare' => '=',
        )
    )
 );

    }
 return $query_args;
}
//add_filter( 'fl_builder_loop_query_args', 'fl_builder_loop_query_args_filter' );


// Add PHP funtionality to Text Widget
//add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}

add_filter( 'wpsl_address_shortcode_defaults', 'custom_address_shortcode_defaults' );

function custom_address_shortcode_defaults( $shortcode_defaults ) {
    #endregion
    $shortcode_defaults['address'] = false;
    $shortcode_defaults['address2'] = false;
    $shortcode_defaults['city'] = false;
    $shortcode_defaults['state'] = false;
    $shortcode_defaults['zip'] = false;
    $shortcode_defaults['country'] = false;
    $shortcode_defaults['phone'] = false;
    $shortcode_defaults['fax'] = false;
    $shortcode_defaults['email'] = false;
    $shortcode_defaults['url'] = false;
    $shortcode_defaults['name'] = false;

    return $shortcode_defaults;
}

// Add Shortcode
function custom_direction_shortcode( $atts ) {

    $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

   

$locationid = $url_path[4];


$query_args_meta = array(
    'posts_per_page' => -1,
    'post_type' => 'wpsl_stores',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'wpsl_retailer_id_api',
            'value' => $locationid,
            'compare' => '='
        )
        
    )
);

$my_posts = get_posts($query_args_meta);
if( $my_posts ) :
//	echo 'ID on the first post found ' . $my_posts[0]->ID;
	$post   = get_post( $my_posts[0]->ID );
endif;

global $post;

    // Attributes
    $title       = get_the_title( $post->ID);
    $address       = get_post_meta( $post->ID, 'wpsl_address', true );
	$city          = get_post_meta( $post->ID, 'wpsl_city', true );
    $state         = get_post_meta( $post->ID, 'wpsl_state', true );
    $country       = get_post_meta( $post->ID, 'wpsl_country', true );
    $destination   = $title.', '.$address . ',' . $city . ',' . $state.',' . $country;

     $direction_url = "https://maps.google.com/maps?saddr=".get_current_zip_code()."&daddr=" . urlencode( $destination ) . "";

    $directionurl = '<a target="_blank" href="'.$direction_url.'">Get Directions</a>';
    
    return $directionurl;

}
add_shortcode( 'direction', 'custom_direction_shortcode' );


//Function for custom store url - Get post id from url

function get_post_id_from_custom_store_url() {

    $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

    $the_slug = $url_path[0];
    $args = array(
      'name'        => $the_slug,
      'post_type'   => 'wpsl_stores',
      'post_status' => 'publish',
      'numberposts' => 1
    );
    $my_posts = get_posts($args);
    if( $my_posts ) :
    //	echo 'ID on the first post found ' . $my_posts[0]->ID;
        $postid   =  $my_posts[0]->ID ;
    endif;
    
    return $postid;

}
//Custom Url for store name and sale url


// function prefix_url_rewrite_templates() {

//     $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

    
//     if( is_numeric($url_path[3]) && $url_path[4] != '' && $url_path[5] != '' && is_numeric($url_path[5]) && $url_path[6]=='' )
//     {
//         add_filter( 'template_include', function() {
//             //flooringyouwell.com/store-name/city/state/zipcode/
//             $new_template = locate_template( array( 'single-stores-contact.php' ) );
//             return  $new_template;
//         });
//     }
    
   
// } 
// add_action( 'template_redirect', 'prefix_url_rewrite_templates' );
 
//Function for addd class for SEM Pages

add_filter( 'body_class','halfhalf_body_class' );
function halfhalf_body_class( $classes ) {

    $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));   
 
    if( @$url_path[4] != '' && @$url_path[5] == 'waterproof-coretec')
    {
        $classes[] = 'semPage-Noheaderfooter';

    } else if( @$url_path[4] != '' && @$url_path[5] == 'waterproof-flooring')
    {
        $classes[] = 'semPage-Noheaderfooter';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'carpet')
    {
         $classes[] = 'semPage-Noheaderfooter';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'flooring' && @$url_path[6] == 'sale' )
    {
         $classes[] = 'semPage-Noheaderfooter';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'flooring-all' && @$url_path[6] == 'sale')
    {
         $classes[] = 'semPage-Noheaderfooter';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'vinyl-flooring')
    {
         $classes[] = 'semPage-Noheaderfooter';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'vinyl-coretec')
    {
         $classes[] = 'semPage-Noheaderfooter';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'hardwood-flooring')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }

    
    return $classes;
     
}


add_filter( 'body_class','coupon_body_class' );
function coupon_body_class( $classes ) {

    $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));   
 
    if( @$url_path[4] != '' && @$url_path[5] == 'waterproof-coretec' )
    {
    //    $classes[] = 'RetailerCouponPage';
    } else if( @$url_path[4] != '' && @$url_path[5] == 'waterproof-flooring' )
    {
       // $classes[] = 'RetailerCouponPage';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'carpet' )
    {
        // $classes[] = 'RetailerCouponPage';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'flooring' )
    {
        // $classes[] = 'RetailerCouponPage';

    }else if( @$url_path[4] != '' && @$url_path[5] == 'flooring-all' )
    {
       //  $classes[] = 'RetailerCouponPage';
    }

    
    return $classes;
     
}

//Function for addd class for SEM Pages
add_filter( 'body_class','noheadfoot_body_class' );
function noheadfoot_body_class( $classes ) {

    if( @$_GET['pagetype'] == 'waterproof-flooring')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'waterproof-coretec')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'flooring-all')
    {
       //  $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'flooring')
    {
       //  $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'carpet')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'hardwood-flooring')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'vinyl-flooring')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }
    if( @$_GET['pagetype'] == 'vinyl-coretec')
    {
         $classes[] = 'semPage-Noheaderfooter';
    }

    return $classes;
}


//Function for addd class for SEM Pages
add_filter( 'body_class','nocoupon_body_class' );
function nocoupon_body_class( $classes ) {

    if( @$_GET['pagetype'] == 'waterproof-flooring')
    {
         $classes[] = 'RetailerCouponPage';
    }
    if( @$_GET['pagetype'] == 'waterproof-coretec')
    {
         $classes[] = 'RetailerCouponPage';
    }
    if( @$_GET['pagetype'] == 'flooring-all')
    {
         $classes[] = 'RetailerCouponPage';
    }
    if( @$_GET['pagetype'] == 'flooring')
    {
         $classes[] = 'RetailerCouponPage';
    }
    if( @$_GET['pagetype'] == 'carpet')
    {
         $classes[] = 'RetailerCouponPage';
    }

    return $classes;
}

//For dynamic url creation
//remove_action('template_redirect', 'redirect_canonical');

//Get Id from sluf function  

function get_id_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
} 


//Custom filter for Store listing on find retailer page

add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl, $wpsl_settings;
    //echo '<%= permalink %>';
   $perma = "<%= permalink.replace('".home_url()."/stores','') %>";
   // echo $perma;
   $current_store_id =0;
   $st_id = '<%= id %>';
   
   //echo "<%= jQuery.cookie('my_store') %>";
  
    
    $st_id1 = (json_decode(json_encode($st_id)));
  //  $st_id1 = '<div>'.$st_id.'</div>' . "\r\n";
   // echo 'TEST =>'.$st_id1;
       // $storename = get_the_title($st_id);
        //$store_name = strtolower(str_replace(' ','-',$storename));
        $zipcode_store = get_post_meta( $st_id1, 'wpsl_zip', true );
        $city_store = strtolower(get_post_meta( $st_id1, 'wpsl_city', true ));
        $state_store = strtolower(get_post_meta( $st_id1, 'wpsl_state', true ));

        $retailer_id = get_post_meta( $st_id1, 'retailer_id_api', true );
        $retailer_id_url = get_post_meta( $st_id1, 'retailer_id_api', true );

    
        $custom_store_url = home_url().'/<%= retailer_couponSeoUrl %>';
        $store_url = home_url().'/<%= retailer_storeurl %>';

    $listing_template = '<li data-store-id="'.$st_id.'">' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-store-location">' . "\r\n";
   // $listing_template .= "\t\t\t" . '<p><%= thumb %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . wpsl_store_header_template( 'listing' ) . "\r\n"; // Check which header format we use
    $listing_template .= "\t\t\t\t" . '<div class="storeDetails"><div class="storeAddress"><span class="wpsl-street"><%= address %> <% if ( address2 ) { %>,<% } %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" .'<% if ( address2 ) { %><span class="wpsl-street"><%= address2 %>,</span><% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" .'<span class="wpsl-street"><%= city %>, <%= state %> <%= zip %></span>' . "\r\n";

    $listing_template .= "\t\t\t\t" . '' . "\r\n";
    

   

    // Show exist Forwarding Phone Number.
    if ( $wpsl_settings['show_contact_details'] ) {
        $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
		
		$listing_template .= "\t\t\t" . '<% if ( retailer_forwardingphnumber ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><a class="usphnum" href="tel:<%= formatPhoneNumber( retailer_forwardingphnumber ) %>">Ph. <span class="num"><%= formatPhoneNumber( retailer_forwardingphnumber ) %></span></a></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } else { %>' . "\r\n";
		
        $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><a class="usphnum" href="tel:<%= formatPhoneNumber( phone ) %>">Ph. <span class="num"><%= formatPhoneNumber( phone ) %></span></a></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% }  } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    }

    // Show the phone, fax or email data if they exist.
    // if ( $wpsl_settings['show_contact_details'] ) {
    //     $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
    //     $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
    //     $listing_template .= "\t\t\t" . '<span><a href="tel:<%= formatPhoneNumber( phone ) %>">Ph. <%= formatPhoneNumber( phone ) %></a></span>' . "\r\n";
    //     $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
    //     $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    // }
    
    $listing_template .= "\t\t\t" . '</div>' . "\r\n";

   // $listing_template .= "\t\t\t" . '<div class="sfn-store-links-wrapper">' . "\r\n";    
   // $listing_template .= "\t\t\t" . '<ul class="sfn-store-links"><li><a href="<% if ( retailer_sale ) { %>'.$custom_store_url.'<% }else{ %>'.home_url().'/<%= retailer_storeurl %><% } %>">View Retailer</a></li> </ul>' . "\r\n";
   // $listing_template .= "\t\t\t" . '</div></div>' . "\r\n";
     

    
    $listing_template .= "\t\t\t" . wpsl_more_info_template() . "\r\n"; // Check if we need to show the 'More Info' link and info
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $st_id = str_replace("\0", "", $st_id);
    //var_dump(trim($_COOKIE['my_store']),($st_id));
    //if(trim($_COOKIE['my_store']) == ($st_id)){

        $listing_template .= "\t\t" . '<div class="Couponlinks-wrap"><div class="mystorelink">' . "\r\n";
        $listing_template .= "\t\t\t" . '<input type="radio" id="<%= id %>_mystore"  class="mystore" name="mystore" data-id="<%= id %>"  value="<%= id %>"> <label for="<%= id %>_mystore">Make My Store</label>' . "\r\n"; 
        $listing_template .= "\t\t" . '</div>' . "\r\n";
    /* }
    else{
        $listing_template .= "\t\t" . '<div class="Couponlinks-wrap"><div class="mystorelink">' . "\r\n";
        $listing_template .= "\t\t\t" . 'My Store' . "\r\n"; 
        $listing_template .= "\t\t" . '</div>' . "\r\n";
    } */

    $listing_template .= "\t\t" . '<div class="Coupon-wrapper">' . "\r\n";
    $listing_template .= "\t\t\t" . '<div class="couponcode"></div>' . "\r\n";

    $listing_template .= "\t\t\t" . '<div class="GetCouponBtn test1"><a href="'.home_url().'/<%= retailer_couponSeoUrl %>" class="fl-button">GET COUPON</a></div>' . "\r\n"; 
    $listing_template .= "\t\t" . '</div></div>' . "\r\n";
    $listing_template .= "\t" . '</li>';

    return $listing_template;
}


//Custom filter for Store Header template

add_filter( 'wpsl_store_header_template', 'custom_store_header_template' );

function custom_store_header_template() {

    global $wpsl, $wpsl_settings;    
   $perma = "<%= permalink.replace('".home_url()."/stores','') %>";
 
    $st_id = '<%= id %>';
    $st_id1 = (json_decode(json_encode($st_id)));
  
        $zipcode_store = get_post_meta( $st_id1, 'wpsl_zip', true );
        $city_store = strtolower(get_post_meta( $st_id1, 'wpsl_city', true ));
        $state_store = strtolower(get_post_meta( $st_id1, 'wpsl_state', true ));
        $retailer_id = get_post_meta( $st_id1, 'retailer_id_api', true );

        $custom_store_url = home_url().'/<%= state.toLowerCase() %>/<%= city.toLowerCase() %>/<%= zip %>'.$perma.'<%= retailer_id_api %>/';
    
    //$header_template = '<% if ( wpslSettings.storeUrl == 1 && url ) { %>' . "\r\n";
    $header_template = '<div class="listingTitle"><h3><a href="'.home_url().'/<%= retailer_couponSeoUrl %>/"><%= store %></a></h3> <span class="diststore"><%= distance %> ' . esc_html( wpsl_get_distance_unit() ) . ' </span></div>' . "\r\n";
    //$header_template .= '<% } else { %>' . "\r\n";
   // $header_template .= '<h3 class="strtitle"><%= store %></h3>' . "\r\n";
   // $header_template .= '<% } %>'; 
    
    return $header_template;
}


//Custom filter for Store info window

add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );

function custom_info_window_template() {

    global $wpsl_settings, $wpsl;
   
    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window">' . "\r\n";
    $info_window_template .= "\t\t" . '<p>' . "\r\n";
    $info_window_template .= "\t\t\t" .  wpsl_store_header_template() . "\r\n";  
    $info_window_template .= "\t\t\t" . '<span><%= address %>,</span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= address2 %>,</span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= city %>, <%= state %> <%= zip %></span>' . "\r\n";
    
    $info_window_template .= "\t\t" . '</p>' . "\r\n";
    
    //$info_window_template .= "\t\t" . '<% if ( phone ) { %>' . "\r\n";
    //$info_window_template .= "\t\t" . '<span class="ph"><strong>' . esc_html( $wpsl->i18n->get_translation( 'phone_label', __( 'Phone', 'wpsl' ) ) ) . '</strong>: <%= formatPhoneNumber( phone ) %></span>' . "\r\n";
   // $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
   
        $info_window_template .= "\t\t\t" . '<% if ( retailer_forwardingphnumber ) { %>' . "\r\n";
        $info_window_template .= "\t\t\t" . '<span><a class="usphnum" href="tel:<%= formatPhoneNumber( retailer_forwardingphnumber ) %>">Ph. <span class="num"><%= formatPhoneNumber( retailer_forwardingphnumber ) %></span></a></span>' . "\r\n";
        $info_window_template .= "\t\t\t" . '<% } else { %>' . "\r\n";
		
        $info_window_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
        $info_window_template .= "\t\t\t" . '<span><a class="usphnum" href="tel:<%= formatPhoneNumber( phone ) %>">Ph. <span class="num"><%= formatPhoneNumber( phone ) %></span></a></span>' . "\r\n";
        $info_window_template .= "\t\t\t" . '<% }  } %>' . "\r\n";
        
    $info_window_template .= "\t\t" . '<% if ( fax ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong>: <%= fax %></span>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    //$info_window_template .= "\t\t" . '<% if ( email ) { %>' . "\r\n";
   // $info_window_template .= "\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</strong>: <%= formatEmail( email ) %></span>' . "\r\n";
   // $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    
    
    
   // $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
    
    return $info_window_template;
}









//Add to My store FUnctionality

add_action( 'wp_ajax_nopriv_make_my_store', 'make_my_store' );
add_action( 'wp_ajax_make_my_store', 'make_my_store' );

function make_my_store() {
   $store_name = get_the_title($_POST['store_id']); 
   $store_ph  = get_post_meta($_POST['store_id'],'wpsl_phone', true); 
    echo $resarr = $store_name.'|'.$store_ph;  
    wp_die();
}


// Header Sidebar
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Header Store Sidebar', 'theme-slug' ),
        'id' => 'header-store',
        'description' => __( 'Widgets in this area will be shown on header store area.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h4 class="widgettitle">',
	'after_title'   => '</h4>',
    ) );
}


// Define url Store custom field

add_filter( 'wpsl_meta_box_fields', 'custom_meta_box_fields' );

function custom_meta_box_fields( $meta_fields ) {

   /**
     * If no 'type' is defined it will show a normal text input field.
     *
     * Supported field types are checkbox, textarea and dropdown.
     */
     $meta_fields[ __( 'Show Hide Website', 'wpsl' ) ] = array(
       
        'show_url' => array(
            'label' => __( 'Show Website Url', 'wpsl' ),
            'type'  => 'checkbox'
        )
    );

    return $meta_fields;
}


// Store custom field for site url
add_filter( 'wpsl_frontend_meta_fields', 'custom_frontend_meta_fields' );

function custom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_show_url'] = array( 
        'name' => 'show_url',
        'type' => 'text'
    );

    return $store_fields;
}

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}

// function getUserIpAddr(){
    // if(!empty($_SERVER['HTTP_CLIENT_IP'])){
       //// ip from share internet
        // $ip = $_SERVER['HTTP_CLIENT_IP'];
    // }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
      // // ip pass from proxy
        // $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    // }else{
        // $ip = $_SERVER['REMOTE_ADDR'];
    // }
    // return $ip;
// }


// Custom Shortcode for Store Listing

function get_storelisting() {
    
    global $wpdb;
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
        
                    $response = wp_remote_post( $urllog, array(
                        'method' => 'GET',
                        'timeout' => 45,
                        'redirection' => 5,
                        'httpversion' => '1.0',
                        'headers' => array('Content-Type' => 'application/json'),         
                        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
                        'blocking' => true,               
                        'cookies' => array()
                        )
                    );
                        //print_r($response['body']);

                        
                        
                        $rawdata = json_decode($response['body'], true);
                        $userdata = $rawdata['response'];
                        $autolat = $userdata['pulseplus-latitude'];
                        $autolng = $userdata['pulseplus-longitude'];

                       /*  $autolat = '40.47';//'35.384940';//$userdata['pulseplus-latitude'];
                        $autolng = ' -80.11';//'-86.230690';//$userdata['pulseplus-longitude']; */
                       

                       // writeToLog($userdata['pulseplus-postal-code']);

                       if($_GET['search'] == "coretec"){                           
                        $coretec_search = "AND  post_retailer_coretecVinyl.meta_value = '1'";
                       }else{
                        $coretec_search = "";
                    }

                    $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,post_retailer_coretecVinyl.meta_value AS retailer_coretecVinyl,posts.ID, 
                                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM $wpdb->posts AS posts
                                INNER JOIN $wpdb->postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
                                INNER JOIN $wpdb->postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'
                                INNER JOIN $wpdb->postmeta AS post_retailer_coretecVinyl ON post_retailer_coretecVinyl.post_id = posts.ID AND post_retailer_coretecVinyl.meta_key = 'wpsl_retailer_coretecVinyl'
                                WHERE posts.post_type = 'wpsl_stores' $coretec_search
                                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50 ORDER BY distance LIMIT 0, 10";

   

                  
    $storeposts = $wpdb->get_results(  $sql);
                  
          
    $listcontent = '<div id="header_storeListing">'.dynamic_sidebar( 'header-store' ).' <ul>';   

    global $post;

    if(isset($_COOKIE['my_store']) && $_COOKIE['my_store'] !=""){
        $slug = get_post_field( 'post_name',$_COOKIE['my_store'] );
        $address_list = get_post_meta( $_COOKIE['my_store'], 'wpsl_address', true );
        
        $zipcode_list = get_post_meta( $_COOKIE['my_store'], 'wpsl_zip', true );
        $city_list = strtolower(get_post_meta( $_COOKIE['my_store'], 'wpsl_city', true ));
        $state_list = strtolower(get_post_meta( $_COOKIE['my_store'], 'wpsl_state', true ));   
        $city_list_up = get_post_meta( $_COOKIE['my_store'], 'wpsl_city', true );
        $state_list_up = get_post_meta( $_COOKIE['my_store'], 'wpsl_state', true );  
        $ph_list = get_post_meta( $_COOKIE['my_store'], 'wpsl_phone', true );
        $retailer_couponSeoUrl = get_post_meta( $_COOKIE['my_store'], 'wpsl_retailer_couponSeoUrl', true );        
        $retailer_id_from_api =  get_post_meta( $_COOKIE['my_store'], '	wpsl_retailer_id_api', true );
        $retailer_st_url = get_post_meta( $_COOKIE['my_store'], 'wpsl_retailer_storeurl', true );
        $retailer_sale =  get_post_meta( $_COOKIE['my_store'], 'wpsl_retailer_sale', true );

        if($retailer_sale==1){

            $store_sale_url = home_url().'/'.$retailer_couponSeoUrl;

        }else{

            $store_sale_url = home_url().'/'.$retailer_st_url.'/contact-us/';
        }

        $listcontent .= '<li id="'.$post->ID.'" class="current-my-store">
        <div class="Store_list_item ">

            <h3 class="strtitle test3"><a href="/'.$retailer_couponSeoUrl.'">'.get_the_title( $_COOKIE['my_store']).'</a></h3>

            <div class="storeDetails">
                <div class="storeAddress">
                <span class="wpsl-street">'.$address_list.'</span>                
                <span class="wpsl-street">'.$city_list_up.', '.$state_list_up.' '.$zipcode_list.'</span>
                </div>

            </div>

            <div class="Couponlinks-wrap"><div class="mystorelink">

			<input type="radio" id="'.$post->ID.'_mystore" class="mystore" name="mystore" data-id="'.$post->ID.'" value="'.$post->ID.'" checked="checked" disabled="disabled" /> <label for="'.$post->ID.'_mystore">My Store</label>
           </div>';
          
           if($retailer_sale==1){
           
           $listcontent .= '<div class="Coupon-wrapper">

			<div class="couponcode"></div>
			<div class="GetCouponBtn test2"><a href="'.$store_sale_url.'" class="fl-button">GET COUPON</a></div>
        </div>';

           }

        $listcontent .= '      
        </div>

        </div>
        <div class="store_map_link">
            <div class="headmystorelink">
            <a href="/'.$retailer_couponSeoUrl.'" class="fl-button mystore btnmystore current-my-store-link"  data-id="'. $_COOKIE['my_store'].'" class="mystore">
            <span class="fl-button-text">MY STORE</span></a>
            </div>
        </div>
        </li>';
    }
    
    foreach ($storeposts as $post) {
        
    //print_r($post);
   // if(isset($_COOKIE['my_store']) && $_COOKIE['my_store'] !=$post->ID){
    
    if( $_COOKIE['my_store'] == $post->ID){
       $hide = 'display:none';
    }else{

        $hide = '';
    }

    $slug = get_post_field( 'post_name', $post->ID );
    $address_list = get_post_meta( $post->ID, 'wpsl_address', true );
    $zipcode_list = get_post_meta( $post->ID, 'wpsl_zip', true );
    $city_list = strtolower(get_post_meta( $post->ID, 'wpsl_city', true ));
    $city_list_head = get_post_meta( $post->ID, 'wpsl_city', true );
    $state_list = strtolower(get_post_meta( $post->ID, 'wpsl_state', true ));   
    $state_list_head = get_post_meta( $post->ID, 'wpsl_state', true );
    $ph_list = get_post_meta( $post->ID, 'wpsl_phone', true );
    $retailer_couponSeoUrl_li = get_post_meta(  $post->ID, 'wpsl_retailer_couponSeoUrl', true );
    $retailer_id_from_api =  get_post_meta( $post->ID, 'retailer_id_from_api', true );
    $retailer_st_url = get_post_meta( $post->ID, 'wpsl_retailer_storeurl', true );
    $retailer_sale =  get_post_meta( $post->ID, 'wpsl_retailer_sale', true );

        if($retailer_sale==1){

            $store_sale_url = home_url().'/'.$retailer_couponSeoUrl_li;

        }else{

            $store_sale_url = home_url().'/'.$retailer_st_url.'/contact-us/';
        }

    $listcontent .= '<li id="'.$post->ID.'" style="'.$hide.'">
    <div class="Store_list_item">

        <h3 class="strtitle lis"><a href="'.home_url().'/'. $store_sale_url.'/">'.get_the_title($post->ID).'</a></h3>

        <div class="storeDetails">
            <div class="storeAddress">
            <span class="wpsl-street">'.$address_list.'</span>
            <span class="wpsl-street">'.$city_list_head.', '.$state_list_head.' '.$zipcode_list.'</span>
            </div>
        </div>

        <div class="Couponlinks-wrap"><div class="mystorelink">
			<input type="radio" id="'.$post->ID.'_mystore" class="mystore" name="mystore" data-id="'.$post->ID.'" value="'.$post->ID.'"> <label for="'.$post->ID.'_mystore">Make My Store</label>

        </div>';
        
        if($retailer_sale==1){

		$listcontent .='<div class="Coupon-wrapper">

			<div class="couponcode"> </div>
			<div class="GetCouponBtn test3"><a href="'.$store_sale_url.'/" class="fl-button">GET COUPON</a></div>
        </div>';
        }
        
        $listcontent .=  '</div>

    </div>
    <div class="store_map_link">
    <div class="headmystorelink">
    
    
    </div>
     
</li>';
   // }
    }

wp_reset_postdata();

    $listcontent .= '</ul></div>';

    return $listcontent;

}

add_shortcode('storelisting', 'get_storelisting');



// Custom  Marker for map
add_filter( 'wpsl_admin_marker_dir', 'custom_admin_marker_dir' );

function custom_admin_marker_dir() {

    $admin_marker_dir = get_stylesheet_directory() . '/wpsl-markers/';
    
    return $admin_marker_dir;
}



//Search Widget Custom Template function
add_filter( 'wpsl_widget_templates', 'custom_widget_templates' );

function custom_widget_templates( $templates ) {

    /**
     * The 'id' is used in the shortcode and accessible with [wpsl_widget template="the-used-id"]
     * The 'name' is shown in the widget configuration screen.
     * The 'path' points in this example to a 'wpsl-templates' folder inside your theme folder.
     * The 'file_name' is the name of the file that contains the custom template code.
     */
    $templates[] = array (
        'id'        => 'custom',
        'name'      => 'Custom template',
        'path'      => get_stylesheet_directory() . '/' . 'wpsl-templates/',
        'file_name' => 'custom-widget.php'
    );

    return $templates;
}


/**
 * Allow numeric slug
 *
 * @param string $slug The slug returned by wp_unique_post_slug().
 * @param int $post_ID The post ID that the slug belongs to.
 * @param string $post_status The status of post that the slug belongs to.
 * @param string $post_type The post_type of the post.
 * @param int $post_parent Post parent ID.
 * @param string $original_slug The requested slug, may or may not be unique.
 */

add_filter( 'wp_unique_post_slug', 'mg_unique_post_slug', 10, 6 ); 

function mg_unique_post_slug( $slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug ) {
 global $wpdb;
 
// don't change non-numeric values
 if ( ! is_numeric( $original_slug ) || $slug === $original_slug ) {
 return $slug;
 }
 
// Was there any conflict or was a suffix added due to the preg_match() call in wp_unique_post_slug() ?
 $post_name_check = $wpdb->get_var( $wpdb->prepare(
 "SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND post_type IN ( %s, 'attachment' ) AND ID != %d AND post_parent = %d LIMIT 1",
 $original_slug, $post_type, $post_ID, $post_parent
 ) );
 
// There really is a conflict due to an existing page so keep the modified slug
 if ( $post_name_check ) {
 return $slug;
 }
 
// Return our numeric slug
 return $original_slug;
}


// Defined Custom field for store post types
add_filter( 'wpsl_meta_box_fields', 'retailer_custom_meta_box_fields' );

function retailer_custom_meta_box_fields( $meta_fields ) {

    /**
     * If no 'type' is defined it will show a normal text input field.
     *
     * Supported field types are checkbox, textarea and dropdown.
     */
    $meta_fields[ __( 'Retailer Information', 'wpsl' ) ] = array(

        'retailer_id_api' => array(
            'label'    => __( 'RetailerID', 'wpsl' ),
            'required' => true // This will place a * after the label
        ),

        'retailer_affiliateCode' => array(
            'label'    => __( 'Retailer Affiliate Code', 'wpsl' ),
            'type'  => 'text'
        ),
        
        'retailer_sale' => array(
            'label'    => __( 'Sale', 'wpsl' ),
            'type'  => 'checkbox'
        ),

        'retailer_paid_search' => array(
            'label'    => __( 'Paid Search', 'wpsl' ),
            'type'  => 'checkbox'
        ),

        'retailer_coretecVinyl' => array(
            'label'    => __( 'Coretec Vinyl', 'wpsl' ),
            'type'  => 'checkbox'
        ),

        'retailer_financing' => array(
            'label'    => __( 'Financing', 'wpsl' ),
            'type'  => 'checkbox'
        ),

        'retailer_canada' => array(
            'label'    => __( 'Canada', 'wpsl' ),
            'type'  => 'checkbox'
        ),
        
        'retailer_couponSeoUrl' => array(
            'label'    => __( 'Coupon SEO URL', 'wpsl' ),
            'type'  => 'text'
        ),

        'retailer_couponUrlSem' => array(
            'label'    => __( 'Coupon SEM URL', 'wpsl' ),
            'type'  => 'text'
        ),

        'retailer_carpeturl' => array(
            'label'    => __( 'Carpet SEO URL', 'wpsl' ),
            'type'  => 'text'
        ),

        'retailer_waterproofurl' => array(
            'label'    => __( 'Waterproof SEO URL', 'wpsl' ),
            'type'  => 'text'
        ),

        'retailer_hardwoodUrl' => array(
            'label'    => __( 'Hardwood URL', 'wpsl' ),
            'type'  => 'text'
        ),

        'retailer_vinylUrl' => array(
            'label'    => __( 'Vinyl URL', 'wpsl' ),
            'type'  => 'text'
        ),

        'retailer_forwardingphnumber' => array(
            'label'    => __( 'Forwarding ph Number', 'wpsl' ),
            'type'  => 'text'
        ),
        'retailer_storeurl' => array(
            'label'    => __( 'Store URL', 'wpsl' ),
            'type'  => 'text'
        )
    );

    return $meta_fields;
}

// Added Custom field for store post types
add_filter( 'wpsl_frontend_meta_fields', 'retailer_id_api_custom_frontend_meta_fields' );

function retailer_id_api_custom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_retailer_id_api'] = array( 
        'name' => 'retailer_id_api' 
    );    

    $store_fields['wpsl_retailer_affiliateCode'] = array( 
        'name' => 'retailer_affiliateCode' 
    );

    $store_fields['wpsl_retailer_sale'] = array( 
        'name' => 'retailer_sale' 
    );

    $store_fields['wpsl_retailer_paid_search'] = array( 
        'name' => 'retailer_paid_search' 
    );

    $store_fields['wpsl_retailer_financing'] = array( 
        'name' => 'retailer_financing' 
    );

    $store_fields['wpsl_retailer_canada'] = array( 
        'name' => 'retailer_canada' 
    );

    $store_fields['wpsl_retailer_coretecVinyl'] = array( 
        'name' => 'retailer_coretecVinyl' 
    );

    $store_fields['wpsl_retailer_couponSeoUrl'] = array( 
        'name' => 'retailer_couponSeoUrl' 
    );

    $store_fields['wpsl_retailer_couponUrlSem'] = array( 
        'name' => 'retailer_couponUrlSem' 
    );

    $store_fields['wpsl_retailer_carpeturl'] = array( 
        'name' => 'retailer_carpeturl' 
    );

    $store_fields['wpsl_retailer_hardwoodUrl'] = array( 
        'name' => 'retailer_hardwoodUrl' 
    );

    $store_fields['wpsl_retailer_vinylUrl'] = array( 
        'name' => 'retailer_vinylUrl' 
    );
    
    $store_fields['wpsl_retailer_waterproofurl'] = array( 
        'name' => 'retailer_waterproofurl' 
    );    

    $store_fields['wpsl_retailer_forwardingphnumber'] = array( 
        'name' => 'retailer_forwardingphnumber' 
    );

    $store_fields['wpsl_retailer_storeurl'] = array( 
        'name' => 'retailer_storeurl' 
    );
    
    return $store_fields;


}

//Populate location in form hidden field
add_filter( 'gform_field_value_locationCode', 'populate_locationCode' );
function populate_locationCode( $value ) {

    $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

    $locationid = $url_path[4];

   return $locationid;
}

//Populate locatpagetype(Seo/SEM)ion in form hidden field
add_filter( 'gform_field_value_pagetype', 'populate_pagetype' );
function populate_pagetype( $value ) {

    $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

    if(in_array('sale',$url_path)){

        $pagetype = $url_path[5].'_'.$url_path[6];

    }else{

        $pagetype = $url_path[5];
    }
    
   return $pagetype;
}

// get user current locztion zip code

function get_current_zip_code() {

    global $wpdb;
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
        
                    $response = wp_remote_post( $urllog, array(
                        'method' => 'GET',
                        'timeout' => 45,
                        'redirection' => 5,
                        'httpversion' => '1.0',
                        'headers' => array('Content-Type' => 'application/json'),         
                        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
                        'blocking' => true,               
                        'cookies' => array()
                        )
                    );
                       // print_r($response['body']);
                                             
                        $rawdata = json_decode($response['body'], true);
                        $userdata = $rawdata['response'];
                        $zipcode = $userdata['pulseplus-postal-code'];
                        date_default_timezone_set('Asia/Calcutta');
                        $logip =  date("Y-m-d h:i:sa").' => IP Address - '.getUserIpAddr().' => ZipCode - '.$userdata['pulseplus-postal-code'].'=> City - '.$userdata['pulseplus-city'];
                       // writeToLog($logip);
                      
                     return $zipcode;
}
add_shortcode('currentzipcode', 'get_current_zip_code');



add_action( 'wp_ajax_nopriv_get_storelisting_header_ajax', 'get_storelisting_header_ajax' );
add_action( 'wp_ajax_get_storelisting_header_ajax', 'get_storelisting_header_ajax' );
//add_action( 'wp_loaded', 'get_storelisting_header_ajax');
function get_storelisting_header_ajax() {
    
    global $wpdb;
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
        
                    $response = wp_remote_post( $urllog, array(
                        'method' => 'GET',
                        'timeout' => 45,
                        'redirection' => 5,
                        'httpversion' => '1.0',
                        'headers' => array('Content-Type' => 'application/json'),         
                        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
                        'blocking' => true,               
                        'cookies' => array()
                        )
                    );
                        //print_r($response['body']);
                        
                        $rawdata = json_decode($response['body'], true);
                        $userdata = $rawdata['response'];
                        $autolat = $userdata['pulseplus-latitude'];
                        $autolng = $userdata['pulseplus-longitude'];

                        

                       $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM $wpdb->posts AS posts
                                INNER JOIN $wpdb->postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
                                INNER JOIN $wpdb->postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'
                                WHERE posts.post_type = 'wpsl_stores' 
                                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50 ORDER BY distance LIMIT 0, 10";

                   

    $storeposts = $wpdb->get_results(  $sql);
                  
             //print_r($storeposts);     
                  
    //my_store
    // $args = array( 'post_type' => 'wpsl_stores', 'posts_per_page' => 10 );
    // $loop = new WP_Query( $args );    

    $listcontent = '<div id="header_storeListing">'.dynamic_sidebar( 'header-store' ).' <ul>';   

    global $post;

    if(isset($_COOKIE['my_store']) && $_COOKIE['my_store'] !=""){
        $slug = get_post_field( 'post_name',$_COOKIE['my_store'] );
        $address_list = get_post_meta( $_COOKIE['my_store'], 'wpsl_address', true );
        $zipcode_list = get_post_meta( $_COOKIE['my_store'], 'wpsl_zip', true );
        $city_list = strtolower(get_post_meta( $_COOKIE['my_store'], 'wpsl_city', true ));
        $state_list = strtolower(get_post_meta( $_COOKIE['my_store'], 'wpsl_state', true ));   
        $city_list_up = get_post_meta( $_COOKIE['my_store'], 'wpsl_city', true );
        $state_list_up = get_post_meta( $_COOKIE['my_store'], 'wpsl_state', true );  
        $ph_list = get_post_meta( $_COOKIE['my_store'], 'wpsl_phone', true );
        $retailer_couponSeoUrl = get_post_meta( $_COOKIE['my_store'], 'wpsl_retailer_couponSeoUrl', true );
        $mystoretitle = get_the_title( $_COOKIE['my_store']); 
        $retailer_id_from_api =  get_post_meta( $_COOKIE['my_store'], 'retailer_id_from_api', true );

        $retailer_st_url = get_post_meta( $_COOKIE['my_store'], 'wpsl_retailer_storeurl', true );
        $retailer_sale =  get_post_meta( $_COOKIE['my_store'], 'wpsl_retailer_sale', true );

        if($retailer_sale==1){

            $store_sale_url = home_url().'/'.$retailer_couponSeoUrl;

        }else{

            $store_sale_url = home_url().'/'.$retailer_st_url.'/contact-us/';
        }

        $listcontent .= '<li id="'.$post->ID.'" class="current-my-store">
        <div class="Store_list_item ">

            <h3 class="strtitle"><a href="'.$store_sale_url.'">'.get_the_title( $_COOKIE['my_store']).'</a></h3>

            <div class="storeDetails">
                <div class="storeAddress">
                <span class="wpsl-street">'.$address_list.'</span>
                <span class="wpsl-street"> '.$city_list_up.', '.$state_list_up.' '.$zipcode_list.'</span>
                </div>
            </div>

            <div class="Couponlinks-wrap"><div class="mystorelink">

			<input type="radio" id="'.$post->ID.'_mystore" class="mystore" name="mystore" data-id="'.$post->ID.'" value="'.$post->ID.'" disabled="disabled" checked="checked" /> <label for="'.$post->ID.'_mystore">My Store</label>
           </div>';
           
           if($retailer_sale==1){

           $listcontent .= '<div class="Coupon-wrapper">

			<div class="couponcode"> </div>
			<div class="GetCouponBtn test4"><a href="'.$store_sale_url.'" class="fl-button">GET COUPON</a></div>
        </div>';
           }
        
        $listcontent .='</div>

        </div>
        <div class="store_map_link">
            <div class="headmystorelink">
            <a href="'.$retailer_couponSeoUrl.'" class="fl-button mystore btnmystore current-my-store-link"  data-id="'. $_COOKIE['my_store'].'" class="mystore">
            <span class="fl-button-text">MY STORE</span></a>
            </div>
        </div>
        </li>';
    }
    
    foreach ($storeposts as $post) {
        
    //print_r($post);
   // if(isset($_COOKIE['my_store']) && $_COOKIE['my_store'] !=$post->ID){
    
    if( $_COOKIE['my_store'] == $post->ID){
       $hide = 'display:none';
    }else{

        $hide = '';
    }

    $slug = get_post_field( 'post_name', $post->ID );
    $address_list = get_post_meta( $post->ID, 'wpsl_address', true );
    $zipcode_list = get_post_meta( $post->ID, 'wpsl_zip', true );
    $city_list = strtolower(get_post_meta( $post->ID, 'wpsl_city', true ));
    $city_list_head = get_post_meta( $post->ID, 'wpsl_city', true );
    $state_list = strtolower(get_post_meta( $post->ID, 'wpsl_state', true ));   
    $state_list_head = get_post_meta( $post->ID, 'wpsl_state', true );
    $ph_list = get_post_meta( $post->ID, 'wpsl_phone', true );
    $retailer_couponSeoUrl = get_post_meta( $post->ID, 'wpsl_retailer_couponSeoUrl', true );
    $retailer_id_from_api =  get_post_meta( $post->ID, 'wpsl_retailer_id_from_api', true );

       $retailer_st_url = get_post_meta( $post->ID, 'wpsl_retailer_storeurl', true );
        $retailer_sale =  get_post_meta( $post->ID, 'wpsl_retailer_sale', true );

        if($retailer_sale==1){

            $store_sale_url = home_url().'/'.$retailer_couponSeoUrl;

        }else{

            $store_sale_url = home_url().'/'.$retailer_st_url.'/contact-us/';
        }

    $listcontent .= '<li id="'.$post->ID.'" style="'.$hide.'">
    <div class="Store_list_item">

        <h3 class="strtitle"><a href="'.home_url().'/'.$store_sale_url.'/">'.get_the_title( $post->ID).'</a></h3>

        <div class="storeDetails">
            <div class="storeAddress">
            <span class="wpsl-street">'.$address_list.'</span>
            <span class="wpsl-street">'.$city_list_head.', '.$state_list_head.' '.$zipcode_list.'</span>
            </div>
        </div>

        <div class="Couponlinks-wrap"><div class="mystorelink">
			<input type="radio" id="'.$post->ID.'_mystore" class="mystore" name="mystore" data-id="'.$post->ID.'" value="'.$post->ID.'"> <label for="'.$post->ID.'_mystore">Make My Store</label>

        </div>';
        
		$listcontent .='<div class="Coupon-wrapper">

			<div class="couponcode"> </div>
			<div class="GetCouponBtn test4"><a href="'.$store_sale_url.'/" class="fl-button">GET COUPON</a></div>
        </div>';
        
        
        $listcontent .=' </div>

    </div>
    <div class="store_map_link">
        <div class="headmystorelink">
        
        
        </div>
         
        
    </div>
</li>';
   // }
    }

wp_reset_postdata();

    $listcontent .= '</ul></div>';

    echo $listcontent;

    wp_die();

}



function get_nearest_store_user() {
 
          if(!isset($_COOKIE['my_store_name']) && !isset($_COOKIE['my_store']) && !isset($_COOKIE['my_store_phone']))
        {   
    
                    global $wpdb;
                    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
        
                    $response = wp_remote_post( $urllog, array(
                        'method' => 'GET',
                        'timeout' => 45,
                        'redirection' => 5,
                        'httpversion' => '1.0',
                        'headers' => array('Content-Type' => 'application/json'),         
                        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
                        'blocking' => true,               
                        'cookies' => array()
                        )
                    );
                        //print_r($response['body']);
                        
                        $rawdata = json_decode($response['body'], true);
                        $userdata = $rawdata['response'];
                        $autolat = $userdata['pulseplus-latitude'];
                        $autolng = $userdata['pulseplus-longitude'];                      

                       
                       $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM $wpdb->posts AS posts
                                INNER JOIN $wpdb->postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
                                INNER JOIN $wpdb->postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'
                                WHERE posts.post_type = 'wpsl_stores' 
                                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50 ORDER BY distance LIMIT 0, 1";

                      $storeposts = $wpdb->get_results(  $sql);

                     //print_r($storeposts);

                     foreach ($storeposts as $post) {

                      $store_name = get_the_title($post->ID);
                      $phnum = get_post_meta( $post->ID, 'wpsl_phone', true );

                     setcookie("my_store_name", $store_name, time() + (365 * 20), COOKIEPATH, COOKIE_DOMAIN, false );
                     setcookie("my_store", $post->ID, time() + (365 * 20), COOKIEPATH, COOKIE_DOMAIN, false ); 
                     setcookie("my_store_phone", $phnum, time() + (365 * 20), COOKIEPATH, COOKIE_DOMAIN, false ); 
                     
                     
                     }

                    //return $output;


        } 
        
}
add_action( 'init', 'get_nearest_store_user');

//Gravity form filter

    add_filter( 'gform_confirmation_anchor', function() {
        return 20;
    } );

  

// Remove Retailer Sale pages from sitemap
    add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', function () {

        global $wpdb;

        $queryPost = "SELECT  id FROM wp_posts WHERE post_name ='sale'";        

	   $queryResults = $wpdb->get_results(  $queryPost ); 
	   
	   $queryfinal = array();

       foreach( $queryResults as $res ) {
	        
		$queryfinal[]= $res->id;
	   }

        return $queryfinal;
      } ); 
      

 // Shortcode for carpet linking

      function landing_carpet_url() {

        global $wpdb;

        $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

        $locationid = $url_path[5];
        $query_args_meta = array(
            'posts_per_page' => -1,
            'post_type' => 'wpsl_stores',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'wpsl_retailer_id_api',
                    'value' => $locationid,
                    'compare' => '='
                )
                
            )
        );

        $my_posts = get_posts($query_args_meta);
        if( $my_posts ) :
        //	echo 'ID on the first post found ' . $my_posts[0]->ID;
            $post   = get_post( $my_posts[0]->ID );
        endif;

        global $post;
      
        $curl = get_post_meta( $post->ID, 'wpsl_retailer_carpeturl', true );

        
        $capetlanding = '<span style="color: #8a2767;"><a class="salehyperlink" style="color: #8a2767;" href="'.home_url().'/'.$curl.'">Save on Shaw Carpet</a></span>';
                          
                         return $capetlanding;
    }
    add_shortcode('landingcarpeturl', 'landing_carpet_url');


     // Shortcode for Waterproof linking

     function landing_waterproof_url() {

        global $wpdb;

        $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

        $locationid = $url_path[5];
        $query_args_meta = array(
            'posts_per_page' => -1,
            'post_type' => 'wpsl_stores',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'wpsl_retailer_id_api',
                    'value' => $locationid,
                    'compare' => '='
                )
                
            )
        );

        $my_posts = get_posts($query_args_meta);
        if( $my_posts ) :
        //	echo 'ID on the first post found ' . $my_posts[0]->ID;
            $post   = get_post( $my_posts[0]->ID );
        endif;

        global $post;
      
        $curl = get_post_meta( $post->ID, 'wpsl_retailer_waterproofurl', true );

        
        $capetlanding = '<span style="color: #8a2767;"><a class="salehyperlink" style="color: #8a2767;" href="'.home_url().'/'.$curl.'">Save on Shaw Waterproof Flooring</a></span>';
                          
                         return $capetlanding;
    }
    add_shortcode('landingwaterproofurl', 'landing_waterproof_url');

    // Shortcode for Flooring linking

    function landing_flooring_url() {

        global $wpdb;

        $url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

        $locationid = $url_path[5];
        $query_args_meta = array(
            'posts_per_page' => -1,
            'post_type' => 'wpsl_stores',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'wpsl_retailer_id_api',
                    'value' => $locationid,
                    'compare' => '='
                )
                
            )
        );

        $my_posts = get_posts($query_args_meta);
        if( $my_posts ) :
        //	echo 'ID on the first post found ' . $my_posts[0]->ID;
            $post   = get_post( $my_posts[0]->ID );
        endif;

        global $post;
      
        $curl = get_post_meta( $post->ID, 'wpsl_retailer_couponSeoUrl', true );

        
        $capetlanding = '<span style="color: #8a2767;"><a class="salehyperlink" style="color: #8a2767;" href="'.home_url().'/'.$curl.'">Save on Shaw Flooring</a></span>';
                          
                         return $capetlanding;
    }
    add_shortcode('landingflooringurl', 'landing_flooring_url');

    function writeToLog( $u ) {
        // this will create the log where the CODE of this function is
        // adjust it to write within the plugin directory
        $path = dirname(__FILE__) . '/log.txt';
        $agent = $_SERVER['HTTP_USER_AGENT'];
        if (($h = fopen($path, "a")) !== FALSE) {
            $mystring = $u . PHP_EOL;           
            fwrite( $h, $mystring );
            fclose($h);
        }
        
           
    }

    // code added for dynamic sale and thank you page images
function get_sale_image( $atts ) {
	$desktopImage = "";
	$mobileImage = "";	
	$saleType = $atts["type"];
	switch($saleType){
		case "flooring-all":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-hero-desktop.jpg";
			$mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-hero-mobile-1.jpg";
            break;	

        case "flooring-all-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-hero-desktop-1.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-hero-mobile-2.jpg";
                break;
            	
		case "flooring":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/flooring-hero-desktop.jpg";
			$mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/flooring-hero-mobile.jpg";
            break;

        case "flooring-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/flooring-hero-desktop-1.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/flooring-hero-mobile-1.jpg";
                break;
            
		case "waterproof-cortec":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-waterproof-hero-desktop.jpg";
			$mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-waterproof-hero-mobile.jpg";
            break;
        
        case "waterproof-cortec-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-waterproof-hero-desktop-.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-waterproof-hero-mobile-2.jpg";
                break;
            
		case "waterproof-flooring":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/waterproof-hero-desktop.jpg";
			$mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/waterproof-hero-mobile.jpg";
            break;
        
        case "waterproof-flooring-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/waterproof-hero-desktop-2.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/waterproof-hero-mobile-2.jpg";
                break;

        case "vinyl-flooring":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/vinyl-hero-desktop-1.jpg";
            $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/vinyl-hero-mobile.jpg";
            break;

        case "vinyl-flooring-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/vinyl-hero-desktop-2.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/vinyl-hero-mobile-1.jpg";
                break;

        case "vinyl-coretec":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-vinyl-hero-desktop-1.jpg";
            $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-vinyl-hero-mobile-1.jpg";
            break;
        
        case "vinyl-coretec-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-vinyl-hero-desktop-2.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-vinyl-hero-mobile-2.jpg";
                break;

        case "hardwood-flooring":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/hardwood-hero-desktop.jpg";
            $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/hardwood-hero-mobile.jpg";
            break;

        case "hardwood-flooring-canada":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/hardwood-hero-desktop-1.jpg";
            $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/hardwood-hero-mobile-1-1.jpg";
            break;

        case "carpet":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/carpet-hero-desktop.jpg";
            $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/carpet-hero-mobile.jpg";
            break;

        case "carpet-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/carpet-hero-desktop-1.jpg";
                $mobileImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/carpet-hero-mobile-1.jpg";
                break;
	}
	//$desktopImage =  "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-hero-desktop.jpg";
	return gethtml($desktopImage,$mobileImage, 'sale' );
}
add_shortcode( 'sale-image', 'get_sale_image' );




function get_thankyou_image( $atts ) {	

	$desktopImage = "";
	$mobileImage = "";	
	$saleType = $atts["type"];
	switch($saleType){
		
            case "flooring":
            $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/flooring-thank-you.jpg";
            
            case "flooring-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/flooring-thank-you-1.jpg";
			
            break;

            case "flooring-all":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-thank-you.jpg";
			            
            break;

            case "flooring-all-canada":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-flooring-thank-you-1.jpg";
			            
            break;

            case "carpet":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/carpet-thank-you.jpg";
                
                break;

            case "carpet-canada":
                    $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/carpet-thank-you-2.jpg";
                    
                    break;

            case "waterproof-flooring":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/waterproof-thank-you.jpg";
                
                break;
            
            case "waterproof-flooring-canada":
                    $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/waterproof-thank-you-1.jpg";
                    
                    break;

            case "waterproof-coretec":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-waterproof-thank-you.jpg";
                break;
            
            case "waterproof-coretec-canada":
                    $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-waterproof-thank-you-1.jpg";
                    break;

            case "vinyl-flooring":
                    $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/vinyl-thank-you.jpg";
                    break;
            
            case "vinyl-flooring-canada":
                        $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/vinyl-thank-you-1.jpg";
                        break;

            case "vinyl-coretec":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-vinyl-thank-you.jpg";
                break;

            case "vinyl-coretec-canada":
                    $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/all-vinyl-thank-you-1.jpg";
                    break;

            case "hardwood-flooring":
                $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/hardwood-thank-you.jpg";
                break;

            case "hardwood-flooring-canada":
                    $desktopImage = "https://flooringyouwell-stg.mm-dev.agency/wp-content/uploads/2020/09/hardwood-thank-you-1.jpg";
                    break;
	}
	
	return gethtml($desktopImage,$mobileImage, 'thankyou' );
}
add_shortcode( 'thankyou-image', 'get_thankyou_image' );


function gethtml( $imagepath,$mobileimage, $type ){
	if($type =="sale"){
		return '<div class=" fl-module-photo fl-visible-desktop-medium">	<div class="fl-module-content fl-node-content">		<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject"> 	<div class="fl-photo-content fl-photo-img-jpg">				<img class="fl-photo-img wp-image-3072 size-full" src="'.$imagepath.'" alt="SS_Flooring_All_Graphic" itemprop="image" height="730" width="491" srcset="'.$imagepath.' 491w" sizes="(max-width: 491px) 100vw, 491px" title="SS_Flooring_All_Graphic"> </div>	</div>	</div></div><div class="fl-module fl-module-photo fl-node-5ca6fd1f36ce0 fl-visible-mobile" data-node="5ca6fd1f36ce0"> 	<div class="fl-module-content fl-node-content"> 		<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject"> 	<div class="fl-photo-content fl-photo-img-jpg"> 				<img class="fl-photo-img wp-image-3073 size-full" src="'.$mobileimage.'" alt="SS_Flooring_All_Graphic_mobile" itemprop="image" height="246" width="320" title="SS_Flooring_All_Graphic_mobile" srcset="'.$mobileimage.' 320w" sizes="(max-width: 320px) 100vw, 320px"> 				</div>	</div>	</div></div>';
	}
	else if($type =="thankyou"){
		return '<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">	<div class="fl-photo-content fl-photo-img-png">				<img class="fl-photo-img wp-image-92519 size-full" src="'.$imagepath.'" alt="waterproof-coretech_canadian_thank-you" itemprop="image" height="259" width="614" srcset="'.$imagepath.' 614w" sizes="(max-width: 614px) 100vw, 614px" title="waterproof-coretech_canadian_thank-you">					</div>	</div>';
	}	
}


function sale_description_tagline( $atts ) {	

		
	$saleType = $atts["type"];
	switch($saleType){
		
            case "flooring":
			$sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Flooring purchases, at";
			
            break;

            case "flooring-all":
			$sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Flooring purchases, at";
			
            break;

            case "carpet":
            $sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Carpet purchases, at";
                
                break;

           case "hardwood-flooring":
            $sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Hardwood Flooring purchases, including waterproof Floorte at";
                        
                        break;
		
            case "waterproof-flooring":
            $sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Flooring purchases, including Floorte styles at";
                
                break;
          
            case "waterproof-coretec":
            $sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Waterproof Flooring purchases, including COREtec and Floorte styles at";
                
                break;
            
            case "vinyl-coretec":
            $sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Waterproof Flooring purchases, including COREtec and Floorte styles at";
                        
            break;

            case "vinyl-flooring":
                $sale_desc = "Fill out the form to save up to <strong>$1000</strong> off select Shaw Waterproof Flooring purchases, including Floorte styles at";
                            
            break;
           
	}
	
	return $sale_desc;
}
add_shortcode( 'sale-description', 'sale_description_tagline' );

//Choose your Colors Shortcode
function choose_your_color_fn() {
    ob_start();
    get_template_part('choose_your_color');
    return ob_get_clean(); 
}
add_shortcode( 'choose_your_color', 'choose_your_color_fn');

//Choose your Colors Shortcode
function choose_your_style_fn() {
    ob_start();
    get_template_part('choose_your_style');
    return ob_get_clean(); 
}
add_shortcode( 'choose_your_style', 'choose_your_style_fn');


add_filter( 'gform_field_value_formtype', 'populate_formtype' );
function populate_formtype( $value ) {
   return 'coupon';
}

add_filter( 'gform_field_value_promocode', 'populate_promocode' );
function populate_promocode( $value ) {
   return 'nwyrnwflr';
}

function featured_products_slider($atts) {

    // print_r($atts);
 
     $args = array(
         'post_type' => $atts['0'],        
         'posts_per_page' => 8,
         'orderby' => 'rand',
         'meta_query' => array(
             array(
                 'key' => 'collection', 
                 'value' => $atts['1'], 
                 'compare' => '=='
             )
         ) 
     );
     $query = new WP_Query( $args );
     if( $query->have_posts() ){
         $outpout = '<div class="featured-products"><div class="featured-product-list">';
         while ($query->have_posts()) : $query->the_post(); 
             $gallery_images = get_field('gallery_room_images');
             $gallery_img = explode("|",$gallery_images);
             $count = 0;
             // loop through the rows of data
             foreach($gallery_img as  $key=>$value) {
                 $room_image = $value;
                 if(!$room_image){
                     $room_image = "http://placehold.it/245x310?text=No+Image";
                 }
                 else{
                     if(strpos($room_image , 's7.shawimg.com') !== false){
                         if(strpos($room_image , 'http') === false){ 
                             $room_image = "http://" . $room_image;
                         }
                         $room_image = $room_image ;
                     } else{
                         if(strpos($room_image , 'http') === false){ 
                             $room_image = "https://" . $room_image;
                         }
                         $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
                     }
                 }
                 if($count>0){
                     break;
                 }
                 $count++;
             }
 
             $color_name = get_field('color');
             $brand_name = get_field('brand');
             $collection = get_field('collection');
             $post_type = get_post_type_object(get_post_type( get_the_ID() ));
             
             $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="'.$room_image.'" alt="'.get_the_title().'" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="'.get_the_permalink().'" class="button alt">View Product</a>
                                 <a href="/flooring-coupon/" class="button">Get Coupon</a>
                             </div>
                         </div>
                     </div>
                     <div class="product-info">
                         <h5><a href="'.get_the_permalink().'">'.$collection.'</a></h5>
                         <h6>'.$color_name.'</h6>
                     </div>
                 </div>
             </div>';
 
         endwhile;
         $outpout .= '</div></div>';
         wp_reset_query();
     }  
 
 
     return $outpout;
 }
 add_shortcode( 'featured_products_slider', 'featured_products_slider' );


 
/////////Flooring/Flooring all

  if (!wp_next_scheduled('create_flooring_page_retailerjob')) {    
     
   // wp_schedule_event( time(), 'each_saturday', 'create_flooring_page_retailerjob');
 }

add_action( 'create_flooring_page_retailerjob', 'create_flooring_page' ); 

function create_flooring_page(){

    write_log('<------------Started flooring function------------>');

    $page_title = '';
    $page_name = '';

    $page_title_finance = '';
    $page_name_finance = '';

    $storeposts = get_posts([
        'post_type' => 'wpsl_stores',
        'post_status' => 'publish',
        'numberposts' => -1
     ]);

    

     
 
     $i = 1;
    foreach($storeposts as $store){

        

        $retailerid = get_post_meta( $store->ID, 'wpsl_retailer_id_api', true );
        write_log($store->ID.'-------'.$retailerid );
        $sale = get_post_meta( $store->ID, 'wpsl_retailer_sale', true );
        $coretec = get_post_meta( $store->ID, 'wpsl_retailer_coretecVinyl', true );
        $canada = get_post_meta( $store->ID, 'wpsl_retailer_canada', true );
        $financing = get_post_meta( $store->ID, 'wpsl_retailer_financing', true );
        $paid_search = get_post_meta( $store->ID, 'wpsl_retailer_paid_search', true );
        $couponUrlSem = get_post_meta( $store->ID, 'wpsl_retailer_couponUrlSem', true );
        // write_log('retailerid--'.$retailerid);
        // write_log('sale--'.$sale);
        // write_log('coretec--'.$coretec);
        // write_log('canada--'.$canada);
        // write_log('financing--'.$financing);

       

        if($sale ==  '1' && $coretec == '1' ){
            
            $page_title = 'Flooring All';
            $page_name = 'flooring-all';
    
            }
        elseif($sale ==  '1' && $coretec == '' ){
    
                $page_title = 'Flooring';
                $page_name = 'flooring';
            }
    


        write_log('<------------Current Loop ->'.$i.'------------>');

        write_log('------------Retailer ID ->'.$retailerid.'->'.get_the_title($store->ID).'------------');

        $check_retailer = get_page_by_title($retailerid, OBJECT, 'page');

        $check_flooring_page = 'false';

        $args = array(            
            'post_parent'    => $check_retailer->ID,
            'post_type'      => 'page',
            'post_status'   => 'publish',
        );

        $child_pages = new WP_Query( $args );

            if ( $child_pages->have_posts() ) {

                    while ( $child_pages->have_posts() ) {

                        $child_pages->the_post();

                        $childtitle = get_the_title();
                        
                        if($childtitle == $page_title){

                            $check_flooring_page = 'true';

                            write_log('<----flooring  page already inserted--->');

                        }          
                    
                    }
            }

        if( $check_retailer != null &&  $check_flooring_page != 'true'){

            write_log('Retailer Landing Page-> ID ->'.$check_retailer->ID.'---title->'.get_the_title($check_retailer->ID).'---------->');

            // Gather post data.
           $flooring_post_contet_data = '';   
    
           $my_flooring = array(
               'post_title'    => $page_title,
               'post_name'     => $page_name,
               'post_content'  => $flooring_post_contet_data,
               'post_type'     => 'page',               
               'post_status'   => 'publish',
               'post_parent'   => $check_retailer->ID,
               'post_author'  => 1,               
               'comment_status' => 'closed',   // if you prefer
               'ping_status' => 'closed'
               
           );
           
             write_log('flooring'.$page_title.'-----'.$page_name);

            // Flooring Insert the post into the database.
            $add_flooring = wp_insert_post( $my_flooring );  
            
            update_post_meta( $add_flooring, '_wp_page_template', 'template-retailer-flooring.php' );
            update_post_meta( $add_flooring, '_fl_builder_enabled', '1' );          

            write_log('New Retailer Flooring  Page->'.get_permalink($add_flooring));


            if($couponUrlSem !='' || $couponUrlSem != null ){
            
                $flooring_sale_data = '';   

                $my_flooring_sale = array(
                    'post_title'    => 'Sale',
                    'post_name'     => 'sale',
                    'post_content'  => $flooring_sale_data,
                    'post_type'     => 'page',               
                    'post_status'   => 'publish',
                    'post_parent'   => $add_flooring,
                    'post_author'  => 1,               
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'
                    
                );

                $flooring_sale = wp_insert_post( $my_flooring_sale );  

                write_log('New Retailer Flooring sale Page->'.get_permalink($flooring_sale));
            
                update_post_meta( $flooring_sale, '_wp_page_template', 'template-retailer-flooring-sale.php' );
                update_post_meta( $flooring_sale, '_fl_builder_enabled', '1' );     

            }

       }

        write_log('<------------End ->'.$retailerid.'<- End------------->');

        //exit;

        if($i % 100==0){ sleep(10);}

       $i++;

      // exit;

     }

     write_log('<------------Ended with flooring function------------>');
}


/////////Carpet

if (!wp_next_scheduled('create_carpet_page_retailerjob')) {    
     
  //  wp_schedule_event( time(), 'each_saturday', 'create_carpet_page_retailerjob');
}

add_action( 'create_carpet_page_retailerjob', 'create_carpet_page' ); 

function create_carpet_page(){

   write_log('<------------Started function for carpet pages------------>');

   
   $storeposts = get_posts([
       'post_type' => 'wpsl_stores',
       'post_status' => 'publish',
       'numberposts' => -1
    ]);

   // write_log($storeposts);

    

    $i = 1;
    foreach($storeposts as $store){

        $wpsl_retailer_carpeturl = get_post_meta( $store->ID, 'wpsl_retailer_carpeturl', true );

    if($wpsl_retailer_carpeturl !=  null ){

       $retailerid = get_post_meta( $store->ID, 'wpsl_retailer_id_api', true );
       $sale = get_post_meta( $store->ID, 'wpsl_retailer_sale', true );
       $coretec = get_post_meta( $store->ID, 'wpsl_retailer_coretecVinyl', true );
       $canada = get_post_meta( $store->ID, 'wpsl_retailer_canada', true );
       $financing = get_post_meta( $store->ID, 'wpsl_retailer_financing', true );

       

    //    write_log('retailerid--'.$retailerid);
    //    write_log('sale--'.$sale);
    //    write_log('coretec--'.$coretec);
    //    write_log('canada--'.$canada);
    //    write_log('financing--'.$financing);

      
           
           $page_title_carpet = 'Carpet';
           $page_name_carpet = 'carpet';
       

       write_log('<------------Current Loop ->'.$i.'------------>');

       write_log('------------Retailer ID ->'.$retailerid.'->'.get_the_title($store->ID).'------------');

       $check_retailer = get_page_by_title($retailerid, OBJECT, 'page');

       $check_carpet_page = 'false';

       $args = array(            
           'post_parent'    => $check_retailer->ID,
           'post_type'      => 'page',
           'post_status'   => 'publish',
       );

       $child_pages = new WP_Query( $args );

           if ( $child_pages->have_posts() ) {

                   while ( $child_pages->have_posts() ) {

                       $child_pages->the_post();

                       $childtitle = get_the_title();
                       
                       if($childtitle == $page_title_carpet){

                           $check_carpet_page = 'true';

                           write_log('<----carpet page already inserted--->');

                       }          
                   
                   }
           }

     if( $check_retailer != null &&  $check_carpet_page!='true'){

           write_log('Retailer Landing Page-> ID ->'.$check_retailer->ID.'---title->'.get_the_title($check_retailer->ID).'---------->');

           // Gather post data.
          $flooring_post_contet_data = '';   
   
          $my_flooring = array(
              'post_title'    => $page_title_carpet,
              'post_name'     => $page_name_carpet,
              'post_content'  => $flooring_post_contet_data,
              'post_type'     => 'page',               
              'post_status'   => 'publish',
              'post_parent'   => $check_retailer->ID,
              'post_author'  => 1,               
              'comment_status' => 'closed',   // if you prefer
              'ping_status' => 'closed'
              
          );          

       write_log('flooring'.$page_title.'-----'.$page_name);

           // Flooring Insert the post into the database.
           $add_flooring = wp_insert_post( $my_flooring );  
           
           update_post_meta( $add_flooring, '_wp_page_template', 'template-carpet.php' );
           update_post_meta( $add_flooring, '_fl_builder_enabled', '1' );         

           write_log('New Retailer carpet Page->'.get_permalink($add_flooring));

      }

       write_log('<------------End ->'.$retailerid.'<- End------------->');

       //exit;

      

    }
    $i++;
    }

    write_log('<------------Ended with carpet function------------>');
}


/////////hardwood

if (!wp_next_scheduled('create_hardwood_page_retailerjob')) {    
     
   // wp_schedule_event( time(), 'each_saturday', 'create_hardwood_page_retailerjob');
}

add_action( 'create_hardwood_page_retailerjob', 'create_hardwood_page' ); 

function create_hardwood_page(){

   write_log('<------------Started function for hardwood pages------------>');

   
   $storeposts = get_posts([
       'post_type' => 'wpsl_stores',
       'post_status' => 'publish',
       'numberposts' => -1
    ]);

 //   write_log($storeposts);

    

    $i = 1;
    foreach($storeposts as $store){

        $wpsl_retailer_hardwoodUrl = get_post_meta( $store->ID, 'wpsl_retailer_hardwoodUrl', true );

        if($wpsl_retailer_hardwoodUrl !=  null ){

       $retailerid = get_post_meta( $store->ID, 'wpsl_retailer_id_api', true );
       $sale = get_post_meta( $store->ID, 'wpsl_retailer_sale', true );
       $coretec = get_post_meta( $store->ID, 'wpsl_retailer_coretecVinyl', true );
       $canada = get_post_meta( $store->ID, 'wpsl_retailer_canada', true );
       $financing = get_post_meta( $store->ID, 'wpsl_retailer_financing', true );
       $paid_search = get_post_meta( $store->ID, 'wpsl_retailer_paid_search', true );

    //    write_log('retailerid--'.$retailerid);
    //    write_log('sale--'.$sale);
    //    write_log('coretec--'.$coretec);
    //    write_log('canada--'.$canada);
    //    write_log('financing--'.$financing);

      //if($sale ==  '1' ){
           
           $page_title_hard = 'Hardwood Flooring';
           $page_name_hard = 'hardwood-flooring';
       //}

       write_log('<------------Current Loop ->'.$i.'------------>');

       write_log('------------Retailer ID ->'.$retailerid.'->'.get_the_title($store->ID).'------------');

       $check_retailer = get_page_by_title($retailerid, OBJECT, 'page');

       $check_hard_page = 'false';

       $args = array(            
           'post_parent'    => $check_retailer->ID,
           'post_type'      => 'page',
           'post_status'   => 'publish',
       );

       $child_pages = new WP_Query( $args );

           if ( $child_pages->have_posts() ) {

                   while ( $child_pages->have_posts() ) {

                       $child_pages->the_post();

                       $childtitle = get_the_title();
                       
                       if($childtitle == $page_title_hard){

                           $check_carpet_page = 'true';

                           write_log('<----hardwood flooring page already inserted--->');

                       }          
                   
                   }
           }

     if( $check_retailer != null &&  $check_carpet_page!='true'){

           write_log('Retailer Landing Page-> ID ->'.$check_retailer->ID.'---title->'.get_the_title($check_retailer->ID).'---------->');

           // Gather post data.
          $flooring_post_contet_data = '';   
   
          $my_flooring = array(
              'post_title'    => $page_title_hard,
              'post_name'     => $page_name_hard,
              'post_content'  => $flooring_post_contet_data,
              'post_type'     => 'page',               
              'post_status'   => 'publish',
              'post_parent'   => $check_retailer->ID,
              'post_author'  => 1,               
              'comment_status' => 'closed',   // if you prefer
              'ping_status' => 'closed'
              
          );          

       write_log('flooring'.$page_title.'-----'.$page_name);

           // Flooring Insert the post into the database.
           $add_flooring = wp_insert_post( $my_flooring );  
           
           update_post_meta( $add_flooring, '_wp_page_template', 'template-hardwood.php' );
           update_post_meta( $add_flooring, '_fl_builder_enabled', '1' );         

           write_log('New Retailer Hardwood Page->'.get_permalink($add_flooring));

      }

       write_log('<------------End ->'.$retailerid.'<- End------------->');

       //exit;

      $i++;

      //exit;
    }
    $i++;

    }

    write_log('<------------Ended with hardwood function------------>');
}



/////////waterproof-flooring

if (!wp_next_scheduled('create_waterproof_page_retailerjob')) {    
     
   // wp_schedule_event( time(), 'each_saturday', 'create_waterproof_page_retailerjob');
}

add_action( 'create_waterproof_page_retailerjob', 'create_waterproof_page' ); 

function create_waterproof_page(){

   write_log('<------------Started function for waterproof pages------------>');

   
   $storeposts = get_posts([
       'post_type' => 'wpsl_stores',
       'post_status' => 'publish',
       'numberposts' => -1
    ]);

   // write_log($storeposts);

    

    $i = 1;
    foreach($storeposts as $store){

        $wpsl_retailer_waterproofurl = get_post_meta( $store->ID, 'wpsl_retailer_waterproofurl', true );

      if($wpsl_retailer_waterproofurl !=  null ){

       $retailerid = get_post_meta( $store->ID, 'wpsl_retailer_id_api', true );
       $sale = get_post_meta( $store->ID, 'wpsl_retailer_sale', true );
       $coretec = get_post_meta( $store->ID, 'wpsl_retailer_coretecVinyl', true );
       $canada = get_post_meta( $store->ID, 'wpsl_retailer_canada', true );
       $financing = get_post_meta( $store->ID, 'wpsl_retailer_financing', true );

    //    write_log('retailerid--'.$retailerid);
    //    write_log('sale--'.$sale);
    //    write_log('coretec--'.$coretec);
    //    write_log('canada--'.$canada);
    //    write_log('financing--'.$financing);

      if($sale ==  '1' && $coretec == ''){
           
           $page_title_water = 'Waterproof Flooring';
           $page_name_water = 'waterproof-flooring';

       }elseif($sale ==  '1' && $coretec == '1'){

            $page_title_water = 'Waterproof Coretec';
            $page_name_water = 'waterproof-coretec';

       }

       write_log('<------------Current Loop ->'.$i.'------------>');

       write_log('------------Retailer ID ->'.$retailerid.'->'.get_the_title($store->ID).'------------');

       $check_retailer = get_page_by_title($retailerid, OBJECT, 'page');

       $check_water_page = 'false';

       $args = array(            
           'post_parent'    => $check_retailer->ID,
           'post_type'      => 'page',
           'post_status'   => 'publish',
       );

       $child_pages = new WP_Query( $args );

           if ( $child_pages->have_posts() ) {

                   while ( $child_pages->have_posts() ) {

                       $child_pages->the_post();

                       $childtitle = get_the_title();
                       
                       if($childtitle == $page_title_water){

                           $check_water_page = 'true';

                           write_log('<----waterproof flooring page already inserted--->');

                       }          
                   
                   }
           }

     if( $check_retailer != null &&  $check_water_page!='true'){

           write_log('Retailer Landing Page-> ID ->'.$check_retailer->ID.'---title->'.get_the_title($check_retailer->ID).'---------->');

           // Gather post data.
          $flooring_post_contet_data = '';   
   
          $my_flooring = array(
              'post_title'    => $page_title_water,
              'post_name'     => $page_name_water,
              'post_content'  => $flooring_post_contet_data,
              'post_type'     => 'page',               
              'post_status'   => 'publish',
              'post_parent'   => $check_retailer->ID,
              'post_author'  => 1,               
              'comment_status' => 'closed',   // if you prefer
              'ping_status' => 'closed'
              
          );          

       write_log('flooring'.$page_title.'-----'.$page_name);

           // Flooring Insert the post into the database.
           $add_flooring = wp_insert_post( $my_flooring );  
           
           update_post_meta( $add_flooring, '_wp_page_template', 'template-waterproof-coretecflooring.php' );
           update_post_meta( $add_flooring, '_fl_builder_enabled', '1' );         

           write_log('New Retailer waterproof Page->'.get_permalink($add_flooring));

      }

       write_log('<------------End ->'.$retailerid.'<- End------------->');

       //exit;

      

     // exit;
     }

     $i++;

    }

    write_log('<------------Ended with waterproof function------------>');
}

/////////vinyl-flooring

if (!wp_next_scheduled('create_vinyl_page_retailerjob')) {    
     
    wp_schedule_event( time(), 'each_saturday', 'create_vinyl_page_retailerjob');
}

add_action( 'create_vinyl_page_retailerjob', 'create_vinyl_page' ); 

function create_vinyl_page(){

   write_log('<------------Started function for vinyl pages------------>');

   
   $storeposts = get_posts([
       'post_type' => 'wpsl_stores',
       'post_status' => 'publish',
       'numberposts' => -1
    ]);

   // write_log($storeposts);

    

    $i = 1;
    foreach($storeposts as $store){

        $wpsl_retailer_vinylUrl = get_post_meta( $store->ID, 'wpsl_retailer_vinylUrl', true );

        if($wpsl_retailer_vinylUrl !=  null ){

       $retailerid = get_post_meta( $store->ID, 'wpsl_retailer_id_api', true );
       $sale = get_post_meta( $store->ID, 'wpsl_retailer_sale', true );
       $coretec = get_post_meta( $store->ID, 'wpsl_retailer_coretecVinyl', true );
       $canada = get_post_meta( $store->ID, 'wpsl_retailer_canada', true );
       $financing = get_post_meta( $store->ID, 'wpsl_retailer_financing', true );

    //    write_log('retailerid--'.$retailerid);
    //    write_log('sale--'.$sale);
    //    write_log('coretec--'.$coretec);
    //    write_log('canada--'.$canada);
    //    write_log('financing--'.$financing);

      if($sale ==  '1' && $coretec == ''){
           
           $page_title_vinyl = 'Vinyl Flooring';
           $page_name_vinyl = 'vinyl-flooring';

       }elseif($sale ==  '1' && $coretec == '1'){

            $page_title_vinyl = 'Vinyl Coretec';
            $page_name_vinyl = 'vinyl-coretec';

       }

       write_log('<------------Current Loop ->'.$i.'------------>');

       write_log('------------Retailer ID ->'.$retailerid.'->'.get_the_title($store->ID).'------------');

       $check_retailer = get_page_by_title($retailerid, OBJECT, 'page');

       $check_vinyl_page = 'false';

       $args = array(            
           'post_parent'    => $check_retailer->ID,
           'post_type'      => 'page',
           'post_status'   => 'publish',
       );

       $child_pages = new WP_Query( $args );

           if ( $child_pages->have_posts() ) {

                   while ( $child_pages->have_posts() ) {

                       $child_pages->the_post();

                       $childtitle = get_the_title();
                       
                       if($childtitle == $page_title_vinyl){

                           $check_vinyl_page = 'true';

                           write_log('<----vinyl flooring page already inserted--->');

                       }          
                   
                   }
           }

     if( $check_retailer != null &&  $check_vinyl_page!='true'){

           write_log('Retailer Landing Page-> ID ->'.$check_retailer->ID.'---title->'.get_the_title($check_retailer->ID).'---------->');

           // Gather post data.
          $flooring_post_contet_data = '';   
   
          $my_flooring = array(
              'post_title'    => $page_title_vinyl,
              'post_name'     => $page_name_vinyl,
              'post_content'  => $flooring_post_contet_data,
              'post_type'     => 'page',               
              'post_status'   => 'publish',
              'post_parent'   => $check_retailer->ID,
              'post_author'  => 1,               
              'comment_status' => 'closed',   // if you prefer
              'ping_status' => 'closed'
              
          );          

       write_log('flooring'.$page_title.'-----'.$page_name);

           // Flooring Insert the post into the database.
           $add_flooring = wp_insert_post( $my_flooring );  
           
           update_post_meta( $add_flooring, '_wp_page_template', 'template-vinylflooring.php' );
           update_post_meta( $add_flooring, '_fl_builder_enabled', '1' );         

           write_log('New Retailer vinyl Page->'.get_permalink($add_flooring));

      }

       write_log('<------------End ->'.$retailerid.'<- End------------->');

       //exit;
    }

      $i++;

      //exit;

    }

    write_log('<------------Ended with vinyl function------------>');
}

add_shortcode( 'thankyoufield', 'thankyoufield_func' );
function thankyoufield_func($arg) {


    if($_GET['entry'] != ''){

        $entry_id = $_GET['entry'];
        $entry = GFAPI::get_entry( $entry_id );

        if($arg[0] == 'email'){

            $content = $entry[3];

        }elseif($arg[0] == 'phone'){

            $content = $entry[4];

        }elseif($arg[0] == 'name'){

            $content = $entry[1].' '.$entry[2];

        }elseif($arg[0] == 'couponcode'){

            $content = $entry[23];
        }        

    }    

    return $content;
}

//add_shortcode( 'thankyoushawcouponcode', 'thankyoushawcouponcode_func' );
function thankyoushawcouponcode_func($arg) {

    $url = 'https://sso.mm-api.agency/oauth/token?grant_type=client_credentials&client_id=wordpress@canwilltech.com&client_secret=NYESqMvXRHCpIsWHpHGkTnWm';
    
    $authresponse = wp_remote_post($url,array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
                    'httpversion' => '1.0',
                    'blocking' => true,               
                    'cookies' => array(),
                    'body' =>''
                    )
                );

    $responceData = json_decode( $authresponse['body'], true);
   // write_log($responceData['access_token']);

    if(isset($responceData['error'])){

        echo $responceData['error'];

    }else if(isset($responceData['access_token'])){

        if($_GET['entry'] != ''){

                $entry_id = $_GET['entry'];
                $entry = GFAPI::get_entry( $entry_id );

                write_log($entry);

                $url_coupon = 'https://crm.mm-api.agency/coupon/code?email='.$entry[3].'&locationCode='.$entry[18].'&firstName='.$entry[1].'&lastName='.$entry[2].'&fyw=true';
                
                $headers = array('authorization'=>"bearer ".$responceData['access_token']);
               
                $response = wp_remote_get($url_coupon,array(
                    'method' => 'GET',
                    'timeout' => 45,
                    'redirection' => 5,
                    'headers' =>$headers,
                    'httpversion' => '1.0',
                    'blocking' => true,               
                    'cookies' => array(),
                    'body' =>''
                    )
                );
                write_log($response['body']);

                $coupon_response = json_decode( $response['body'], true);
                $coupon_code = strtoupper($coupon_response['couponCode']);

                gform_add_meta($entry['id'], '23', $coupon_code, $entry['form_id']);
                return $coupon_code;
        }
    }    
}

add_action( 'gform_pre_submission', 'pre_submission_handler' );
function pre_submission_handler( $form ) {

    write_log($_POST);
    //write_log($form);

  // if( $form['id'] == 12 ){

    write_log('asasd');

    $url = 'https://sso.mm-api.agency/oauth/token?grant_type=client_credentials&client_id=wordpress@canwilltech.com&client_secret=NYESqMvXRHCpIsWHpHGkTnWm';
    
    $authresponse = wp_remote_post($url,array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
                    'httpversion' => '1.0',
                    'blocking' => true,               
                    'cookies' => array(),
                    'body' =>''
                    )
                );

    $responceData = json_decode( $authresponse['body'], true);
   // write_log($responceData['access_token']);

    if(isset($responceData['error'])){

        echo $responceData['error'];

    }else if(isset($responceData['access_token'])){

        if($_POST['input_21'] == 'coupon-shaw'){

             //   $entry_id = $_GET['entry'];
             //   $entry = GFAPI::get_entry( $entry_id );

              //  write_log($entry);

                $url_coupon = 'https://crm.mm-api.agency/coupon/code?email='.$_POST['input_3'].'&locationCode='.$_POST['input_18'].'&firstName='.$_POST['input_1'].'&lastName='.$_POST['input_2'];

              write_log('urlcoupon-------'.$url_coupon);
                
                $headers = array('authorization'=>"bearer ".$responceData['access_token']);
               
                $response = wp_remote_get($url_coupon,array(
                    'method' => 'GET',
                    'timeout' => 45,
                    'redirection' => 5,
                    'headers' =>$headers,
                    'httpversion' => '1.0',
                    'blocking' => true,               
                    'cookies' => array(),
                    'body' =>''
                    )
                );
                write_log($response['body']);

                $coupon_response = json_decode( $response['body'], true);
                $coupon_code = strtoupper($coupon_response['couponCode']);

                //gform_add_meta($entry['id'], '23', $coupon_code, $entry['form_id']);
                $_POST['input_23'] = $coupon_code;
                write_log($_POST);
      //  }
    } 

   }
}


if (!wp_next_scheduled('create_retailer_store_retailerjob')) {    
     
    wp_schedule_event( time(), 'each_saturday', 'create_retailer_store_retailerjob');
}

add_action( 'create_retailer_store_retailerjob', 'create_store_page' ); 

function create_store_page(){

    global $wpdb;

   write_log('<------------Started Retailer store function------------>');  

  $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers" );

    $i = 1;
    foreach($new_retailers as $retailers){      

        write_log('Current loop -->'.$i );
        write_log('Retailer -->'.$retailers->Name );

        $args = array(
            'posts_per_page'   => 1,
            'post_type' => 'wpsl_stores',
            'post_status' => 'publish',            
            'meta_query' => array(
                array(
                    'key'     => 'wpsl_retailer_id_api',
                    'value'   => $retailers->LocationCode,
                    'compare' => '=',
                ),
            ),
        );

        $store_posts = get_posts( $args );


        if($store_posts == ''){

            write_log('Not found any retailer for -->'.$retailers->LocationCode );

        }else{

            write_log('found retailer for -->'.$retailers->LocationCode.'-------'.$store_posts->ID );
        }       

        $store_posts =  cvf_convert_object_to_array($store_posts);

       
        write_log('POst ID ------------>'.$store_posts[0]['ID']);
       // exit;

       $part   = implode('/', explode('/', $retailers->couponUrlSeo, -1));

       $store_url = $part.'/';

      
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_storeurl', $store_url );
     

      if($retailers->affiliateCode != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_affiliateCode', $retailers->affiliateCode );
      }

      if($retailers->PaidSearch == 'yes'){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_paid_search', '1' );
      }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_paid_search', '' );
      }

      if($retailers->coretec == 'yes'){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_coretecVinyl', '1' );
      }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_coretecVinyl', '' );
      }

       if($retailers->Country == 'CA'){
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_canada', '1' );
       }

       if($retailers->Sale == 'yes'){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_sale', '1' );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_sale', '' );
      }

       if($retailers->carpetUrl != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_carpeturl', $retailers->carpetUrl );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_carpeturl', '' );
      }

       if($retailers->couponUrlSeo != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_couponSeoUrl', $retailers->couponUrlSeo );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_couponSeoUrl', '' );
      }

       if($retailers->waterproofUrl != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_waterproofurl', $retailers->waterproofUrl );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_waterproofurl', '' );
      }

       if($retailers->couponUrlSem != '' ){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_couponUrlSem', $retailers->couponUrlSem );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_couponUrlSem', '' );
      }

       if($retailers->hardwoodUrl != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_hardwoodUrl', $retailers->hardwoodUrl );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_hardwoodUrl', '' );
      }

       if($retailers->vinylUrl != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_vinylUrl', $retailers->vinylUrl );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_vinylUrl', '' );
      }

       if($retailers->ForwardingPhone != ''){
       update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_forwardingphnumber', $retailers->ForwardingPhone );
       }else{
        update_post_meta( $store_posts[0]['ID'], 'wpsl_retailer_forwardingphnumber', '' );
      }
      
       if($i % 100==0){ sleep(5);}
       //exit;
       $i++;
    }


}

function cvf_convert_object_to_array($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}

if (!wp_next_scheduled('removeextra_retailer_store_retailerjob')) {    
     
   // wp_schedule_event( time(), 'each_saturday', 'removeextra_retailer_store_retailerjob');
}

add_action( 'removeextra_retailer_store_retailerjob', 'remove_store_page' ); 

function remove_store_page(){

    global $wpdb;

    write_log('<------------Started Retailer store function------------>');  
 
   
 
     $i = 1;

     $args = array(
        'posts_per_page'   => -1,
        'post_type' => 'wpsl_stores',
        'post_status' => 'publish'           
        
    );

    $store_posts = get_posts( $args );

     foreach($store_posts as $storepost){    
         
        write_log('Current loop'.$i);

        $retailerid = get_post_meta( $storepost->ID, 'wpsl_retailer_id_api', true );

        write_log('retailerid------'.$retailerid);

        $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers WHERE LocationCode = $retailerid " );

       // write_log($new_retailers);

        if($wpdb->num_rows > 0 ){

           
            
        }else{

            write_log($retailerid.'----not found-------'.$storepost->ID);
        }

        $i++;
       // exit;

     }

     write_log('<------------ended Retailer store function------------>');  

}


if (!wp_next_scheduled('addextra_retailer_store_retailerjob')) {    
     
   // wp_schedule_event( time(), 'each_saturday', 'addextra_retailer_store_retailerjob');
}

add_action( 'addextra_retailer_store_retailerjob', 'add_store_page' ); 

function add_store_page(){
    global $wpdb;

    $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers" );

    $i = 1;
    foreach($new_retailers as $retailers){      

        write_log('Current loop -->'.$i );
       // write_log('Retailer -->'.$retailers->Name );

        $args = array(
            'posts_per_page'   => 1,
            'post_type' => 'wpsl_stores',
            'post_status' => 'publish',            
            'meta_query' => array(
                array(
                    'key'     => 'wpsl_retailer_id_api',
                    'value'   => $retailers->LocationCode,
                    'compare' => '=',
                ),
            ),
        );

        $store_posts = get_posts( $args );


        if($store_posts ){

           
            
        }else{

            write_log($retailers->LocationCode.'----not found-------');
            write_log('Creating store----------'.$retailers->Name.'----'.$retailers->LocationCode);

            $flooring_post_contet_data = '';   
   
          $my_store_data = array(
              'post_title'    => $retailers->Name,
              'post_name'     => sanitize_title($retailers->Name),
              'post_content'  => $flooring_post_contet_data,
              'post_type'     => 'wpsl_stores',               
              'post_status'   => 'publish',             
              'post_author'  => 1,               
              'comment_status' => 'closed',   // if you prefer
              'ping_status' => 'closed'
              
          );      

           // Flooring Insert the post into the database.
           $add_store = wp_insert_post( $my_store_data );  

           write_log('Store post id'.$add_store);

           update_post_meta( $add_store, 'wpsl_address', $retailers->Address1 );
           update_post_meta( $add_store, 'wpsl-address2', $retailers->Address2 );
           update_post_meta( $add_store, 'wpsl_state', $retailers->State );
           update_post_meta( $add_store, 'wpsl_city', $retailers->City );
           update_post_meta( $add_store, 'wpsl_email', $retailers->Email );
           update_post_meta( $add_store, 'wpsl_zip', $retailers->Zip );
           update_post_meta( $add_store, 'wpsl_retailer_id_api', $retailers->LocationCode );
           update_post_meta( $add_store, 'wpsl_country', $retailers->Country );
           update_post_meta( $add_store, 'wpsl_retailer_forwardingphnumber', $retailers->ForwardingPhone );
           update_post_meta( $add_store, 'wpsl_retailer_carpeturl', $retailers->carpetUrl );
           update_post_meta( $add_store, 'wpsl_retailer_couponSeoUrl', $retailers->couponUrlSeo );
           update_post_meta( $add_store, 'wpsl_retailer_waterproofurl', $retailers->waterproofUrl );
           update_post_meta( $add_store, 'wpsl_retailer_storeurl', $retailers->couponUrlSeo );
           update_post_meta( $add_store, 'wpsl_country_iso', $retailers->Country );
           update_post_meta( $add_store, 'wpsl_lat', $retailers->Latitude );
           update_post_meta( $add_store, 'wpsl_lng', $retailers->Longitude );
           update_post_meta( $add_store, 'wpsl_retailer_affiliateCode', $retailers->affiliateCode );
           if($retailers->PaidSearch == 'yes'){
            update_post_meta( $add_store, 'wpsl_retailer_paid_search', '1' );
            }
            if($retailers->coretec == 'yes'){
                update_post_meta( $add_store, 'wpsl_retailer_coretecVinyl', '1' );
            }
            if($retailers->Sale == 'yes'){
                update_post_meta( $add_store, 'wpsl_retailer_sale', '1' );
            }
            if($retailers->financing == 'yes'){
                update_post_meta( $add_store, 'wpsl_retailer_financing', '1' );
            }                      
           if($retailers->Country == 'CA'){
            update_post_meta( $add_store, 'wpsl_retailer_canada', '1' );
           }
           
           update_post_meta( $add_store, 'wpsl_retailer_couponUrlSem', $retailers->couponUrlSem );
           update_post_meta( $add_store, 'wpsl_retailer_hardwoodUrl', $retailers->hardwoodUrl );
           update_post_meta( $add_store, 'wpsl_retailer_vinylUrl', $retailers->vinylUrl );

           $zip = explode("/",$retailers->couponUrlSeo);

           write_log($zip[2]);
 
         $zip_pages = get_page_by_slug($zip[2]);
 
         if ( $zip_pages ) {       
             
            write_log($zip[2].'-------Found Page in data------'.$zip_pages->ID);

                $retailerlanding_data = array(
                    'post_title'    => $retailers->Name,
                    'post_name'     => $zip[3],
                    'post_content'  => '',
                    'post_type'     => 'page',               
                    'post_status'   => 'publish',             
                    'post_author'  => 1,   
                    'post_parent'  =>  $zip_pages->ID,   
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'
                    
                );      
      
                 // Flooring Insert the post into the database.
                 $add_store_page = wp_insert_post( $retailerlanding_data );
                 	
                 add_post_meta($add_store_page , '_wp_page_template' , 'single-stores-contact.php');

                 write_log('store page id---->'.$add_store_page);
                 write_log(get_permalink($add_store_page));

                 $retailer_location_data = array(
                    'post_title'    => $retailers->LocationCode,
                    'post_name'     => sanitize_title($retailers->LocationCode),
                    'post_content'  => '',
                    'post_type'     => 'page',               
                    'post_status'   => 'publish',             
                    'post_author'  => 1,   
                    'post_parent'  =>  $add_store_page,   
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'
                    
                );      
      
                 // Flooring Insert the post into the database.
                 $add_store_location_page = wp_insert_post( $retailer_location_data );
                 add_post_meta($add_store_location_page , '_wp_page_template' , 'template-location-id.php');
                 write_log('locstionpage id---->'.$add_store_location_page);
                 write_log(get_permalink($add_store_location_page));




            }else{
               
                write_log($zip[2].'-------Do not have zippage');
                $cityPage = get_page_by_title($retailers->City);

                if($cityPage){

                    write_log($cityPage->ID.'----'.$retailers->City.'---City found in data');

                }else{

                    write_log($retailers->City.'-------Do not have citypage');
                    $statePage = get_page_by_title($retailers->State);

                    if($statePage){

                        write_log($statePage->ID.'--'.$retailers->State.'-------state found in data');

    
                    }else{

                        write_log($retailers->State.'-------Do not have statepage');
                    }

                }
            

            }

           // exit;
        }


        $i++;
        

    }


}

// if (!wp_next_scheduled('state_retailer_store_retailerjob')) {    
     
//     wp_schedule_event( time(), 'each_saturday', 'state_retailer_store_retailerjob');
// }

// add_action( 'state_retailer_store_retailerjob', 'add_state_page' ); 

// function add_state_page(){
//     write_log('state cron running');

//     global $wpdb;

//     $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers" );

//     $i = 1;

//     foreach($new_retailers as $retailers){ 

//         //write_log('state cron runningsdsadasd');

//         $state_pages = get_page_by_title($retailers->State);       

//         if ( $state_pages ) {

//             write_log($retailers->State.'----state found');

//         }else{

//             write_log($retailers->State.'--------state not found');
//         }


//         $i++;
//     }
//     write_log('state cron ended');
// }

if (!wp_next_scheduled('zip_retailer_store_retailerjob')) {    
     
  //  wp_schedule_event( time(), 'each_saturday', 'zip_retailer_store_retailerjob');
}

add_action( 'zip_retailer_store_retailerjob', 'add_zip_page' ); 

function add_zip_page(){
    write_log('zip cron running');

    global $wpdb;

    $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers" );

    $i = 1;

    foreach($new_retailers as $retailers){ 

        //write_log('zip cron runningsdsadasd');

        $zip = explode("/",$retailers->couponUrlSeo);
        $zip2 = explode("-",$retailers->Zip);

               

        $Zip_pages = get_page_by_slug($zip[2]);
        

        if ( $Zip_pages ) {

            write_log($retailers->Zip.'----Zip found');

        }else{

            if($zip[2] == $zip2[0] ){


            $Citypage = get_page_by_title( $retailers->City );
            write_log('urlzip----->'.$zip[2].'Locationdata zip---->'.$retailers->Zip); 
            write_log('ID->'.$Citypage->ID.'-----'.$retailers->City.'-----'.$retailers->Zip.'--------zip not found');

            $retailer_zip_data = array(
                'post_title'    => $zip2[0],
                'post_name'     => sanitize_title($zip2[0]),
                'post_content'  => '',
                'post_type'     => 'page',               
                'post_status'   => 'publish',             
                'post_author'  => 1,   
                'post_parent'  =>  $Citypage->ID,   
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'
                
            );      
  
             // Flooring Insert the post into the database.
            // $add_store_zip_page = wp_insert_post( $retailer_zip_data );
            // add_post_meta($add_store_zip_page , '_wp_page_template' , 'template-zipcode.php');

             write_log('Post ->'.$add_store_zip_page);
             write_log(get_permalink($add_store_zip_page));


        }else{
            $Citypage = get_page_by_title( $retailers->City );
            write_log('Location->'.$retailers->LocationCode.'----City page ID->'.$Citypage->ID.'----->'.$zip[2].'<------->'.$zip2[0].'<-----did not machtch');
        }
    }


        $i++;
    }

    write_log('zip cron ended');
}


if (!wp_next_scheduled('city_retailer_store_retailerjob')) {    
     
   // wp_schedule_event( time(), 'each_saturday', 'city_retailer_store_retailerjob');
}

add_action( 'city_retailer_store_retailerjob', 'add_city_page' ); 
function add_city_page(){
    write_log('city cron running');

    global $wpdb;

    $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers" );

    $i = 1;

    foreach($new_retailers as $retailers){ 

        //write_log('zip cron runningsdsadasd');

        $City_pages = get_page_by_title($retailers->City);

        if ( $City_pages ) {

            write_log($retailers->City.'----City found');

        }else{
            $statepage = get_page_by_title( $retailers->State );
            write_log('ID->'.$statepage->ID.'-----'.$retailers->State.'-----'.$retailers->City.'--------City not found');

            $retailer_city_data = array(
                'post_title'    => $retailers->City,
                'post_name'     => sanitize_title($retailers->City),
                'post_content'  => '',
                'post_type'     => 'page',               
                'post_status'   => 'publish',             
                'post_author'  => 1,   
                'post_parent'  =>  $statepage->ID,   
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'
                
            );      
  
             // Flooring Insert the post into the database.
             $add_store_city_page = wp_insert_post( $retailer_city_data );
             update_post_meta( $add_store_city_page, '_wp_page_template', 'template-city.php' );
             update_post_meta( $add_store_city_page, '_fl_builder_enabled', '1' );  

             write_log(get_permalink($add_store_city_page));

        }


        $i++;
    }

    write_log('City cron ended');
}


function get_page_by_slug( $page_slug, $output = OBJECT, $post_type = 'page' ) {
	global $wpdb;

	if ( is_array( $post_type ) ) {
		$post_type = esc_sql( $post_type );
		$post_type_in_string = "'" . implode( "','", $post_type ) . "'";
		$sql = $wpdb->prepare( "
			SELECT ID
			FROM $wpdb->posts
			WHERE post_name = %s
			AND post_type IN ($post_type_in_string)
		", $page_slug );
	} else {
		$sql = $wpdb->prepare( "
			SELECT ID
			FROM $wpdb->posts
			WHERE post_name = %s
			AND post_type = %s
		", $page_slug, $post_type );
	}

	$page = $wpdb->get_var( $sql );

	if ( $page )
		return get_post( $page, $output );

	return null;
}


  if (!wp_next_scheduled('create_contact_us_page_retailerjob')) {    
     
    // wp_schedule_event( time(), 'each_saturday', 'create_contact_us_page_retailerjob');
 }

add_action( 'create_contact_us_page_retailerjob', 'create_contact_us_page' ); 

function create_contact_us_page(){
    

    write_log('<------------Started function------------>');

    global $wpdb;

    $new_retailers =  $wpdb->get_results( "SELECT * FROM retailers" );

    $i = 1;

    foreach($new_retailers as $retailers){ 

       

        $retailerid = get_post_meta( $retailers->LocationCode, 'wpsl_retailer_id_api', true );
        
          if($retailers->Sale == 'no'){

        write_log('<------------Current Loop ->'.$i.'------------>');

    

              

            write_log('Retailer Landing Page-> ID ->'.$retailerid.'---title->'.get_the_title($retailers->Name).'---------->');

            $zip =  explode('-',$retailers->Zip);

            $Zip_pages = get_page_by_slug($zip[0]);

            $retailerlanding_data = array(
                'post_title'    => $retailers->Name,
                'post_name'     => sanitize_title($retailers->Name),
                'post_content'  => '',
                'post_type'     => 'page',               
                'post_status'   => 'publish',             
                'post_author'  => 1,   
                'post_parent'  =>  $Zip_pages->ID,   
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'
                
            );      
  
             // Flooring Insert the post into the database.
             $add_store_page = wp_insert_post( $retailerlanding_data );
                 
             add_post_meta($add_store_page , '_wp_page_template' , 'single-stores-contact.php');

             write_log('store page id---->'.$add_store_page);
             write_log(get_permalink($add_store_page));

             $retailer_location_data = array(
                'post_title'    => $retailers->LocationCode,
                'post_name'     => sanitize_title($retailers->LocationCode),
                'post_content'  => '',
                'post_type'     => 'page',               
                'post_status'   => 'publish',             
                'post_author'  => 1,   
                'post_parent'  =>  $add_store_page,   
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'
                
            );      
  
             // Flooring Insert the post into the database.
             $add_store_location_page = wp_insert_post( $retailer_location_data );
             add_post_meta($add_store_location_page , '_wp_page_template' , 'template-location-id.php');
             write_log('locstionpage id---->'.$add_store_location_page);
             write_log(get_permalink($add_store_location_page));


            // Gather post data.
           $post_contet_data = '[fl_builder_insert_layout slug="contact-us-shop-local"]';   
    
           $my_post_wall = array(
               'post_title'    => 'Contact Us',
               'post_name'     => 'contact-us',
               'post_content'  => $post_contet_data,
               'post_type'     => 'page',               
               'post_status'   => 'publish',
               'post_parent'   => $add_store_location_page,
               'post_author'  => 1,               
               'comment_status' => 'closed',   // if you prefer
               'ping_status' => 'closed'
               
           );

            // Insert the post into the database.
            $add_contact = wp_insert_post( $my_post_wall );  
            
            update_post_meta( $add_contact, '_wp_page_template', 'template-retailer-contact.php' );
            update_post_meta( $add_contact, '_fl_builder_enabled', '1' );

            write_log('New Retailer Contact us Page->'.$add_contact);

            write_log('New Retailer Contact us Page->'.get_permalink($add_contact));

        

        write_log('<------------End ->'.$retailerid.'<- End------------->');

        //exit;

       $i++;
       }

     }

     write_log('<------------Ended with contact us function------------>');
}