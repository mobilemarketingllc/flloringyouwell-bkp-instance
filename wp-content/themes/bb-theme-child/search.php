<?php get_header(); ?>

    <div class="fl-archive container ">
        <div class="row search-row">

            <?php FLTheme::sidebar('left'); ?>

            <div class="fl-content fl-builder-content  <?php //FLTheme::content_class(); ?>" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

                <?php FLTheme::archive_page_header(); ?>

                <?php if(have_posts()) : ?>

                    <?php
                    include( ABSPATH .'/wp-content/themes/bb-theme-child/product-loop-search.php' );
                    ?>
                    <?php FLChildTheme::archive_nav(); ?>

                    

                <?php else : ?>

                    <?php get_template_part('content', 'no-results'); ?>

                <?php endif; ?>

            </div>



            <?php //FLTheme::sidebar('right'); ?>

        </div>
    </div>

<?php get_footer(); ?>