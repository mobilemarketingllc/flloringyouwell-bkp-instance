<?php
// Custom Shortcode for Retailer Listing on Coupon Form

function get_coupon_storelisting($zipcode="") {
    global $wpdb, $zipcode;
   
        $zipcode =$_POST['input_19'];
        
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zipcode)."&sensor=false&key=AIzaSyC_oZhdow3Y-8ab9GtrkElX44ELgslxZS0";

        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
                        
        $autolat = $result['results'][0]['geometry']['location']['lat'];
        $autolng = $result['results'][0]['geometry']['location']['lng'];


        if( trim($autolat) !="" && trim($autolng) !=""  ){

        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM $wpdb->posts AS posts
                INNER JOIN $wpdb->postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
                INNER JOIN $wpdb->postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'
                WHERE posts.post_type = 'wpsl_stores' 
                AND posts.post_status = 'publish'  GROUP BY posts.ID HAVING distance < 50 ORDER BY distance LIMIT 0, 20";

        $storeposts = $wpdb->get_results(  $sql);

        $listcontent = '<h4 style="font-size:18px;">Select your local flooring store below: </h4><ul>';   

        global $post;
        $COUNT=0;
        foreach ($storeposts as $post) {
            $is_sale = get_post_meta( $post->ID, 'wpsl_retailer_sale', true );
            if($is_sale==1){
             $COUNT++;
            }else{
                continue;
            }
            
            if($COUNT > 5){
                break;
            }
            
        
            $slug = get_post_field( 'post_name', $post->ID );
            $address_list = get_post_meta( $post->ID, 'wpsl_address', true );
            $zipcode_list = get_post_meta( $post->ID, 'wpsl_zip', true );
            $city_list = get_post_meta( $post->ID, 'wpsl_city', true );
            $state_list = get_post_meta( $post->ID, 'wpsl_state', true );   
            $ph_list = get_post_meta( $post->ID, 'wpsl_phone', true );
            $location_id = get_post_meta( $post->ID, 'wpsl_retailer_id_api', true );
            
            $retailer_id_from_api =  get_post_meta( $post->ID, 'retailer_id_from_api', true );
            $retailer_st_url = get_post_meta($post->ID, 'wpsl_retailer_storeurl', true );
            $store_url_link = home_url().'/'.$retailer_st_url.'/contact-us/';
            $retailer_couponSeoUrl = get_post_meta($post->ID, 'wpsl_retailer_couponSeoUrl', true );
            $retailer_st_url_CORETEC = get_post_meta($post->ID, 'wpsl_retailer_coretecVinyl', true );
            $retailer_sale =  get_post_meta( $post->ID, 'wpsl_retailer_sale', true );
            $wpsl_retailer_paid_search =  get_post_meta( $post->ID, 'wpsl_retailer_paid_search', true );
            $canada = get_post_meta( $store->ID, 'wpsl_retailer_canada', true );
            $financing = get_post_meta( $store->ID, 'wpsl_retailer_financing', true );
           
           // if($retailer_sale == 1 && $wpsl_retailer_paid_search=='')
           // {
             $store_sale_url = home_url().'/'.$retailer_couponSeoUrl;
          //  }
           
            $listcontent .= '<li id="'.$post->ID.'">
            <div class="Store_list_item">

                <h3 class="strtitle">'.get_the_title($post->ID).'</h3>

                <div class="storeDetails">
                    <div class="storeAddress"><p><span class="wpsl-street">'.$address_list.'</span></p><p><span class="wpsl-street">'.$city_list.', '.$state_list.' '.$zipcode_list.' </span></p></div>
                </div>

            </div>
            <div class="store_map_link">
                <div class="headmystorelink">
                <input type="hidden" name="retailer_post_id_'.$post->ID.'" value="'.$post->ID.'">
                  
                <h3>'. round($post->distance,2).' MI.</h3>
                <div class="fl-button-wrap fl-button-width-auto fl-button-left">
                <a href="'.$store_sale_url.'" class="fl-button test5">GET COUPON</a>
                </div>
            </div>
        </li>';

        }

        wp_reset_postdata();

        $listcontent .= '</ul>';

    }else{
        $listcontent ="<div class='zip_not_error'><h3>We’re sorry, we are unable to find any retailers that are currently close to the zip code you entered.</h3>
        <h3>Please try another zip code or try the find a retailer search.</h3></div>";
    }
    return $listcontent;

}

add_shortcode('couponstorelisting', 'get_coupon_storelisting');


add_filter( 'gform_confirmation', 'custom_confirmation', 10, 4 );
function custom_confirmation( $confirmation, $form, $entry, $ajax ) {
    if( $form['id'] == '16' ) {
        
        $confirmation = array( 'redirect' => 'https://flooringyouwell-stg.mm-dev.agency/thank-you/?post_id='.$entry[22] );
    } 
    return $confirmation;
}


// add_shortcode('insertSEM', 'insertSEM');

// function insertSEM(){
//     global $wpdb;

//     $templates = array(
//                  'flooring'=>'template-retailer-flooring-sale.php',
//                  'flooring-all'=>'template-all-retailer-flooring-sale.php',
//                  'carpet' => 'template-carpet-sale.php',
//                  'waterproof-flooring' => 'template-waterproof-flooring-sale.php',
//                  'waterproof-coretec'=>'template-waterproof-coretec-sale.php'
//     );

//     $sale_data = $wpdb->get_results( "SELECT * FROM coupon_sale LIMIT 200" );

//     if(count($sale_data) > 0){
//         foreach($sale_data as $data){
//             $page = get_page_by_title(  $data->LocationCode );
//             $page_id = $page->ID;

//             if(trim($data->couponSemUrl ) !=''){
//                 $url_path = explode('/',trim( $data->couponSemUrl, '/'));
//                 if(in_array('flooring-all',$url_path)){
//                     $args = array(
//                         'post_type'      => 'page',
//                         'posts_per_page' => -1,
//                         'post_parent'    => $page_id,
//                      );
//                      $parent = new WP_Query( $args );

//                      if ( $parent->have_posts() ) {
//                         while ( $parent->have_posts() ) {
//                             $parent->the_post();
                            
                          
//                           if($parent->post->post_name == 'flooring-all' ){
//                                $flooring_id = get_the_ID();
//                                $flooring_url = get_the_permalink();

//                                 $args = array(
//                                     'post_type'      => 'page',
//                                     'posts_per_page' => -1,
//                                     'post_parent'    => $flooring_id,
//                                     'guid' => $flooring_url."sale"
                                    
//                                 );

//                                 $flooring_child = new WP_Query( $args );
                                 
//                                  if($flooring_child->found_posts == 0){

//                                     $post_args = array(
//                                         'post_title' => 'Sale',
//                                         'post_status' => 'publish',
//                                         'post_type'   => 'page',
//                                         'post_name' => 'sale',
//                                         'post_parent' => $flooring_id,
//                                         'guid' => $flooring_url."sale"
//                                     );

//                                     $id =  wp_insert_post( $post_args);
//                                     add_post_meta($id , '_wp_page_template' , $templates['flooring-all']);
//                                     echo "inserted flooring-all  post id=".$flooring_id." <br>";
//                                  }
//                                 break;
//                           }
//                         }
//                         wp_reset_postdata();
//                      }        
                      

//                 }else if(in_array('flooring',$url_path)){
//                     $args = array(
//                         'post_type'      => 'page',
//                         'posts_per_page' => -1,
//                         'post_parent'    => $page_id,
//                      );
//                      $parent = new WP_Query( $args );

//                      if ( $parent->have_posts() ) {
//                         while ( $parent->have_posts() ) {
//                             $parent->the_post();
                            
                          
//                           if($parent->post->post_name == 'flooring' ){
//                                $flooring_id = get_the_ID();
//                                $flooring_url = get_the_permalink();

                              
//                                 $args = array(
//                                     'post_type'      => 'page',
//                                     'posts_per_page' => -1,
//                                     'post_parent'    => $flooring_id,
//                                     'guid' => $flooring_url."sale"
                                    
//                                 );

//                                 $flooring_child = new WP_Query( $args );
                                
                                 
//                                  if($flooring_child->found_posts == 0){

//                                     $post_args = array(
//                                         'post_title' => 'Sale',
//                                         'post_status' => 'publish',
//                                         'post_type'   => 'page',
//                                         'post_name' => 'sale',
//                                         'post_parent' => $flooring_id,
//                                         'guid' => $flooring_url."sale"
//                                     );

//                                     $id =  wp_insert_post( $post_args);
//                                     add_post_meta($id , '_wp_page_template' , $templates['flooring']);
//                                     echo "inserted flooring  post id=".$flooring_id."  <br>";
//                                  }
//                                 break;
//                           }
//                         }
//                         wp_reset_postdata();
//                      }        
//                 }
//             }

//             if(trim($data->carpetSemUrl ) !=''){
//                 $url_path = explode('/',trim( $data->carpetSemUrl, '/'));
                
//                 if(in_array('carpet',$url_path)){
//                     $args = array(
//                         'post_type'      => 'page',
//                         'posts_per_page' => -1,
//                         'post_parent'    => $page_id,
//                      );
//                      $parent = new WP_Query( $args );

//                      if ( $parent->have_posts() ) {
//                         while ( $parent->have_posts() ) {
//                             $parent->the_post();

//                             if($parent->post->post_name == 'carpet' ){
//                                $flooring_id = get_the_ID();
//                                $flooring_url = get_the_permalink();
                              
//                                 $args = array(
//                                     'post_type'      => 'page',
//                                     'posts_per_page' => -1,
//                                     'post_parent'    => $flooring_id,
//                                     'guid' => $flooring_url."sale"
//                                 );

//                                 $flooring_child = new WP_Query( $args );
                                 
//                                  if($flooring_child->found_posts == 0){
//                                     $post_args = array(
//                                         'post_title' => 'Sale',
//                                         'post_status' => 'publish',
//                                         'post_type'   => 'page',
//                                         'post_name' => 'sale',
//                                         'post_parent' => $flooring_id,
//                                         'guid' => $flooring_url."sale"
//                                     );

//                                     $id =  wp_insert_post( $post_args);
//                                     add_post_meta($id , '_wp_page_template' , $templates['carpet']);
//                                     echo "inserted carpet post id=".$flooring_id."  <br>";
//                                  }
//                                 break;
//                           }
//                         }
//                         wp_reset_postdata();
//                      }        
//                 }
//             }

//             if(trim($data->waterproofSemUrl ) !=''){
//                 $url_path = explode('/',trim( $data->waterproofSemUrl, '/'));
                
//                 if(in_array('waterproof-flooring',$url_path)){
//                     $args = array(
//                         'post_type'      => 'page',
//                         'posts_per_page' => -1,
//                         'post_parent'    => $page_id,
//                      );
//                      $parent = new WP_Query( $args );

//                      if ( $parent->have_posts() ) {
//                         while ( $parent->have_posts() ) {
//                             $parent->the_post();
                          
//                           if($parent->post->post_name == 'waterproof-flooring' ){
//                                $flooring_id = get_the_ID();
//                                $flooring_url = get_the_permalink();

//                                 $args = array(
//                                     'post_type'      => 'page',
//                                     'posts_per_page' => -1,
//                                     'post_parent'    => $flooring_id,
//                                     'guid' => $flooring_url."sale"
                                    
//                                 );

//                                 $flooring_child = new WP_Query( $args );
                                 
//                                  if($flooring_child->found_posts == 0){

//                                     $post_args = array(
//                                         'post_title' => 'Sale',
//                                         'post_status' => 'publish',
//                                         'post_type'   => 'page',
//                                         'post_name' => 'sale',
//                                         'post_parent' => $flooring_id,
//                                         'guid' => $flooring_url."sale"
//                                     );

//                                     $id =  wp_insert_post( $post_args);
//                                     add_post_meta($id , '_wp_page_template' , $templates['waterproof-flooring']);
//                                     echo "inserted waterproof flooring  post id=".$flooring_id."  <br>";
//                                  }

//                                 break;
//                           }
//                         }
//                         wp_reset_postdata();
//                      }        
//                 }else if(in_array('waterproof-coretec',$url_path)){
//                     $args = array(
//                         'post_type'      => 'page',
//                         'posts_per_page' => -1,
//                         'post_parent'    => $page_id,
//                      );
//                      $parent = new WP_Query( $args );

//                      if ( $parent->have_posts() ) {
//                         while ( $parent->have_posts() ) {
//                             $parent->the_post();
                          
//                           if($parent->post->post_name == 'waterproof-coretec' ){
//                                $flooring_id = get_the_ID();
//                                $flooring_url = get_the_permalink();

//                                 $args = array(
//                                     'post_type'      => 'page',
//                                     'posts_per_page' => -1,
//                                     'post_parent'    => $flooring_id,
//                                     'guid' => $flooring_url."sale"
//                                 );

//                                 $flooring_child = new WP_Query( $args );
                                                                 
//                                  if($flooring_child->found_posts == 0){

//                                     $post_args = array(
//                                         'post_title' => 'Sale',
//                                         'post_status' => 'publish',
//                                         'post_type'   => 'page',
//                                         'post_name' => 'sale',
//                                         'post_parent' => $flooring_id,
//                                         'guid' => $flooring_url."sale"
//                                     );

//                                     $id =  wp_insert_post( $post_args);
//                                     add_post_meta($id , '_wp_page_template' , $templates['waterproof-coretec']);
//                                     echo "inserted water coretec  post id=".$flooring_id." <br>";
//                                  }
//                                 break;
//                           }
//                         }
//                         wp_reset_postdata();
//                      }        
//                 }
//             }

//             $wpdb->delete( 'coupon_sale' , array( 'LocationCode' => $data->LocationCode ) );

//             echo "deleted Location Id = ".$data->LocationCode;
//             echo "<br>==========================================================<br>";

//         }
//     }

// }

 /* add_shortcode('insertSEO', 'insertSEO');

function insertSEO(){
    global $wpdb;

    $sale_templates = array(
                 'flooring'=>'template-retailer-flooring-sale.php',
                 'flooring all'=>'template-all-retailer-flooring-sale.php',
                 'carpet' => 'template-carpet-sale.php',
                 'waterproof flooring' => 'template-waterproof-flooring-sale.php',
                 'waterproof coretec'=>'template-waterproof-coretec-sale.php'
    );


    $flooring_templates = array(
        'flooring'=>'template-retailer-flooring.php',
        'flooring all'=>'template-retailer-flooring.php',
        'carpet' => 'template-carpet.php',
        'waterproof flooring' => 'template-waterproof-flooring.php',
        'waterproof coretec'=>'template-waterproof-flooring.php'
);

    $retailer_data = $wpdb->get_results( "SELECT * FROM coupon_sale LIMIT 2700, 500" );

   
    if(count($retailer_data) > 0){
        foreach($retailer_data as $data){

            $state_details =  check_page_exists_by_title($data->State);
            
            if($state_details == FALSE){
                 $state_details = create_retailer_page($data->State ,  'template-state.php' );
            }

            $state_page_id  = $state_details['id'];
            $state_page_url  = $state_details['url'];

            $city_details = check_page_exists_by_title($data->City , $state_page_id );
  
            if($city_details == FALSE){
                 $city_details= create_retailer_page($data->City ,  'template-city.php' ,$state_page_id ,   $state_page_url);
            }
           
            $city_page_id  = $city_details['id'];
            $city_page_url  = $city_details['url'];

            $zip = $data->Zip;
            if(strstr($data->Zip,'-')){
                $zip = explode('-',$data->Zip);
                $zip = $zip[0];
            }

            $zip_details = check_page_exists_by_title($zip , $city_page_id );
             
            if($zip_details == FALSE){
                 $zip_details= create_retailer_page($zip,  'template-zipcode.php' ,$city_page_id ,   $city_page_url);
            }

            $zip_page_id  = $zip_details['id'];
            $zip_page_url  = $zip_details['url'];


            $store_details = check_page_exists_by_title($data->Name , $zip_page_id );
             
            if($store_details == FALSE){
                 $store_details= create_retailer_page($data->Name,  'single-stores-contact.php' ,$zip_page_id ,  $zip_page_url);
            }

            $store_page_id  = $store_details['id'];
            $store_page_url  = $store_details['url'];


            $location_details = check_page_exists_by_title($data->LocationCode , $store_page_id );
             
            if($location_details == FALSE){
                 $location_details= create_retailer_page($data->LocationCode,  'template-location-id.php' ,$store_page_id ,  $store_page_url);
            }

            $location_page_id  = $location_details['id'];
            $location_page_url  = $location_details['url'];


  //flooring SEO

            $couponSeoUrl_details = check_page_exists_by_title($data->couponSeoUrl , $location_page_id );
             

            if($couponSeoUrl_details == FALSE){
                 $couponSeoUrl_details= create_retailer_page($data->couponSeoUrl, $flooring_templates[strtolower($data->couponSeoUrl)]  ,$location_page_id ,  $location_page_url);
            }

            $couponSeoUrl_page_id  = $couponSeoUrl_details['id'];
            $couponSeoUrl_page_url  = $couponSeoUrl_details['url'];

 //Floorin SEM           

            if($data->couponSemUrl == 'sale'){
                $couponSemUrl_details = check_page_exists_by_title($data->couponSemUrl , $couponSeoUrl_page_id );
             

                if($couponSemUrl_details == FALSE){
                     $couponSemUrl_details= create_retailer_page($data->couponSemUrl, $sale_templates[strtolower($data->couponSeoUrl)]  ,$couponSeoUrl_page_id ,  $couponSeoUrl_page_url);
                }
    
                $couponSemUrl_page_id  = $couponSemUrl_details['id'];
                $couponSemUrl_page_url  = $couponSemUrl_details['url'];
            }


//Carpet SEO
            $carpetSeoUrl_details = check_page_exists_by_title($data->carpetSeoUrl , $location_page_id );
             

            if($carpetSeoUrl_details == FALSE){
                 $carpetSeoUrl_details= create_retailer_page($data->carpetSeoUrl, $flooring_templates[strtolower($data->carpetSeoUrl)]  ,$location_page_id ,  $location_page_url);
            }

            $carpetSeoUrl_page_id  = $carpetSeoUrl_details['id'];
            $carpetSeoUrl_page_url  = $carpetSeoUrl_details['url'];

//Carpet SEM           

if($data->carpetSemUrl == 'sale'){
    $carpetSemUrl_details = check_page_exists_by_title($data->carpetSemUrl , $carpetSeoUrl_page_id );
 

    if($carpetSemUrl_details == FALSE){
         $carpetSemUrl_details= create_retailer_page($data->carpetSemUrl, $sale_templates[strtolower($data->carpetSeoUrl)]  ,$carpetSeoUrl_page_id ,  $carpetSeoUrl_page_url);
    }

    $carpetSemUrl_page_id  = $carpetSemUrl_details['id'];
    $carpetSemUrl_page_url  = $carpetSemUrl_details['url'];
}            


//Waterproof            
            $waterproofSeoUrl_details = check_page_exists_by_title($data->waterproofSeoUrl , $location_page_id );
             

            if($waterproofSeoUrl_details == FALSE){
                 $waterproofSeoUrl_details= create_retailer_page($data->waterproofSeoUrl, $flooring_templates[strtolower($data->waterproofSeoUrl)]  ,$location_page_id ,  $location_page_url);
            }

            $waterproofSeoUrl_page_id  = $waterproofSeoUrl_details['id'];
            $waterproofSeoUrl_page_url  = $waterproofSeoUrl_details['url'];


//Waterproof SEM           

if($data->waterproofSemUrl == 'sale'){
    $waterproofSemUrl_details = check_page_exists_by_title($data->waterproofSemUrl , $waterproofSeoUrl_page_id );
 

    if($waterproofSemUrl_details == FALSE){
         $waterproofSemUrl_details= create_retailer_page($data->waterproofSemUrl, $sale_templates[strtolower($data->waterproofSeoUrl)]  ,$waterproofSeoUrl_page_id ,  $waterproofSeoUrl_page_url);
    }

    $waterproofSemUrl_page_id  = $waterproofSemUrl_details['id'];
    $waterproofSemUrl_page_url  = $waterproofSemUrl_details['url'];
}            


        }
    }
}

function check_page_exists_by_title($title , $parent = ""){
    global $wpdb;
     
    $sql = " SELECT * FROM wp_posts WHERE post_title ='" .$title. "' AND post_status = 'publish' AND post_type = 'page' ";
    
    if($parent !=""){
        $sql .= " AND post_parent =".$parent ;
    } 

    $sql .=" LIMIT 1 ";
       
    $page = $wpdb->get_results( $sql );
    
                    
    if($page[0]->ID){
        $data = array('id' => $page[0]->ID , 'url' => $page[0]->guid );
        return $data;
    }else{
        return FALSE;
    }
}

function  create_retailer_page($title , $template , $parent='', $parent_url='https://flooringyouwell.com/' ){
    $post_args = array(
        'post_title' => $title,
        'post_status' => 'publish',
        'post_type'   => 'page',
        'post_name' => sanitize_title($title),
        
        'guid' => $parent_url."".sanitize_title($title)."/"
    );

    if($parent !=''){
        $post_args['post_parent']= $parent;

    }
    $id =  wp_insert_post( $post_args);

    add_post_meta($id , '_wp_page_template' , $template);
    add_post_meta($id , '_wp_is_seo_page' , 'yes');

    echo "<br> inserted post id=".$id."(".$title.") <br>";

    $data = array('id' => $id , 'url' => $post_args['guid']);

    return $data;
} */


// add_shortcode('insertSEO', 'insertSEO');

// function insertSEO(){
//     global $wpdb;

//     $templates = array(
//                  'flooring'=>'template-retailer-flooring.php',
//                  'flooring-all'=>'template-retailer-flooring.php',
//                  'carpet' => 'template-carpet.php',
//                  'waterproof-flooring' => 'template-waterproof-flooring.php',
//                  'waterproof-coretec'=>'template-waterproof-flooring.php'
//     );

//     $sale_data = $wpdb->get_results( "SELECT * FROM retailers LIMIT 100" );

//     if(count($sale_data) > 0){
//         foreach($sale_data as $data){
//             $page = get_page_by_title(  $data->LocationCode );
//             $page_id = $page->ID;

//             if(trim($data->couponSeoUrl ) !=''){
//                 $url_path = explode('/',trim( $data->couponSeoUrl, '/'));

//                 if(in_array('flooring-all',$url_path)){
//                      $args = array(
//                         'post_type'      => 'page',
//                         'posts_per_page' => -1,
//                         'post_parent'    => $page_id,
//                         'guid' =>  "https://flooringyouwell.com/".$data->couponSeoUrl
                        
//                     );

//                     $flooring_child = new WP_Query( $args );
//                     var_dump( $flooring_child);die();
//                     if($flooring_child->found_posts == 0){

//                         $post_args = array(
//                             'post_title' => 'Flooring All',
//                             'post_status' => 'publish',
//                             'post_type'   => 'page',
//                             'post_name' => 'flooring-all',
//                             'post_parent' => $page_id ,
//                             'guid' => "https://flooringyouwell.com/".$data->couponSeoUrl
//                         );

//                         $id =  wp_insert_post( $post_args);
//                         add_post_meta($id , '_wp_page_template' , $templates['flooring-all']);
//                         echo "inserted flooring-all  post id=".$flooring_id." <br>";
//                      }

//                     }
//                 }               
//             } 
//         }              
// }




add_shortcode('updateSEO_store_slug', 'updateSEO_store_slug');

function updateSEO_store_slug(){
    global $wpdb;

    $retailer_data = $wpdb->get_results( "SELECT * FROM retailers" );   
    if(count($retailer_data) > 0){
        $i =0;
        foreach($retailer_data as $data){
            $url_path = explode('/',trim( $data->couponSeoUrl, '/'));
              
            // var_dump($url_path);
if($url_path[3] !=""){
 
        global $wpdb;
            $sql = "SELECT ID FROM wp_posts WHERE ID = (SELECT post_parent FROM wp_posts WHERE post_title ='".$data->LocationCode."' AND post_status = 'publish' AND post_type = 'page')";
            $page = $wpdb->get_results( $sql );

$id = $page[0]->ID;

     if( $id &&  $id !=""){
           
            $sql = "UPDATE wp_posts SET post_name = '".$url_path[3]."'  WHERE ID = '".$page[0]->ID."'";
            $page = $wpdb->get_results( $sql );
            $i++;
            echo "post id". $page[0]->ID." updated<br>".$i;
        }
    }
}
    }
}