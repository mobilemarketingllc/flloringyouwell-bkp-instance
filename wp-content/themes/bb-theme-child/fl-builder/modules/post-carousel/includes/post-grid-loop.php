<div class="fl-post-carousel-post" itemscope="itemscope" itemtype="<?php FLPostGridModule::schema_itemtype(); ?>">
   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div class="fl-post-carousel-text">
		
		<div class="fl-post-carousel-content">

					<?php 

						 $videotype = get_field('video_type');
						if($videotype=='Embed'){

						//	echo get_field('video_embed_code');

							$video = get_field( 'video_embed_code' );

							preg_match('/src="(.+?)"/', $video, $matches_url );
							$src = $matches_url[1];	

							preg_match('/embed(.*?)?feature/', $src, $matches_id );
							$id = $matches_id[1];
							$id = str_replace( str_split( '?/' ), '', $id );
						
								// Generate youtube thumbnail url
								 $thumbURL = 'http://img.youtube.com/vi/'.$id.'/maxresdefault.jpg';

						?>

							<img src="<?php echo $thumbURL;?>" width="370"/>	

						<?php }else{ ?>

						<img src="<?php echo get_field('video_placeholder_image');?>" width="370"/>		
						
						<?php } ?>
		</div>
		
		<h2 class="fl-post-carousel-title" itemprop="headline">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</h2>

	</div>
	</a>

</div>
