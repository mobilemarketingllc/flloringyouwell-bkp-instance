<?php

/*
Template Name: Retailer Contact us

*/

//Retailer Landing Page WIth COntactus Form
//flooringyouwell.com/store-name/city/state/zipcode/


get_header(); 


//$user_country =  get_current_country();
?>

<?php

$url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

//print_r($url_path);

$locationid = $url_path[4];

$query_args_meta = array(
    'posts_per_page' => -1,
    'post_type' => 'wpsl_stores',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'wpsl_retailer_id_api',
            'value' => $locationid,
            'compare' => '='
        )
        
    )
);

$my_posts = get_posts($query_args_meta);
if( $my_posts ) :
//	echo 'ID on the first post found ' . $my_posts[0]->ID;
	$post   = get_post( $my_posts[0]->ID );
endif;

global $post;
$wpsl_retailer_affiliate_code       = get_post_meta( $post->ID, 'wpsl_retailer_affiliate_code', true );
$wpsl_retailer_rugsale = get_post_meta( $post->ID, 'wpsl_retailer_rugsale', true );
?>

            <?php 
       
                echo do_shortcode('[fl_builder_insert_layout slug="contact-us-shop-local"]');  
      
            ?>


<?php get_footer(); ?>
