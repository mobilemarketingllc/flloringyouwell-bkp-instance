<?php include( dirname( __FILE__ ) . '../include/constant.php' );?>
<?php get_header(); ?>
    <div class="container">
        <div class="row">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/" style="padding-top: 20px; padding-left: 20px;">
        <a href="/">Home</a> &raquo; <a href="/flooring/">Flooring</a> &raquo; <a href="/flooring/hardwood/">Hardwood</a> &raquo; <a href="/flooring/hardwood/products/">Hardwood Products</a> &raquo; <?php the_title(); ?>
        </div>
            </div>
    </div>

    <div class="fl-content product">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php get_template_part('content', 'single-product'); ?>
		<?php endwhile; endif; ?>
	</div>
    
<?php get_footer(); ?>