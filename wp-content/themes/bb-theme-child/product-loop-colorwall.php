
<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
	<div class="row product-row">
	<?php

$args_for_query2 =  array(
	"post_type" => "luxury_vinyl_tile",
	"post_status" => "publish",
	"orderby" => "title",
	"order" => "ASC",
	"posts_per_page" => 4, 
	'meta_query'    => array(
			  'key'       => 'collection',
			  'value'     => 'Coretec Colorwall',
			  'compare'   => '=',
		  )  
  );

  $query2 = new WP_Query($args_for_query2);  

  //$wp_query->posts = array_merge( $wp_query->posts, $query2->posts );

 while ( have_posts() ): the_post();

			$flooringtype = $post->post_type; 
			$collection = get_field('collection', $post->ID);
?>
		<div class="col-md-4 col-sm-4 col-xs-6">
			<div class="fl-post-grid-post" itemscope itemtype="Product">
				<?php FLPostGridModule::schema_meta(); ?>
				<?php if(get_field('swatch_image_link')) { ?>
				<div class="fl-post-grid-image prod_img">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php
							$itemImage = get_field('swatch_image_link');												  
							$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
							
						?>
						<img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
						<?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {  ?>
							<span class="exlusive-badge"><img src="https://staging.flooringyouwell.com/wp-content/uploads/2019/07/exclusive-products-icon-copy.png" alt="<?php the_title(); ?>" /></span>
						<?php } ?>
					</a>
					<div class="button-wrapper">
						<a class="button alt" href="<?php the_permalink(); ?>">View Product</a>
						<a class="button" href="/find-sfn-retailer/">Find A Retailer</a>
						<?php /*?>
						<a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button" role="button">
							<span class="fl-button-text">GET COUPON</span>
						</a><br />
						<?php*/ ?>
					</div>
				</div>
				<?php } else { ?>
				<div class="fl-post-grid-image ">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<img class="<?php echo $class; ?>" src="http://placehold.it/168x123?text=COMING+SOON" alt="<?php the_title_attribute(); ?>" />
					</a>
					<div class="button-wrapper">
						<a class="button alt" href="<?php the_permalink(); ?>">View Product</a>
						<a class="button" href="/find-sfn-retailer/">Find A Retailer</a>
						<?php /*?>
						<a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button" role="button">
							<span class="fl-button-text">GET COUPON</span>
						</a><br />
						<?php*/ ?>
					</div>
				</div>
				<?php } ?>
				<div class="fl-post-grid-text product-grid btn-grey">
					<div class="colletionTitle">
						<h4><?php echo get_field('collection', $post->ID); ?></h4>
						<h2 class="fl-post-grid-title" itemprop="headline">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { echo get_field('style', $post->ID).' '.get_field('color', $post->ID); } else{ echo get_field('color', $post->ID); }?>
							</a>
						</h2>
						<?php
							$familysku = get_post_meta($post->ID, 'collection', true);     
							$familycolor = get_post_meta($post->ID, 'style', true);                  

							if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { 

							$args = array(
								'post_type'      => $flooringtype,
								'posts_per_page' => -1,
								'post_status'    => 'publish',
								'meta_query'     => array(
									array(
										'key'     => 'style',
										'value'   => $familycolor,
										'compare' => '='
									),
									array(
										'key' => 'swatch_image_link',
										'value' => '',
										'compare' => '!='
										)
								)
							);
						}else{

							$args = array(
								'post_type'      => $flooringtype,
								'posts_per_page' => -1,
								'post_status'    => 'publish',
								'meta_query'     => array(
									array(
										'key'     => 'collection',
										'value'   => $familysku,
										'compare' => '='
									),
									array(
										'key' => 'swatch_image_link',
										'value' => '',
										'compare' => '!='
										)
								)
							);
						}
							$the_query = new WP_Query( $args );
						?>
					</div>
					<div class="color-count"><?php echo $the_query->found_posts; ?> Colors Available</div>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>
</div>