<?php
    $post_types = 'luxury_vinyl_tile';

    $style_page_url = "/coretec/coretec-colorwall/choose-your-style/";

    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 

    $args = get_posts(array(
        'numberposts'	=> -1,
        'post_type'		=> $post_types,
        'meta_query'	=> array(
            array(
                'key'	 	=> 'collection',
                'value'	  	=> 'COREtec Colorwall',
                'compare' 	=> '=',
            ),
        ),
    ));

    $the_query = new WP_Query( $args );
    $colornames = array();
    foreach( $the_query->query as $color ) {
        $productid = $color->ID;
        $colorname = array(
            "ID" => $productid,
            "Color" => get_field('color', $productid),
        );
        array_push($colornames, $colorname);
    }

    $finalcolornames = unique_multidim_array($colornames,'Color');
    
    // echo "<pre>";
    // print_r($colornames);
    // echo "</pre>";
    
    // echo "<pre>";
    // print_r($finalcolornames);
    // echo "</pre>";
  
    if(!empty($finalcolornames)){ ?>
    <div class="colors-list">
        <ul>
        <?php
            foreach( $finalcolornames as $colors ) { 
            // echo "<pre>";
            // print_r($colors);
            // echo "</pre>";
            if(get_field('swatch_image_link', $colors["ID"])) {
        ?>
            <li>
                <div class="fl-post-grid-image prod_img">
                    <a href="<?php echo $style_page_url.'?color='.$colors["Color"]; ?>" title="<?php the_title_attribute(); ?>">
                        <?php
                            $itemImage =  swatch_image_product($colors["ID"],'600','400')
                            
                        ?>
                        <img class="<?php echo $class; ?>" src="<?php echo $itemImage; ?>" alt="<?php the_title_attribute(); ?>" />
                    </a>
                    <div class="product-info-box">
                        <div class="product-info-box-inner">
                            <h6><?php echo $colors["Color"]; ?></h6>
                            <a href="<?php echo $style_page_url.'?color='.$colors["Color"]; ?>" class="readmore-link">Choose Your Style</a>
                        </div>
                    </div>
                </div>
            </li>
        <?php  }
        } ?>
        </ul>
    </div>
    <?php } ?>

<?php wp_reset_query(); ?>