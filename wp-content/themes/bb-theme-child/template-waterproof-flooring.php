<?php
/*
Template Name: Retailer Flooring waterproof-flooring

*/

//Retailer Landing Page WIth COntactus Form
//flooringyouwell.com/store-name/city/state/zipcode/

get_header(); ?>

<?php

$url_path = explode('/',trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));

$locationid = $url_path[4];

$query_args_meta = array(
    'posts_per_page' => -1,
    'post_type' => 'wpsl_stores',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'wpsl_retailer_id_api',
            'value' => $locationid,
            'compare' => '='
        )
        
    )
);

$my_posts = get_posts($query_args_meta);
if( $my_posts ) :
//	echo 'ID on the first post found ' . $my_posts[0]->ID;
	$post   = get_post( $my_posts[0]->ID );
endif;

global $post;

?>

            <?php
            
            if($url_path[5]=='waterproof-flooring'){

                echo do_shortcode('[fl_builder_insert_layout slug="sfn-retailer-waterproof-landing-no-cortec-seo"]');
            }
            
            if($url_path[5]=='waterproof-coretec'){

                echo do_shortcode('[fl_builder_insert_layout slug="sfn-retailer-waterproof-landing-with-coretec-seo"]');
            }
            
            ?>


<?php get_footer(); ?>
