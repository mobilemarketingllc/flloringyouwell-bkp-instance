// Facet Changes and Title Start
function reloadFacet() {
    jQuery('.facetwp-facet').each(function() {
        if (jQuery(this).children('.facet-inner').length == 0) {
            var moreCount = jQuery(this).children('.facetwp-overflow').children('.facetwp-checkbox').length;
            jQuery(this).children('div').wrapAll('<div class="facet-inner" />');
            jQuery(this).children('.facetwp-toggle:eq(0)').text("See " + moreCount + " more");
        }
    });
}

jQuery(document).on('facetwp-refresh', function() {
    reloadFacet();
});

jQuery(document).on('facetwp-loaded', function() {
    jQuery.each(FWP.settings.num_choices, function(key, val) {
        var html = jQuery('.facetwp-facet-' + key).html();
        if (html == "") { jQuery('.facetwp-facet-' + key).parent().hide() } else { jQuery('.facetwp-facet-' + key).parent().show(); }
    });
    reloadFacet();
});

function usformatNumber() {
    jQuery('div[title="Start location"]').parent().addClass('parentLocator');
    jQuery('.usphnum').each(function() {
        if (jQuery(this).hasClass('numchanged')) {} else {
            var number = jQuery(this).children('.num').text().replace(/[^\d]/g, '')
            if (number.length == 7) {
                number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
            } else if (number.length == 10) {
                number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
            }
            jQuery(this).children('.num').text(number);
            jQuery(this).attr('href', 'tel:' + number).addClass('numchanged');
        }
    });
}

jQuery(document).ready(function() {
    jQuery('.open-stores').click(function(e) {
        e.preventDefault();
        // jQuery('.open-stores ').toggleClass('open-store-box');
        // jQuery('.header_store').toggleClass('active');

        if (jQuery(this).hasClass('open-store-box')) {
            jQuery('.header_store').removeClass('active');
            jQuery(this).removeClass('open-store-box');
        } else {
            jQuery(jQuery(this).attr('href')).addClass('active');
            jQuery(this).addClass('open-store-box');
        }
    });
    //  mouse leave on popup
    jQuery(".header_store").mouseleave(function() {
        jQuery(this).removeClass('active');
        jQuery('.open-stores').removeClass('open-store-box');
    });


    usformatNumber();

    window.setInterval(function() {
        usformatNumber();
    }, 300);

    if (jQuery('.toggle-image-thumbnails > div').length > 7) {
        jQuery('.toggle-image-thumbnails').slick({
            rows: 2,
            slidesToShow: 3,
            arrows: true,
            infinite: false,
            mobileFirst: false,
            prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-left"></i></a>',
            nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-right"></i></a>',
            responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 3
                    }
                }
            ]
        });
    }
});
jQuery(window).load(function() {
    reloadFacet();

    jQuery('#product-images-holder').lightGallery();

    if (jQuery('.facet_filters .fl-html > .close_bar').length == 0) {
        jQuery('.facet_filters .fl-html').prepend('<div class="close_bar"><a class="close close_sidebar" href="javascript:void(0)"><i class="fa fa-close">&nbsp;</i></a></div>');
    }
    if (jQuery('.facet_filters .fl-html > .close_sidebar_button').length == 0) {
        jQuery('.facet_filters .fl-html').append('<div class="close_sidebar_button"><a class="fl-button close_sidebar" href="#"><span class="fl-button-text">View Results</span></a></div>');
    }

    jQuery(document).on('click', '.open_sidebar', function(e) {
        e.preventDefault();
        if (jQuery(this).hasClass('active')) {
            jQuery('.facet_filters').animate({ left: '-' + (jQuery('.facet_filters').width() + 50) });
            jQuery(this).removeClass('active');
        } else {
            jQuery('.facet_filters').animate({ left: '0px' });
            jQuery(this).addClass('active');
        }

    });

    jQuery(document).on('click', '.close_sidebar', function(e) {
        e.preventDefault();
        jQuery('.facet_filters').animate({ left: '-' + (jQuery('.facet_filters').width() + 50) });
        jQuery('.open_sidebar').removeClass('active');
    });
});
jQuery(function($) {
    var win = $(window);

    function bbScroll() {
        if ('undefined' != typeof FLBuilderLayoutConfig) {
            var offset = 0;

            if ('undefined' === typeof FLBuilderLayout) {
                return;
            }

            if (FLBuilderLayout._isMobile() && win.width() < 992) {
                offset = 0;
            }

            if ($('body.admin-bar').length > 0) {
                offset += 32;
            }

            FLBuilderLayoutConfig.anchorLinkAnimations.duration = 1000;
            FLBuilderLayoutConfig.anchorLinkAnimations.easing = 'swing';
            FLBuilderLayoutConfig.anchorLinkAnimations.offset = offset;
        }
    }

    bbScroll();
    win.on('resize', bbScroll);

});

jQuery(window).load(function() {
    jQuery('#header_store_listing').html(jQuery('.header_store_listing.hidden').html());
    jQuery('.header_store_listing.hidden').remove();
});

var st_name = jQuery.cookie("my_store_name");
var st_phone = jQuery.cookie("my_store_phone");
if (st_name) {
    st_name = st_name.replace(/&#038;/gi, " & ");
    st_name = st_name.replace(/&#8217;/gi, "'");
    jQuery(".my_store_name").text(st_name);
}
if (st_phone) {
    jQuery(".my_store_phone").text(st_phone);
}


jQuery(document).on('click', '.viewMapLink', function() {

    jQuery('.header_store .wpsl-search-widget input[name="wpsl-widget-search"]').val(jQuery(this).data('address'));
    jQuery('.header_store #wpsl-widget-form').submit();
});

jQuery(document).on('click', '.usercurent', function(e) {
    e.preventDefault();
    //jQuery('#locsearchform #wpsl-search-input ').attr('value', jQuery(this).data('location'));
    jQuery('.header_store .wpsl-search-widget input[name="wpsl-widget-search"]').attr('value', jQuery(this).data('location'));
    jQuery('.header_store #wpsl-widget-form').submit();
    //jQuery('#locsearchform #wpsl-search-btn').trigger('click');
    //jQuery('#locsearchform').submit();
});


jQuery(document).on('click', '.mystore', function() {
    //alert('mystore');
    var mystore = jQuery(this).attr("data-id");

    jQuery.cookie("my_store", null, { path: '/' });
    jQuery.cookie("my_store_name", null, { path: '/' });
    jQuery.cookie("my_store_phone", null, { path: '/' });
    jQuery('#header_storeListing ul li').remove('hideasmystore');

    jQuery('label[for=' + mystore + '_mystore]').trigger('click');
    jQuery('.mystore + label').removeClass('activeStore');
    jQuery('label[for=' + mystore + '_mystore]').addClass('activeStore');
    jQuery.cookie("my_store", mystore, { expires: 365 * 20, path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=make_my_store&store_id=' + jQuery(this).attr("data-id"),

        success: function(data) {
            var storename = data;
            storename = storename.replace(/&#038;/gi, " & ");
            storename = storename.replace(/&#8217;/gi, "'");
            var resarray = storename.split('|');

            jQuery(".my_store_name").text(resarray[0]);
            jQuery(".my_store_phone").text(resarray[1]);
            jQuery('#header_storeListing ul li#' + mystore).addClass('hideasmystore');
            jQuery.cookie("my_store_name", resarray[0], { expires: 365 * 20, path: '/' });
            jQuery.cookie("my_store_phone", resarray[1], { expires: 365 * 20, path: '/' });
        }
    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=get_storelisting_header_ajax&store_id=' + jQuery(this).attr("data-id"),

        success: function(data) {
            jQuery("#header_store_listing").html(data);
        }
    });


    // jQuery('.header_store').toggleClass('active');
    //jQuery('.headStoreMobile .open-stores ').toggleClass('open-store-box');
});


jQuery('.pageScrollTabs .fl-col.fl-col-small').click(function() {

    jQuery(this).siblings().removeClass('activeLink');
    jQuery(this).addClass('activeLink');

})

jQuery(function() {
    var current = location.pathname;
    //alert(current);
    jQuery('.collectionLinks a').each(function() {
        var $this = jQuery(this);
        // if the current path is like this link, make it active
        if ($this.attr('href') == current) {
            $this.addClass('active');
        }
    })
    jQuery('.color_variations_slider > .slides').slick({
        dots: false,
        infinite: false,
        speed: 300,
        arrows: true,
        slidesToShow: 7,
        slidesToScroll: 7,
        mobileFirst: false,
        prevArrow: '<a href="javascript:void(0)" class="arrow slick-prev"> <i class="fa fa-angle-left"></i> </a>',
        nextArrow: '<a href="javascript:void(0)" class="arrow slick-next"> <i class="fa fa-angle-right"></i> </a>',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    jQuery('.toggle-image-thumbnails .toggle-image-holder a').click(function(e) {
        jQuery('.toggle-image-thumbnails .toggle-image-holder a').removeClass('active');
        url = jQuery(this).attr("data-background");
        bkImg = "url('" + url + "')";
        jQuery(jQuery(this).attr("data-fr-replace-bg")).css("background-image", bkImg).attr("data-responsive", url).attr("data-src", url);
        jQuery(jQuery(this).attr("data-fr-replace-bg") + ' a.popup-overlay-link').attr('data-src', url);
        jQuery('div.img-responsive.toggle-image').attr('data-src', url);
        jQuery('#product-images-holder img.img-responsive').attr('src', url);
        jQuery(this).addClass('active');
        e.preventDefault();
    });

    jQuery('body').on('click', '.submit_coupon_trigger', function(e) {
        var postid = jQuery(this).data('postid');
        jQuery('#input_16_22').val(postid);
        var locationid = jQuery(this).data('locationid');
        jQuery('#input_16_23').val(locationid);
        jQuery('#gform_16').submit();
    });
});

jQuery(document).ready(function(e) {
    jQuery('.aTagNotWorking a').click(function() {
        var urlRedirect = jQuery(this).attr('href');
        window.location = urlRedirect
    });
});


// Home page redirect banner URL
setTimeout(function() {
    if (jQuery('.headStoreMobile .my_store_name').text() == 'Please Select Store') {
        jQuery('.storeNamePhone').hide();
    }
}, 2000);

var $ = jQuery;

$(document).ready(function() {
    if ($('.featured-products > .featured-product-list > .featured-product-item').length > 4) {
        $('.featured-products > .featured-product-list').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                    breakpoint: 899,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
})