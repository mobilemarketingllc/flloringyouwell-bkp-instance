<?php 
$collections = array();
?>
<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
	<div class="row product-row">
        <?php while ( have_posts() ): the_post(); ?>
        
		<?php 
		if(count($collections) >= 12){
			continue;
		}

          $collection = get_post_meta($post->ID, 'collection', true);
        if(trim($collection) !="" && !in_array($collection , $collections)){
            $collections[] = $collection;
        }else{
            continue;
        }
     ?>
		<div class="col-md-4 col-sm-4 col-xs-6">
			<div class="fl-post-grid-post" itemscope itemtype="Product">
				<?php FLPostGridModule::schema_meta(); ?>
				<?php if(get_field('swatch_image_link')) { ?>
				<div class="fl-post-product-image">
					<div class="fl-post-grid-image prod_img">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php

								if(get_field('gallery_room_images')){
									$gallery_images = get_field('gallery_room_images');
									$gallery_img = explode("|",$gallery_images);
									shuffle($gallery_img);
									foreach($gallery_img as  $key=>$value) {
										$room_image = $value;
									
										if(strpos($room_image , 's7.shawimg.com') !== false){
											if(strpos($room_image , 'http') === false){ 
												$room_image = "http://" . $room_image;
											}
											$room_image_small = $room_image ;
											$room_image = $room_image ;
											$class = "";
										} else{
											if(strpos($room_image , 'http') === false){ 
												$room_image = "https://" . $room_image;
											}
											$room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[300x300]&sink";
											$class = "shadow";
										}
										break;
									}

								}
								$itemImage = get_field('swatch_image_link');
															
								if(strpos($itemImage , 's7.shawimg.com') !== false){
									if(strpos($itemImage , 'http') === false){ 
										$itemImage = "http://" . $itemImage;
									}
								}else{
									if(strpos($itemImage , 'http') === false){ 
										$itemImage = "https://" . $itemImage;
									}
								}							  
								$itemImage= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage . "]&scale=size[222]&sink";

								if($room_image != "") {
								?>
<!-- 									<img class="<?php //echo $class; ?> room_img" src="<?php // echo $room_image; ?>" alt="<?php //the_title_attribute(); ?>" /> -->
								<?php
								}else{
									?>
								<!-- 	<img src="http://placehold.it/300x300?text=No+Image" alt="<?php// the_title_attribute(); ?>" /> -->
									<?php
								}
							?>

							

							<img class="<?php echo $class; ?> swatch_img" src="<?php  echo $itemImage; ?>" alt="<?php the_title_attribute(); ?>" />
						</a>
					</div>
					<?php } else { ?>
					<div class="fl-post-grid-image ">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<img class="<?php echo $class; ?>" src="http://placehold.it/168x123?text=COMING+SOON" alt="<?php the_title_attribute(); ?>" />
						</a>
					</div>
				<?php } ?>
				<div class="product_details_link">
					<a class="fl-button-rev fl-button" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
                    <a class="fl-button" href="/find-sfn-retailer/">FIND RETAILER</a>
				</div>	
				</div>	
				<div class="fl-post-grid-text product-grid btn-grey">
					<h4><?php the_field('collection'); ?></h4>
                    <?php 
                    $flooringtype = get_post_type( get_the_ID() );
                    $value = get_post_meta($post->ID, 'collection', true);
                    $key =  "collection";
                
                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => $key,
                                'value'   => $value,
                                'compare' => '='
                            )
                        )
                    );
                    $the_query = new WP_Query( $args );
                ?>
                 <h6 class="found"><?php  echo count($the_query->posts)." Colors";?> </h6>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
		

	</div>
</div> 