<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
	<div class="row product-row search">
		<?php
			while ( have_posts() ): the_post();
			$flooringtype = $post->post_type; 
		?>
		<div class="col-md-3 col-sm-3 col-xs-6">
			<div class="fl-post-grid-post" itemscope itemtype="Product">
				<?php FLPostGridModule::schema_meta(); ?>
				<?php if(get_field('swatch_image_link')) { ?>
				<div class="fl-post-grid-image prod_img">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php
							$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
							
						?>
						<img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
					</a>
					<div class="button-wrapper">
						<a class="button alt" href="<?php the_permalink(); ?>">View Product</a>
						<a class="button" href="/find-sfn-retailer/">Find A Retailer</a>
						<?php /*?>
						<a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button" role="button">
							<span class="fl-button-text">GET COUPON</span>
						</a><br />
						<?php*/ ?>
					</div>
				</div>
				<?php } else { ?>
				<div class="fl-post-grid-image ">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<img class="<?php echo $class; ?>" src="http://placehold.it/168x123?text=COMING+SOON" alt="<?php the_title_attribute(); ?>" />
					</a>
					<div class="button-wrapper">
						<a class="button alt" href="<?php the_permalink(); ?>">View Product</a>
						<a class="button" href="/find-sfn-retailer/">Find A Retailer</a>
						<?php /*?>
						<a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button" role="button">
							<span class="fl-button-text">GET COUPON</span>
						</a><br />
						<?php*/ ?>
					</div>
				</div>
				<?php } ?>
				<div class="fl-post-grid-text product-grid btn-grey">
					<h4><?php the_field('collection'); ?></h4>
					<h2 class="fl-post-grid-title" itemprop="headline">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_field('color'); ?></a>
					</h2>
					<?php
						$familysku = get_post_meta($post->ID, 'collection', true);                  

						$args = array(
							'post_type'      => $flooringtype,
							'posts_per_page' => -1,
							'post_status'    => 'publish',
							'meta_query'     => array(
								array(
									'key'     => 'collection',
									'value'   => $familysku,
									'compare' => '='
								),
								array(
									'key' => 'swatch_image_link',
									'value' => '',
									'compare' => '!='
									)
							)
						);
						$the_query = new WP_Query( $args );
					?>
					<div class="color-count"><?php echo $the_query->found_posts; ?> Colors Available</div>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>
</div>