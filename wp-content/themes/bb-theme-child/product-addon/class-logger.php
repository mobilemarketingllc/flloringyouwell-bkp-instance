<?php

trait WP_Example_Logger {

	/**
	 * Really long running process
	 *
	 * @return int
	 */
	public function really_long_running_task() {
		return sleep( 1 );
	}

	/**
	 * Log
	 *
	 * @param string $message
	 */
	public function log( $message ) {
		error_log( $message );
	}

	/**
	 * Get lorem
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function get_message( $name,$id ) {

		$msg = $name.'-'.$id.' Inserted Successfully' ;
		
		return $msg;
	}

	/**
	 * Insert Product 
	 *
	 * @param string $message
	 */
	 public function insert_product( $data ,$main_category) {

		global $wpdb;

		//$table_redirect = $wpdb->prefix.'redirection_items';
		//$table_group = $wpdb->prefix.'redirection_groups';		

		//write_log('----------'.$data['sku'].'-------------');

		if($data['status']=='active' &&  trim(@$data['swatch'])!="" ){

			//condition true when product status ia active

			//write_log($main_category);

					// Set post data as per fields in ACF

					$data['collection'] = @$data['collection_name'] ;		
					$data['installation_method'] = @$data['installation'] ;
					$data['warranty_info'] =  @$data['warranty_text']  ;
					$data['swatch_image_link'] = $data['swatch'] ;
					$data['gallery_room_images'] = $data['gallery_images'] ;

					
					unset($data['collection_name'] );		
					unset($data['installation'] );
					unset($data['warranty_text'] );
					unset($data['swatch'] );
					unset($data['gallery_images'] );


					// args for checking already inserted product
					// find list of states in DB
					global $wpdb;	
				
					// args to query for your sku checking
						$args = array(
							'post_type' => $main_category,
							'meta_query' => array(
								array(
									'key' => 'sku',
									'value' => $data['sku']
								)
							),
							'fields' => 'ids'
						);
						// perform the query
						$query = new WP_Query( $args );
						$duplicates = $query->posts;

						if($duplicates['0']){
					//	write_log($duplicates['0']);
						}
												
						// do something if the sku exists in another post
						if ( ! empty( $duplicates ) ) {
							
							// do your stuff

								//write_log('Already-'.$data['sku'].'->'.$duplicates[0]);
								
								$post_id = $duplicates[0];
								///error_log("Updated POST ID $post_id",$duplicates[0]);		

								//write_log('----------'.$data['sku'].'----------');
								
								foreach($data as $key=>$value){

									//update sale post meta data
									update_post_meta($post_id, $key, $value); 
								}							
								
								// Insert the post into the database.
								$post_id = wp_update_post( $post_id );

						}else{

									$my_post = array(
										'post_title'    => $data['name'].' '.$data['sku'],
										'post_content'  => '',
										'post_type'  => $main_category,
										'post_status'   => 'publish',
										'post_author'   => 1,	
										'meta_input'   => $data,
						
									);

							   //remove 301 redirect for this sku

							  // $wpdb->query('DELETE FROM '.$table_redirect.' WHERE title = "'.$data['sku'].'"');

							  // $wpdb->query('DELETE FROM '.$table_redirect.' WHERE url LIKE "%'.$data['sku'].'%"');

							//   write_log($wpdb->last_query);
									
								// Insert the post into the database.
								$post_id = wp_insert_post( $my_post );
									
								
								//	$wpdb->insert( $product_check_table, array('skuid' => $data['sku'], 'post_id' =>$post_id), array( '%s', '%s'));
									
								//	write_log('New-'.$post_id." SUKD: ".$data['sku']);

								//	write_log('----------'.$data['sku'].'----------');


						}
			
					}else{
				
				
						//COndition true if status of product is inactive or dropped or gone or deleted
						//	args to query for sku which deleted from api
						$args = array(
							'post_type' => $main_category,
							'meta_query' => array(
								array(
									'key' => 'sku',
									'value' => $data['sku']
								)
							),
							'fields' => 'ids'
						);
						// perform the query
						$query = new WP_Query( $args );
						$deleted = $query->posts;
						if ( ! empty( $deleted ) ) {
		
						
						//write_log(' Deleted this product -'.$deleted['0'].' SKU -'.$data['sku']);
		
						$brandmapping = array(
							"/flooring/carpet/products/"=>"carpeting",
							"/flooring/hardwood/products/"=>"hardwood_catalog",
							"/flooring/laminate/products/"=>"laminate_catalog",
							"/flooring/"=>"luxury_vinyl_tile",
							"/flooring/tile/products/"=>"tile_catalog",
							"/flooring/waterproof/"=>"solid_wpc_waterproof"
						);
						

					   $destination_url = array_search($main_category,$brandmapping);
		
					   $source_url = wp_make_link_relative(get_permalink($deleted['0']));

					   $match_url = rtrim($source_url, '/');
		
					  // write_log($product_permalink);
					  
					//    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
        			// 	$redirect_group =  $datum[0]->id;

					//    $data = array("url" => $source_url,
					//    "match_url" => $match_url,
					//    "match_data" => "",
					//    "action_code" => "301",
					//    "action_type" => "url",
					//    "action_data" => $destination_url,
					//    "match_type" => "url",
					//    "title" => $data['sku'],
					//    "regex" => "true",
					//    "group_id" => $redirect_group,
					//    "position" => "1",
					//    "last_access" => current_time( 'mysql' ),
					//    "status" => "enabled");

					//    $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

					//    $wpdb->insert($table_redirect,$data,$format);


						
						// write_log(' Added 301 redirect -'.$deleted['0'].' SKU -'.$data['sku']);

						 wp_delete_post( $deleted['0']);
		
						// write_log('----------'.$data['sku'].'----------');
						}
		
		
					}
		 }
		
		}