<?php 


include( dirname( __FILE__ ) . '/include/apicaller.php' );
include( dirname( __FILE__ ) . '/include/constant.php' );
include( dirname( __FILE__ ) . '/include/helper.php' );
include( dirname( __FILE__ ) . '/include/track-leads.php' );    
//require_once dirname( __FILE__ ) . '/product-import.php';


/** GTM */
include( dirname( __FILE__ ) . '/include/gtm-manager.php' ); 
//include( dirname( __FILE__ ) . '/include/track-leads.php' );    
//new Main_Processing();

require_once plugin_dir_path( __FILE__ ) . 'example-plugin.php';

require_once( ABSPATH . "wp-includes/pluggable.php" );

/**  Array Filter function **/

function getArrayFiltered($aFilterKey, $aFilterValue, $array) {
    $filtered_array = array();
    foreach ($array as $value) {
        if (isset($value->$aFilterKey)) {
            if ($value->$aFilterKey == $aFilterValue) {
                $filtered_array[] = $value;
            }
        }
    }

    return $filtered_array;
}

     
global $jal_db_version;
$jal_db_version = '2.8';



function jal_install() {
	global $wpdb;
	global $jal_db_version;

	
	
	$charset_collate = $wpdb->get_charset_collate();

    $dataload = "SET FOREIGN_KEY_CHECKS=0";
    $wpdb->query( $dataload );

	$sql = "DROP TABLE IF EXISTS `wp_product_check`;
            CREATE TABLE `wp_product_check` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `skuid` varchar(100) COLLATE utf8_unicode_ci NOT NULL UNIQUE ,
            `post_id` bigint(20) unsigned NOT NULL UNIQUE,
            PRIMARY KEY (`id`),
            KEY `fk_post_id` (`post_id`),
            CONSTRAINT `fk_post_id` FOREIGN KEY (`post_id`) REFERENCES `wp_posts` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );


    
    $insert = "insert into wp_product_check(skuid,post_id) SELECT meta_value,post_id FROM wp_postmeta WHERE meta_key = 'sku'";
    $wpdb->query( $insert);
    $dataload = "SET FOREIGN_KEY_CHECKS=1;";
    $wpdb->query( $dataload );

    
    if(get_site_option( 'jal_db_version' ) !== false ){
        update_option( 'jal_db_version', $jal_db_version );
    }
    else{
        
        add_option( 'jal_db_version', $jal_db_version );
    }

	
}



function getIntervalTime($day){

    date_default_timezone_set("Asia/Kolkata");
     $time = (int)time();  
    $daytime = (int)strtotime('next '.$day); 
    return  ($daytime +34200) - $time;
   
}


/**  Cron job timing **/

add_filter( 'cron_schedules', 'example_add_cron_interval' );

function example_add_cron_interval( $schedules ) 
{ 
    
    $schedules['one_min'] = array(
        'interval' => 3600,
        'display' => esc_html__( 'Every one hour' ),
    );
    $schedules['each_monday'] = array(
        'interval' => getIntervalTime('monday'),
        'display' => esc_html__( 'Each Monday' ),
    );
    $schedules['each_tuesday'] = array(
        'interval' => getIntervalTime('tuesday'),
        'display' => esc_html__( 'Each Tuesday' ),
    );
    $schedules['each_wednesday'] = array(
        'interval' => getIntervalTime('wednesday'),
        'display' => esc_html__( 'Each Wednesday' ),
    );
    $schedules['each_thursday'] = array(
        'interval' => getIntervalTime('thursday'),
        'display' => esc_html__( 'Each Thursday' ),
    );
    $schedules['each_friday'] = array(
        'interval' => getIntervalTime('friday'),
        'display' => esc_html__( 'Each Friday' ),
    );
    $schedules['each_saturday'] = array(
        'interval' => getIntervalTime('saturday'),
        'display' => esc_html__( 'Each Saturday' ),
    );

    return $schedules;
}



/** Carpet Shaw Product sync Cron job **/

if (! wp_next_scheduled ( 'sync_carpet_monday_event_shaw')) {

    $interval =  getIntervalTime('monday');    
    wp_schedule_event( time() + $interval, 'each_monday', 'sync_carpet_monday_event_shaw');
}


add_action( 'sync_carpet_monday_event_shaw', 'do_this_monday_carpet_shaw', 10, 2 );

function do_this_monday_carpet_shaw() {    
   
    wp_mail( 'velocity.syncproduct@gmail.com',  get_bloginfo(). '- Carpet Shaw Sync Started '.date("Y-m-d h:i:s",time()),'Carpet Product Sync Started' ); 
 
   global $wpdb;
   $upload = wp_upload_dir();
   $upload_dir = $upload['basedir'];
   $upload_dir = $upload_dir . '/sfn-data';

   for( $x=1;$x<100;$x++){

   write_log('Carpet batch shaw sync started - '.$x);

   $permfile = $upload_dir.'/carpeting_shaw.json';
   //$res = SOURCEURL.'/fyw/www/carpet/'.$carpet->manufacturer.'.json?status=active&status=pending&status=dropped&status=gone&rpp=1000&pg='.$x;
  // $res = SOURCEURL.get_option('SITE_CODE').'/fyw/www/carpet/shaw.json?'.SFN_STATUS_PARAMETER.'&rpp=1000&pg='.$x;
   $res = 'https://sfn3.mm-api.agency/fyw/www/carpet/shaw.json?status=active&status=inactive&status=pending&status=dropped&status=deleted&status=remove&status=gone&status=unknown&allowZeroPrice=true&hideMissingImages=true&rpp=1000&pg='.$x;

   write_log($res);
   
   $tmpfile = download_url( $res, $timeout = 900 );
    write_log($tmpfile);

   if(is_file($tmpfile)){

       copy( $tmpfile, $permfile );
       unlink( $tmpfile ); 
       Write_log('file downloaded');
   }
   
//    Write_log('file downloaded path -'.$permfile);

   $strJsonFileContents = file_get_contents($permfile);
  
   $handle = json_decode($strJsonFileContents, true); 
   

   if (empty($handle)) {

      switch (json_last_error()) {
          case JSON_ERROR_NONE:
            write_log("No errors");
            break;
          case JSON_ERROR_DEPTH:
              write_log( "Maximum stack depth exceeded");
            break;
          case JSON_ERROR_STATE_MISMATCH:
              write_log( "Invalid or malformed JSON");
            break;
          case JSON_ERROR_CTRL_CHAR:
              write_log( "Control character error");
            break;
          case JSON_ERROR_SYNTAX:
              write_log( "Syntax error");
            break;
          case JSON_ERROR_UTF8:
              write_log( "Malformed UTF-8 characters");
            break;
          default:
            write_log( "Unknown error");
            break;
        }	

   }else{   

    write_log('auto_sync - do_this_tuesday_carpet_shaw - shaw');
    $obj = new Example_Background_Processing();
    set_time_limit(0);
    $obj->handle_all('carpeting', 'shaw');
    write_log('Carpet Shaw batch sync ended - '.$x);

   } 
}
   write_log('Sync Completed for shaw Carpet brands');    
}


/** Hardwood Product sync Cron job **/

if (! wp_next_scheduled ( 'sync_hardwood_tuesday_event')) {

    $interval =  getIntervalTime('tuesday');    
    wp_schedule_event( time() + $interval, 'each_tuesday', 'sync_hardwood_tuesday_event');
}


add_action( 'sync_hardwood_tuesday_event', 'do_this_tuesday_hardwood', 10, 2 );

function do_this_tuesday_hardwood() {      


    write_log('sync_tue'); 
    $product_json =  json_decode(get_option('product_json'));     
    $hardwood_array = getArrayFiltered('productType','hardwood',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
  
   foreach ($hardwood_array as $hardwood){

    wp_mail( 'velocity.syncproduct@gmail.com',  get_bloginfo(). ' - '.$hardwood->manufacturer.'- Hardwood Product Sync Started '.date("Y-m-d h:i:s",time()),'Hardwood Product Sync Started ' );

    $permfile = $upload_dir.'/hardwood_catalog_'.$hardwood->manufacturer.'.json';
    $res = SOURCEURL.'/fyw/www/hardwood/'.$hardwood->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
    
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    } 

    write_log('auto_sync - do_this_wednesday_hardwood-'.$hardwood->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('hardwood_catalog', $hardwood->manufacturer);

    write_log('Sync Completed - '.$hardwood->manufacturer);
    
    }

    write_log('Sync Completed for all hardwood brand');

}



/** Laminate Product sync Cron job **/

if (! wp_next_scheduled ( 'sync_laminate_wednesday_event')) {

    $interval =  getIntervalTime('wednesday'); 
    wp_schedule_event(time() + $interval , 'each_wednesday', 'sync_laminate_wednesday_event');
}

add_action( 'sync_laminate_wednesday_event', 'do_this_wednesday_laminate', 10, 2 );

function do_this_wednesday_laminate() {    
    
    wp_mail( 'velocity.syncproduct@gmail.com',  get_bloginfo(). '-FYW- Laminate Product Sync Started '.date("Y-m-d h:i:s",time()),'Laminate Product Sync Started' );

    write_log('Sync Started');
    $product_json =  json_decode(get_option('product_json'));     
    $laminate_array = getArrayFiltered('productType','laminate',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    
    
   foreach ($laminate_array as $laminate){
    write_log('brand');
    write_log( $laminate->manufacturer);

   //exit;

    $permfile = $upload_dir.'/laminate_catalog_'.$laminate->manufacturer.'.json';
    $res = SOURCEURL.'/fyw/www/laminate/'.$laminate->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
    write_log($res);
    
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    } 

    write_log('auto_sync - sync_laminate_thursday_event-'.$laminate->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('laminate_catalog', $laminate->manufacturer);   
    write_log('Sync Completed - '.$laminate->manufacturer);
    
    }
    write_log('Sync Completed all laminate brand');

}



/** LVT Product sync Cron job **/


// wp_clear_scheduled_hook( 'sync_lvt_thursday_event' );
if (! wp_next_scheduled ( 'sync_lvt_thursday_event')) {      

    $interval =  getIntervalTime('thursday');    
    wp_schedule_event( time() + $interval + 10800 , 'each_thursday', 'sync_lvt_thursday_event');
}

add_action( 'sync_lvt_thursday_event', 'do_this_thursday_lvt', 10, 2 );

function do_this_thursday_lvt() {

    //check_product_group_redirection();

    wp_mail( 'velocity.syncproduct@gmail.com',  get_bloginfo(). '- LVT Product Sync Started '.date("Y-m-d h:i:s",time()) , 'LVT Product Sync Started');

    $product_json =  json_decode(get_option('product_json'));     
    $lvt_array = getArrayFiltered('productType','lvt',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
   
   foreach ($lvt_array as $lvt){

    $permfile = $upload_dir.'/luxury_vinyl_tile_'.$lvt->manufacturer.'.json';
    $res = SOURCEURL.'/fyw/www/lvt/'.$lvt->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
  
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    }                
  

    write_log('auto_sync - sync_lvt_thursday_event-'.$lvt->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('luxury_vinyl_tile', $lvt->manufacturer);
   
    write_log('Sync Completed - '.$lvt->manufacturer);

    }

    write_log('Sync Completed for all lvt brand');
    
}

/** Tile Product sync Cron job **/

if (! wp_next_scheduled ( 'sync_tile_friday_event')) {

    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() + $interval + 10800, 'each_friday', 'sync_tile_friday_event');
}


add_action( 'sync_tile_friday_event', 'do_this_friday_tile', 10, 2 );

function do_this_friday_tile() {

    wp_mail( 'velocity.syncproduct@gmail.com',  get_bloginfo(). '- Tile Product Sync Started '.date("Y-m-d h:i:s",time()), 'Tile Product Sync Started' );

    $product_json =  json_decode(get_option('product_json'));     
    $tile_array = getArrayFiltered('productType','tile',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    
   foreach ($tile_array as $tile){

    $permfile = $upload_dir.'/tile_catalog_'.$tile->manufacturer.'.json';
    $res = SOURCEURL.'/fyw/www/tile/'.$tile->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
   
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    }   
   

    write_log('auto_sync - sync_carpet_friday_event-'.$tile->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('tile_catalog', $tile->manufacturer);
   
    write_log('Sync Completed - '.$tile->manufacturer);    
    }    
    write_log('Sync Completed');
    
}



function myplugin_update_db_check() {
    
    
    global $jal_db_version;
    if(get_site_option( 'jal_db_version' ) !== false ){
        if ( get_site_option( 'jal_db_version' ) != $jal_db_version ) {
            jal_install();
        }
    }
    else{
        
        jal_install();
    }
    
}
function getRetailerPagesInfo($locationid){
    // global $wpdb;
   // $location_ids = "00977,44248,47557,44568,23181,40065,37256,24665,42159,47531,43801,48912,01186,35906,39429,00359,49288,31452,37374,00146,35962,36046,07849,37307,22966,49318,00756,46433,41544,41477,19989,01319,49316,49317,49319,00589,04625,01992,01994,02496,02498,02499,19567,22627,22629,22631,22632,22633,22721,22977,24500,24501,24502,49900,36964,31159,36992,00323,09410,05621,41546,42974,27633,39431,00206,00210,40177,48304,02024,41434,34836,43988,30457,01993,40976,42062,36679,39777,40003,41440,43638,13423,13424,35964,38493,48830,48834,44650,07414,19794,23877,28223,28224,28225,36079,42134,22909,00541,18377,01109,49320,00554,19070,01616,25955,27210,02014,24293,19086,43670,39735,27720,39958,01524,48308,02041,41449,42030,26394,07416,36090,16593,01199,45023,19236,41371,01757,02384,48259,09221,36255,06545,00660,36275,48426,00754,06252,43493,47536,47810,30185,02206,37356,42118,15351,18353,38596,49814,20043,39326,41499,00739,36331,00529,22964,23901,43825,23038,02512,36056,01284,00437,01535,37817,31408,42776,44633,44634,00778,00779,00780,00781,22603,22604,24428,36358,39376,27541,40533,39996,48389,11855,19613,30687,43870,36022,15538,29533,37499,29861,48104,35973,49321,35891,39934,41110,41433,36043,41724,02199,02317,31464,49569,11346,15016,02279,41415,38748,38745,38766,39768,00877,36428,36463,38255,48446,45426,45352,48860,36700,24409,00308,07217,39229,01204,00975,00764,35959,39732,13369,37632";
    $query = 'select p.post_title,post_parent,p.ID
    from wp_posts p
    join wp_postmeta pm on pm.post_id = p.id
    where p.post_type = "page" AND
    p.post_title in ('.$locationid.') AND
    pm.meta_key = "_wp_page_template" AND meta_value="template-location-id.php"';
    $retailer = $wpdb->get_results($query);
   
    
   
    foreach($retailer as $key=>$value){
        $child = 'select p.ID
                    from wp_posts p
                    where p.post_type = "page" AND
                    p.post_parent = '.$retailer[$key]->ID; 
                    $childern = $wpdb->get_results($child);
                   
                    $ids = array();
                    echo $retailer[$key]->ID;
                    echo $retailer[$key]->post_title;
                   
                    foreach($childern as $key1){
                        array_push($ids,$key1->ID);
                    }
                    array_push($ids,$key1->ID);
                   $string = implode(",",$ids);
                 
               
       
    }
    return $update;
}

function draftRetailerPages($locationid){
     // global $wpdb;
    // $location_ids = "00977,44248,47557,44568,23181,40065,37256,24665,42159,47531,43801,48912,01186,35906,39429,00359,49288,31452,37374,00146,35962,36046,07849,37307,22966,49318,00756,46433,41544,41477,19989,01319,49316,49317,49319,00589,04625,01992,01994,02496,02498,02499,19567,22627,22629,22631,22632,22633,22721,22977,24500,24501,24502,49900,36964,31159,36992,00323,09410,05621,41546,42974,27633,39431,00206,00210,40177,48304,02024,41434,34836,43988,30457,01993,40976,42062,36679,39777,40003,41440,43638,13423,13424,35964,38493,48830,48834,44650,07414,19794,23877,28223,28224,28225,36079,42134,22909,00541,18377,01109,49320,00554,19070,01616,25955,27210,02014,24293,19086,43670,39735,27720,39958,01524,48308,02041,41449,42030,26394,07416,36090,16593,01199,45023,19236,41371,01757,02384,48259,09221,36255,06545,00660,36275,48426,00754,06252,43493,47536,47810,30185,02206,37356,42118,15351,18353,38596,49814,20043,39326,41499,00739,36331,00529,22964,23901,43825,23038,02512,36056,01284,00437,01535,37817,31408,42776,44633,44634,00778,00779,00780,00781,22603,22604,24428,36358,39376,27541,40533,39996,48389,11855,19613,30687,43870,36022,15538,29533,37499,29861,48104,35973,49321,35891,39934,41110,41433,36043,41724,02199,02317,31464,49569,11346,15016,02279,41415,38748,38745,38766,39768,00877,36428,36463,38255,48446,45426,45352,48860,36700,24409,00308,07217,39229,01204,00975,00764,35959,39732,13369,37632";
     $query = 'select p.post_title,post_parent,p.ID
     from wp_posts p
     join wp_postmeta pm on pm.post_id = p.id
     where p.post_type = "page" AND
     p.post_title in ('.$locationid.') AND
     pm.meta_key = "_wp_page_template" AND meta_value="template-location-id.php"';
     $retailer = $wpdb->get_results($query);
    
     
    
     foreach($retailer as $key=>$value){
         $child = 'select p.ID
                     from wp_posts p
                     where p.post_type = "page" AND
                     p.post_parent = '.$retailer[$key]->ID; 
                     $childern = $wpdb->get_results($child);
                    
                     $ids = array();
                     echo $retailer[$key]->ID;
                     echo $retailer[$key]->post_title;
                    
                     foreach($childern as $key1){
                         array_push($ids,$key1->ID);
                     }
                     array_push($ids,$key1->post_parent);
                     
                     
                    $string = implode(",",$ids);
                    var_dump($string);
                    $update = 'Update wp_posts set post_status = "draft" where ID in ('.$string.')';
                //    var_dump($wpdb->query($update));
                    echo $update;
                
        
     }
}

add_action( 'after_setup_theme', 'myplugin_update_db_check' );

if(isset($_POST['locationcode']) ){

    print_r($_POST['locationcode']);
    $result = explode(",",$_POST['locationcode']);
    getRetailerInfoNEW($result);
    print_r($result);exit;
    exit;

}

if(isset($_GET['draftretailer']) && $_GET['draftretailer']==341 ){

    global $wpdb;
    //$location_ids = "00977,44248,47557,44568,23181,40065,37256,24665,42159,47531,43801,48912,01186,35906,39429,00359,49288,31452,37374,00146,35962,36046,07849,37307,22966,49318,00756,46433,41544,41477,19989,01319,49316,49317,49319,00589,04625,01992,01994,02496,02498,02499,19567,22627,22629,22631,22632,22633,22721,22977,24500,24501,24502,49900,36964,31159,36992,00323,09410,05621,41546,42974,27633,39431,00206,00210,40177,48304,02024,41434,34836,43988,30457,01993,40976,42062,36679,39777,40003,41440,43638,13423,13424,35964,38493,48830,48834,44650,07414,19794,23877,28223,28224,28225,36079,42134,22909,00541,18377,01109,49320,00554,19070,01616,25955,27210,02014,24293,19086,43670,39735,27720,39958,01524,48308,02041,41449,42030,26394,07416,36090,16593,01199,45023,19236,41371,01757,02384,48259,09221,36255,06545,00660,36275,48426,00754,06252,43493,47536,47810,30185,02206,37356,42118,15351,18353,38596,49814,20043,39326,41499,00739,36331,00529,22964,23901,43825,23038,02512,36056,01284,00437,01535,37817,31408,42776,44633,44634,00778,00779,00780,00781,22603,22604,24428,36358,39376,27541,40533,39996,48389,11855,19613,30687,43870,36022,15538,29533,37499,29861,48104,35973,49321,35891,39934,41110,41433,36043,41724,02199,02317,31464,49569,11346,15016,02279,41415,38748,38745,38766,39768,00877,36428,36463,38255,48446,45426,45352,48860,36700,24409,00308,07217,39229,01204,00975,00764,35959,39732,13369,37632";
    $location_ids = "15218";
    $query = 'select p.post_title,post_parent,p.ID
    from wp_posts p
    join wp_postmeta pm on pm.post_id = p.id
    where p.post_type = "page" AND
    p.post_title in ('.$location_ids.') AND
    pm.meta_key = "_wp_page_template" AND meta_value="template-location-id.php"';
    $retailer = $wpdb->get_results($query);
    get_page_children( $page_id, $pages );
    
    foreach($retailer as $key=>$value){
        $child = 'select p.ID
                    from wp_posts p
                    where p.post_type = "page" AND
                    p.post_parent = '.$retailer[$key]->ID; 
                    $childern = $wpdb->get_results($child);
                    
                    $ids = array();
                    echo $retailer[$key]->ID;
                    echo $retailer[$key]->post_title;
                    
                    foreach($childern as $key1){
                        array_push($ids,$key1->ID);
                        //second level childs
                        $child = 'select p.ID
                        from wp_posts p
                        where p.post_type = "page" AND
                        p.post_parent = '.$key1->ID; 
                        $subchilds = $wpdb->get_results($child);
                        foreach($subchilds as $key1){
                            array_push($ids,$key1->ID);
                        }
                    }
                   $string = implode(",",$ids);
                   var_dump($string);
                   $update = 'Update wp_posts set post_status = "draft" where ID in ('.$string.')';
                   echo "<br>";
                   var_dump($wpdb->query($update));
                   echo $update;
                
        
    }

}
// if(isset($_GET['delretailer']) && $_GET['delretailer']==341 ){

    // global $wpdb;
    // $location_ids = "13039,39424,01478,15061,01515,09901,24540,37011,43809,38487,39423,00678,31447,01270,43399,18170,07933,02329,00671,26355,18092,47762,01904,37241,36945,02127,09326,14999,46486,44713,47963,17073,15131,15132,04494,01941,39823,23157,42177,00992,17214,36888,23614,49738,14936,25853,00479,25307,41068,27612,36269,01609,17224,14994,29988,36381,01590,36969,24587,21953,08491,27956,35997,09876,39527,36738,14952,29034,28360,02097,37045,24568,39244,36052,30125,13073,24488,39513,24491,15323,37585,48152,19114,31490,39541,15035,23730,31570,02057,27194,40299,06887,36027,36452,31500,31516,04072,23071,31178,26855,48114,26791,16598,47820,36736,43725,07667,14958,39453,47754,01041,00717,35898,18200,01932,18809,41147,26966,36007,20074,43722,24316,08615,35876,47725,39552,15052,44808,15249,19128,15235,42857,48275,27129,24499,27084,43668,31463,48575,13502,18005,00363,02091,01113,36016,24393,14935,36642,24487,18833,28778,39199,41509,40308,38304,38390,39720,08904,37032,04101,42135,25846,01392,37528,18271,41091,19216,41127,00956,44738,15002,36632,15475,31194,47818,24899,39943,24645,24832,43034,01472,41531,39858,30353,35910,24536,22234,38230,24908,00603,48331,17897,06295,19045,30298,24458,15231";
    // $query = 'select p.post_title,post_parent,p.ID
    // from wp_posts p
    // join wp_postmeta pm on pm.post_id = p.id
    // where p.post_type = "page" AND
    // p.post_title in ('.$location_ids.')AND
    // pm.meta_key = "_wp_page_template" AND meta_value="template-location-id.php"';
    // $retailer = $wpdb->get_results($query);
    // echo "<pre>";
    // foreach($retailer as $key=>$value){
        ////Delete parent data first:
        // $delete = "delete
        // p,pm
        // from wp_posts p
        // join wp_postmeta pm on pm.post_id = p.id
        // where p.ID =".$retailer[$key]->post_parent ;
        // echo $delete;
        // var_dump($wpdb->query($delete));
        ////Get the childs
        // echo $retailer[$key]->post_title;
        // $child = 'select p.ID
                    // from wp_posts p
                    // join wp_postmeta pm on pm.post_id = p.id
                    // where p.post_type = "page" AND
                    // p.post_parent = '.$retailer[$key]->ID; 
                    // $childern = $wpdb->get_results($child);
                    // $ids = array();
                    // foreach($childern as $key1){
                        // array_push($ids,$key1->ID);
                    // }
                   // array_push($ids,$retailer[$key]->ID);
                   // $string = implode(",",$ids);
                   // var_dump($string);
        // $delete1 = "delete
            // p,pm
            // from wp_posts p
            // join wp_postmeta pm on pm.post_id = p.id
            // where p.ID in (".$string.")" ;
        // echo $delete1;
        // var_dump($wpdb->query($delete1));
        
    // }
    
// }
if(isset($_GET['retailerinfo']) && $_GET['retailerinfo']==341 ){
    
    
    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>"test",'client_secret'=>'test');
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
    
    
    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){
        //Retailer Info
        global $wpdb;
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        //$retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/participants","GET",$inputs,$headers);
        echo "https://leadgen.mm-api.agency/fyw/locations";
        var_dump($inputs,$headers);
        $retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/locations","GET",$inputs,$headers);
        echo "<pre>";
        print_r($retailers);exit;
        
        $phoneupdated =$noretainlerfound =$nophonenumbers =0;
        
        foreach($retailers as $key => $value){               
        
			 if($retailers[$key]['forwardingPhone'] !=""){
                  
                    $skus = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_id_api' AND meta_value=".$retailers[$key]['locationCode']);
                    //var_dump($skus);
                    if(count($skus) > 0){

                        $post_id = $skus[0]->post_id;

                        $phone_key = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id=".$post_id);
                        if(count($phone_key) > 0){
                            $update = "UPDATE {$wpdb->postmeta} set meta_value='".$retailers[$key]['forwardingPhone']."'   WHERE  meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id = '".$post_id."'";
                        //    $wpdb->query( $update );
                            echo $update."<br>";
                            $phoneupdated++;
                        }else{

                         $insert =  "insert into {$wpdb->postmeta} (post_id,meta_key,meta_value) values (".$post_id.",'wpsl_retailer_forwardingphnumber','".$retailers[$key]['forwardingPhone']."') ";    
							   echo $insert."<br>";;
                           //$wpdb->query( $insert );
                           $phoneupdated++;
                        }

                        

                    }
                    else{
                        echo "No recored found ".$retailers[$key]['locationCode']."<br>";    
                        $noretainlerfound++;
                        
                    }
                }
                 else{
                    echo "No forwardign number"."<br>";
                    $nophonenumbers++;
                } 
			
			
           
        }
        vardump( $nophonenumbers,$phoneupdated,$noretainlerfound);
      
    }
    
}
if(isset($_GET['syncretailerinfo']) && $_GET['syncretailerinfo']==341 ){
    
    
    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>"test",'client_secret'=>'test');
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
    
    
    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){
        global $wpdb;
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/retailers/","GET",$inputs,$headers);
        
        $i =0;
        foreach($retailers as $key => $value){               
            $inputs = array();
            $headers = array('authorization'=>"bearer ".$result['access_token']);
            $single_retailer = $apiObj->call("https://leadgen.mm-api.agency/fyw/retailer/".$value['retailerId'],"GET",$inputs,$headers);
           
            
            //Get the result of single retailer
            if($single_retailer['success'] ){
            echo "<pre>";
            print_r($single_retailer);
            exit;     
                if(isset($single_retailer['result']) && is_array($single_retailer['result']['locations'])){
                    //if(isset($single_retailer['result']) && ($single_retailer['result']['participant'])){
                  
                    foreach($single_retailer['result']['locations'] as $location){              

                                
                            if($location['active']){
                                
                                ///echo "<pre>";var_dump($location);
                             //   var_dump($location['locationCode']);exit;
                                   
                                  /*  foreach($location['urls'] as $url){

                                    //Create new page
                                    //Delete existing
                                    //Update existing
                                   } */

                               


                            }
                            else{
                              
                                //get Location id
                                //Update status of location page as "Trash"
                                //Update status of location parent page as "Trash"
                                //Update status of location children page as "Trash"
                                
                            }

                    } 
                }
                else{
                    //result not found
                
                }
                
                
            }else{
               
            }
           
        }
        
        

    }
    
}



if(isset($_POST['layoutopotion'])){
    get_option('layoutopotion')?update_option('layoutopotion',$_POST['layoutopotion']):add_option('layoutopotion',$_POST['layoutopotion']);
}
if(isset($_POST['siteid']) && $_POST['siteid'] !="" && isset($_POST['clientcode']) && $_POST['clientcode'] !=""  && isset($_POST['clientsecret']) && $_POST['clientsecret'] !=""  )
    {

            //CALL Authentication API:
            $apiObj = new APICaller;
            $inputs = array('grant_type'=>'client_credentials','client_id'=>$_POST['clientcode'],'client_secret'=>$_POST['clientsecret']);
            $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
            
            
            if(isset($result['error'])){
                $msg =$result['error'];                
                $_SESSION['error'] = $msg;
                $_SESSION["error_desc"] =$result['error_description'];
                
            }
            else if(isset($result['access_token'])){

             /*    get_option('CLIENT_CODE')?update_option('CLIENT_CODE',$_POST['clientcode']):add_option('CLIENT_CODE',$_POST['clientcode']);
                get_option('SITE_CODE')?update_option('SITE_CODE',$_POST['siteid']):add_option('SITE_CODE',$_POST['siteid']); */
                get_option('CLIENTSECRET')?update_option('CLIENTSECRET',$_POST['clientsecret']):add_option('CLIENTSECRET',$_POST['clientsecret']);
                get_option('ACCESS_TOKEN')?update_option('ACCESS_TOKEN',$result['access_token']):add_option('ACCESS_TOKEN',$result['access_token']);
                get_option('CDE_ENV')?update_option('CDE_ENV',$_POST['instance-select']):add_option('CDE_ENV',$_POST['instance-select']); 
                get_option('CDE_LAST_SYNC_TIME')?update_option('CDE_LAST_SYNC_TIME',time()):add_option('CDE_LAST_SYNC_TIME',time()); 
                //API Call for Social Icon
                $inputs = array();
               /*  $headers = array('authorization'=>"bearer ".$result['access_token']);
                $sociallinks = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SOCIALURL,"GET",$inputs,$headers);
                
                if(isset($sociallinks['success']) && $sociallinks['success'] == 1 ){
                    $social_json =  json_encode($sociallinks['result']);
                    get_option('social_links') || get_option('social_links')==""?update_option('social_links',$social_json):add_option('social_links',$social_json);   
                }
                else{
                    $msg =$sociallinks['message'];                
                    $_SESSION['error'] = "Error SOCIAL LINKS";
                    $_SESSION["error_desc"] =$msg;
                    //echo $msg;
                    
                } */
                
                //API Call for getting website INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                
                if(isset($website['success']) && $website['success'] ){

                    $website_json = json_encode($website['result']);
                    get_option('website_json')?update_option('website_json',$website_json):add_option('website_json',$website_json);
                    if ( ! function_exists( 'post_exists' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/post.php' );
                    }
                    
                    
                    /* for($i=0;$i<count($website['result']['locations']);$i++){
                        $location_name = isset($website['result']['locations'][$i]['name'])?$website['result']['locations'][$i]['name']:"";

                        if(post_exists($location_name) == 0 && $location_name != "" ){

                            $array = array(
                                'post_title' => $location_name,
                                'post_type' => 'store-locations',
                                'post_content'  => "LOCATIONS",
                                'post_status'   => 'publish',
                                'post_author'   => 0,
                            );
                            $post_id = wp_insert_post( $array );
                            
                            
                        }
                    
                    } */

                    if(isset($website['result']['sites'])){
                        for($k=0;$k<count($website['result']['sites']);$k++){
                            if($website['result']['sites'][$k]['instance'] == ENV){
                                update_option('blogname',$website['result']['sites'][$k]['name']);
                                $gtmid = $website['result']['sites'][$k]['gtmId'];
                                get_option( 'gtm_script_insert')?update_option( 'gtm_script_insert',$gtmid):update_option( 'gtm_script_insert',$gtmid);
                                get_option('CLIENT_CODE')?update_option('CLIENT_CODE',$_POST['clientcode']):add_option('CLIENT_CODE',$_POST['clientcode']);
                                
                                $clientCode = $website['result']['sites'][$k]['clientCode'];
                                $siteCode= $website['result']['sites'][$k]['siteCode'];
                                get_option('SITE_CODE')?update_option('SITE_CODE',$siteCode):add_option('SITE_CODE',$siteCode);
                                get_option('CLIENT_CODE')?update_option('CLIENT_CODE',$clientCode):add_option('CLIENT_CODE',$clientCode);
                            }
                        }
                    }
                    //get_option( 'gtm_script_insert', )
                    
                }
                else{
                    $msg =$website['message'];                
                    //echo $msg;
                } 
               /*  //API Call for getting Contact INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $contacts = $apiObj->call(BASEURL.CONTACTURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                if(isset($contacts['success']) && $contacts['success'] ){

                    $contacts_json = json_encode($contacts['result']);
                    get_option('contacts_json')?update_option('contacts_json',$contacts_json):add_option('contacts_json',$contacts_json);
                    
                    $phone = isset($contacts['result'][0]['phone'])?$contacts['result'][0]['phone']:'';
                    
                    get_option('api_contact_phone')?update_option('api_contact_phone',$phone):update_option('api_contact_phone',$phone);
                
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Error Contact Info";
                    $_SESSION["error_desc"] =$msg;
                } */
                

                //API Call for geting product brand details:
                    
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $products = $apiObj->call(SOURCEURL.get_option('CLIENT_CODE')."/".PRODUCTURL,"GET",$inputs,$headers);
                
                
            
                if(isset($products) ){

                    $product_json = json_encode($products);
                    get_option('product_json')?update_option('product_json',$product_json):add_option('product_json',$product_json);
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Product Brand API";
                    $_SESSION["error_desc"] =$msg;
                }
            }

          
            
           
    }
    else{
        $msg = "Please fill all fields";
        //echo $msg;
    }
    
   
   
 /*    
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    //'https://bitbucket.org/user-name/repo-name',
    'https://bitbucket.org/mobilemarketingllc/grandchild-plugin',
	__FILE__,
	'grand-child'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
	'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));
 */


//Optional: Set the branch that contains the stable release.
/* if(ENV == 'dev' || ENV == 'staging')
    $myUpdateChecker->setBranch('dev');
else if(ENV == 'prod')
    $myUpdateChecker->setBranch('master'); */

 function wpc_theme_global() {
    
/*     define( 'postpercol', '3' );
    include( dirname( __FILE__ ) . '/styles.php' );
    // define( 'productdetail_layout', 'box' );    
    define( 'productdetail_layout', 'box' );  
    wp_enqueue_style('lightbox-style', plugins_url('css/lightgallery.min.css', __FILE__));
    wp_enqueue_script('lightbox-js',plugins_url( 'js/lightgallery-all.min.js', __FILE__));
    wp_enqueue_script('script-js',plugins_url( 'js/script.js', __FILE__)); */
    gtm_head_script();
}
add_action('wp_head', 'wpc_theme_global');

function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'-child/product-addon/css/admin.css');
    wp_enqueue_script('script', get_template_directory_uri().'-child/product-addon/js/retailer_v1.js');
}
add_action('admin_enqueue_scripts', 'admin_style');
// This filter replaces a complete file from the parent theme or child theme with your file (in this case the archive page).
// Whenever the archive is requested, it will use YOUR archive.php instead of that of the parent or child theme.
//add_filter ('archive_template', create_function ('', 'return plugin_dir_path(__FILE__)."archive.php";'));

/* function wpc_theme_add_headers () {
    wp_enqueue_style('frontend-styles', plugins_url('css/styles.css', __FILE__));
} */
// These two lines ensure that your CSS is loaded alongside the parent or child theme's CSS
add_action('init', 'wpc_theme_add_headers');
 
// In the rest of your plugin, add your normal actions and filters, just as you would in functions.php in a child theme.

/* function get_custom_post_type_template($single_template) {
    global $post;
      
    if ($post->post_type != 'post') {
         $single_template = dirname( __FILE__ ) . '/product-listing-templates/single-'.$post->post_type.'.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' ); */

//Code for adding Menu
//Nikhil Chinchane: 7 Jan 2019
//
function add_my_menu() {
    add_menu_page (
        'Retailer Settings', // page title 
        'Retailer Settings', // menu title
        'manage_options', // capability
        'my-menu-slug',  // menu-slug
        'my_menu_page',   // function that will render its output
        get_template_directory_uri().'-child/product-addon/img/theme-option-menu-icon.png'   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );

    add_submenu_page('my-menu-slug', __('Retailer Product Data'), __('Retailer Product Data'), 'manage_options', 'retailer_product_data', 'retailer_product_data_html');

}
add_action('admin_menu', 'add_my_menu');


//Create Form for client code and site id:
    function Calling_API_form($site,$clientcode) {
        if(isset($_SESSION['error'])){
     ?>
        <div class="notice notice-info error-info is-dismissible woo-info">
            <div class="info-image">
                <p> <img src='<?php echo get_template_directory_uri()."-child/img/error.png";?>' width="48px"/></p>
            </div>
            <div class="info-descriptions">
                <div class="info-descriptions-title">
                        <h3><strong><?php echo ucfirst($_SESSION["error"]);?></strong></h3>
                </div>
                <p><?php echo $_SESSION["error_desc"];?></p>
                <p><b>Please Contact Plugin Development Team for further details</b></p>
            </div>
        </div>
        <?php
        }
        $form = '
        <div id="wpcontent1" class="client_info_wrap">
            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
                <table class="form-table">
                    <tr>
                        <th colspan="2"><h4>Enter Retailer Info</h4></th>
                    </tr>
                    <tr>
                        <td colspan="2" style="
                        text-align: right;
                    "><a href="/wp-admin/admin.php?page=my-menu-slug&retailerinfo=341" class="button button-primary">Sync All Forwarding Numbers</a></td>
                    </tr>
                    
                    '.( isset( $msg) ? '
                    <tr>
                        <td colspan="2">
                        <span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> '.$msg.'</span>
                            '.( !isset( $_POST['siteid'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Site ID cannot be blank</span>' : null ).'
                            '.( !isset( $_POST['clientcode'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Client Code cannot be blank</span>' : null ).'
                        </td>
                    </tr>
                    ' : null ).'
                    <tr>
                        <td width="150px">
                            <label for="siteid">Select environment <strong>*</strong></label>
                        </td>';
                        $cde = (get_option('CDE_ENV')) && get_option('CDE_ENV')!="" ?get_option('CDE_ENV'):""; 
                        $lasttimesync = get_option('CDE_LAST_SYNC_TIME')?date("F j, Y, g:i a", get_option('CDE_LAST_SYNC_TIME')):"Not yet sync";
                        if($cde != ""){
                            $form .='<td class="form-group">
                            <select name="instance-select">
                                <option value="prod" '.(  $cde=="prod" ? 'selected=selected' : null ).'>Production</option>
                                <option value="staging" '.(  $cde=="staging" ? 'selected=selected' : null ).'>Staging</option>
                                <option value="dev"  '.(  $cde=="dev" ? 'selected=selected' : null ).'>Development</option>
                            </select>
                        </td></tr>';
                        }
                        else{
                            $form .= '<td class="form-group">
                            <select name="instance-select" required>
                                <option value="">Choose Environment</option>
                                <option value="prod">Production</option>
                                <option value="staging">Staging</option>
                                <option value="dev">Development</option>
                            </select>
                        </td></tr>';
                        }
                        
                    
         $form .= '<tr>
                        <td width="150px">
                            <label for="siteid">Client Code <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="siteid" value="' . ( (get_option('CLIENT_CODE') ) ? get_option('CLIENT_CODE') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientcode">API authcode <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientcode" value="' . ( get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientsecret">API authcode Secret <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientsecret" value="' . ( get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="form-group">
                            <button id="syncdata" type="submit" class="button button-primary" >Sync</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right">Last sync time : '.$lasttimesync.'</td>
                    </tr>
                    
                </table>    
            </form>

            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
            <table class="form-table">
                <tr>
                    <th colspan="2"><h4>Enter Location Code Info</h4></th>
                </tr>
                <tr>
                    <td style="
                    width: 15%;
                ">
                        <label for="locationcode" >Enter location code needs to update<strong>*</strong></label>
                    </td>
                    <td class="form-group">
                        <textarea type="text" name="locationcode" placeholder="Please enter comma seperated location code"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="form-group">
                    <button id="syncdata" type="submit" class="button button-primary" >Sync</button>
                    </td>
                </tr>
                
            </table>
            </form>
        </div>
        ';
        echo $form;
        unset($_SESSION["error"]);
        unset($_SESSION["error_desc"]);
        
        
    }

    


function getRetailerInformation(){
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('retailer_details').'
                    
</div>
	';
}
add_shortcode( 'getRetailerInformation', 'getRetailerInformation' );

function my_menu_page() {
    retailerInformation();
}
function retailerInformation() {
        ?>
        <?php  
        if( isset( $_GET[ 'tab' ] ) ) {  
            $active_tab = $_GET[ 'tab' ];  
        } else {
            $active_tab = 'tab_one';
        }
        ?>  
        <div class="wrap" id="grandchild-backend">
            <h2>Retailer Settings</h2>
            <div class="description"></div>

            <?php Calling_API_form('','');?>
            <?php settings_errors(); ?> 

            <?php
                //$details = json_decode(get_option('retailer_details'));
                $website_json =  json_decode(get_option('website_json'));
               // $details = json_decode(get_option('social_links'));
                if($website_json || $details) { ?>
                <table class="widefat striped" border="1" style="border-collapse:collapse;">
                    <!-- <thead>
                        <tr>
                            <th width="25%"><strong>Name</strong></th>
                            <th width="75%"><strong>Values</strong></th>
                        </tr>
                    </thead>
                    <tbody> -->
                <?php
                   
                    /* foreach($website_json as $key => $value){   
                        if(!is_array($value)){
                            echo "<tr><td>".ucfirst($key)."</td><td>".ucfirst($value)."</td></tr>";
                        }   
                        
                    } */
                   /*  echo "<tr><th colspan='2'><strong>Social Platforms</strong></th></tr>";
                    if(isset($details)){
                        foreach($details as $key => $value){   
                            echo "<tr><td>".ucfirst($value->platform)."</td><td>".$value->url."</td></tr>";
                        }
                    } */
                    
                     echo "<tr><th colspan='2'><strong>Site information (".ENV.")</strong></th></tr>";
                    $website =  $website_json;
                    for($i=0;$i<count($website->sites);$i++){
                        if($website->sites[$i]->instance == ENV){
                            foreach($website->sites[$i] as $key => $value){   
                                if($key)
                                echo "<tr><td>".ucfirst($key)."</td><td>".($value)."</td></tr>";

                                if($key == "siteCode")
                                get_option('CLIENT_SITE_ID')?update_option('CLIENT_SITE_ID',$value):add_option('CLIENT_SITE_ID',$value);
                            }
                        }
                   
                } 
                   /*  echo "<tr><th colspan='2'><strong>Locations</strong></th></tr>";
                    $website =  $website_json;
                    if(isset($website->locations)){
                    for($i=0;$i<count($website->locations);$i++){

                        $location_name = isset($website_json->locations[$i]->name)?$website->locations[$i]->name:"";
                
                        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
                        $location_address  = isset($website->locations[$i]->address)?$website->locations[$i]->address:"";
                        $location_address .= isset($website->locations[$i]->city)?" , ".$website->locations[$i]->city:"";
                        $location_address .= isset($website->locations[$i]->state)?" , ".$website->locations[$i]->state:"";
                        $location_address .= isset($website->locations[$i]->postalCode)?" , ".$website->locations[$i]->postalCode:"";
                        
                        
                
                        $location_phone = isset($website->locations[$i]->phone)?$website->locations[$i]->phone:"";
                        echo "<tr><td>Name</td><td><b>".$location_name."</b></td></tr>";
                        echo "<tr><td>Address</td><td>".$location_address."</td></tr>";
                        echo "<tr><td>Phone</td><td>".$website->locations[$i]->phone."</td></tr>";
                        echo "<tr><td>Forwarding phone</td><td>".$website->locations[$i]->forwardingPhone."</td></tr>";
                        echo "<tr><td>License Number</td><td>".$website->locations[$i]->licenseNumber."</td></tr>";
                        $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
                        $openinghrs = "<ul style='display:block;'>";
                        for ($j = 0; $j < count($weekdays); $j++) {
                            $location .= $website->locations[$i]->monday;
                            if (isset($website->locations[$i]->{$weekdays[$j]})) {
                                $openinghrs .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $openinghrs .= "</ul>";
                        echo "<tr><td>Opening Hrs</td><td>".$openinghrs."</td></tr>";
                    }
                } */
                    /* $contacts = json_decode(get_option('website_json'));
                    echo "<tr><th colspan='2'><strong>Contacts</strong></th></tr>";
                    if(isset($contacts->contacts)){

                   
                    if(is_array($contacts->contacts)){
                        
                        for($j=0;$j<count($contacts->contacts);$j++){
                            foreach($contacts->contacts[$j] as $key => $value){   
                                echo "<tr><td>".ucfirst($key)."</td><td>".$value."</td></tr>";
                            }
                        }
                    }
                } */
                  //  echo do_shortcode("[storelocation_address alldata]"); 
                ?>
                    </tbody>
                </table>
                <?php 
                }
                retailer_product_data_html();        
            ?>

            
        </div>
        
        <?php
        
}





function retailer_product_data_html(){
    
    ?>
    <div id="wpcontent1" class="client_info_wrap">
  
       
            <table class="widefat striped" border="1" style="border-collapse:collapse;">
               <tbody>
                   <tr>
                       <th colspan="4">
                           <h4>Retailer Flooring Data</h4>
                       </th>
                   </tr>
                    <tr>
                        <th class="form-group"><strong>Product Type</strong></th>
                        <th class="form-group"><strong>Brand</strong></th>
                        <th class="form-group"><strong>Deal Brand</strong></th>
                        <th class="form-group"><strong>Action</strong></th>
                    </tr>

                       <?php
                            $product_json =  json_decode(get_option('product_json'));                       
                            $brandmapping = array(
                                "carpet"=>"carpeting",
                                "hardwood"=>"hardwood_catalog",
                                "laminate"=>"laminate_catalog",
                                "lvt"=>"luxury_vinyl_tile",
                                "tile"=>"tile_catalog",
                                "waterproof"=>"solid_wpc_waterproof"
                            );
                            for($i=0;$i < count($product_json);$i++){
                                ?>
                                <tr>
                                    <form name="productfrm" action=<?php echo $_SERVER['REQUEST_URI'];?> method="POST">
                                        <td class="form-group"><?php echo ucfirst($product_json[$i]->productType); ?></td>
                                        <?php
                                        
                                            if( strcmp($product_json[$i]->productType,"Carpet")){
                                                ?>
                                                <input type="hidden" name="api_product_data_category" value="<?php echo $pro_type =  $brandmapping[$product_json[$i]->productType]?>" />
                                                <input type="hidden" name="api_product_data_brand" value="<?php echo $product_json[$i]->manufacturer;?>" />
                                                <?php
                                            }
                                        ?>
                                        <td class="form-group"><?php echo $product_json[$i]->manufacturer; ?></td>
                                        <td class="form-group"><?php 
                                        
                                        global $wpdb;    
                                        $product_sync_table = $wpdb->prefix."sfn_sync";  
                                        $quer_status =  "SELECT * FROM {$product_sync_table} WHERE product_brand = '{$product_json[$i]->manufacturer}' and  product_category = '{$pro_type}'" ;
                                        $result_brand = $wpdb->get_results( $quer_status );    
                                        
                                        echo $result_brand[0]->sync_status.'==>';
                                              
                                                if( $result_brand){
        
                                                    echo $result_brand[0]->syn_datetime;
                                                }
                                        
                                        ?></td>
                                        <td class="form-group"><button id="syncdata-pro" type="submit" class="button button-primary">Sync</button></td>
                                    </form>
                                </tr>


                                <?php
                            }

                        ?>
                       
                  
               </tbody>
           </table>
      
   </div>
       <?php
       if(isset($_POST['api_product_data_category'] ) && isset($_POST['api_product_data_brand'] )){
                ob_start();
                $upload = wp_upload_dir();
                $upload_dir = $upload['basedir'];
                $upload_dir = $upload_dir . '/sfn-data';
                if (! is_dir($upload_dir)) {
                    mkdir( $upload_dir, 0700 );
                } 

                $sfnapi_category_array = array_keys($brandmapping,$_POST['api_product_data_category']);
                $sfnapi_category = $sfnapi_category_array[0];
                
                $permfile = $upload_dir.'/'.$_POST['api_product_data_category'].'_'.$_POST['api_product_data_brand'].'.csv';
               //$res = 'https://sfn.mm-api.agency/jbrcttlt/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?status=active&status=pending&status=dropped&status=gone';
               $res = SOURCEURL.get_option('CLIENT_CODE').'/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?status=active&status=pending&status=dropped&status=gone';
               
            //    $tmpfile = download_url( $res, $timeout = 900 );
            //         if(is_file($tmpfile)){
            //             var_dump(copy( $tmpfile, $permfile ));
            //             unlink( $tmpfile ); 

                        $data_url =   admin_url( 'admin.php?page=retailer_product_data&process=all&main_category='.$_POST['api_product_data_category'].'&product_brand='.$_POST['api_product_data_brand'].'');
//unset($_SESSION['error']);
                    //  }
                    //  else{
                    //     $_SESSION['error'] = "Couldn't find products for this category";
                        
                    
                    // }
               

              
          ?>
          <script>
          window.location.href = "<?php echo $data_url; ?>";
          </script>
          <?php exit();}
	  
}

/* 
if(isset($_GET['new_phone']) && $_GET['new_phone'] =1){
			//$retailers = array("25886"=>"978-770-6674","41972"=>"910-505-0508","39808"=>"850-972-0719")
			$retailers = array("25886" => "978-770-6674","41972" => "910-505-0508","39808" => "850-972-0719","38453" => "908-374-6812","44755" => "516-261-2131","31156" => "413-707-0337","5643" => "470-394-2730","599" => "203-807-8985","39230" => "301-591-8775","25826" => "571-286-5144","49744" => "781-222-3293","7860" => "315-975-4378","2121" => "910-444-7104","1561" => "856-249-0940","358" => "401-234-2526","22638" => "833-551-9140","11301" => "850-338-9054","22952" => "865-999-4489","665" => "865-999-4487","36661" => "585-431-6975","39144" => "864-900-2212","820" => "937-506-0284","49857" => "614-362-0577","6232" => "317-739-8219","8269" => "918-438-7254","8146" => "440-290-6687","2343" => "216-438-0219","7988" => "585-577-7563","1410" => "651-964-6667","473" => "502-890-1399","44129" => "984-212-2021","49188" => "803-973-3594","1615" => "704-370-9952","19941" => "217-689-1021","35929" => "203-993-6656","48583" => "615-790-6420","24373" => "475-329-4895","466" => "240-850-3249","44536" => "937-476-7445","11623" => "614-362-1320","31" => "443-269-7431","48264" => "502-890-1386","6391" => "502-890-1411","36663" => "727-513-8889","24434" => "843-823-5819","11277" => "219-402-0138","20092" => "463-212-5078","39766" => "681-248-6607","1266" => "631-542-2034","11611" => "513-712-4883","38584" => "732-314-0489","40100" => "407-278-6909","24309" => "740-248-5556","2202" => "833-553-8157","35939" => "615-691-6021","35884" => "318-545-0706","1435" => "207-819-4091","35999" => "571-396-0229","39322" => "571-396-0221","29091" => "706-960-1018","13165" => "336-666-2703","50044" => "803-756-4142","39056" => "540-680-3018","30493" => "574-830-8760","36424" => "270-600-0150","1390" => "502-233-3585","47880" => "219-237-0664","1241" => "708-719-6644","10897" => "937-569-4636","18648" => "407-848-1433","932" => "317-853-6987","566" => "518-707-0673","24995" => "540-585-1339","40113" => "620-332-6729","44778" => "225-831-6724","1481" => "530-440-7161","23095" => "541-640-5592","40996" => "541-780-0319","40997" => "541-497-7979","38451" => "925-725-6288","1249" => "517-777-0789","29947" => "760-280-2118","1360" => "270-632-2221","31171" => "262-330-0248","23994" => "660-460-8208","25250" => "573-745-4059","25251" => "573-207-2870","25252" => "660-628-0902","28880" => "641-666-6160","726" => "470-460-5239","37749" => "757-509-7828","45424" => "267-732-2143","350" => "816-596-2321","505" => "573-375-4296","40002" => "410-934-4150","1709" => "636-383-4176","11850" => "267-458-2211","7453" => "402-817-1258","18376" => "607-340-3079","1195" => "860-746-9006","55" => "989-898-9269","14895" => "973-739-8617","36893" => "303-214-5936","36006" => "419-513-2927","13331" => "815-320-2056","2325" => "919-897-5062","374" => "951-363-5802","44638" => "949-503-0551","618" => "540-613-5759","19806" => "734-430-0326","47755" => "937-404-2965","23920" => "308-529-4008","24472" => "308-320-7205","24461" => "346-323-7203","402" => "833-553-5432","43112" => "803-756-4130","37097" => "306-803-5866","3995" => "909-660-7857","41734" => "510-280-2153","23009" => "207-466-1854","659" => "803-756-4135","25199" => "602-581-7449","37857" => "937-526-2777","37318" => "240-901-3247","20057" => "952-209-3605","20061" => "952-209-3609","20063" => "952-209-3613","20069" => "952-209-3610","13475" => "240-808-6620","26837" => "901-341-7694","41192" => "346-271-8409","49437" => "346-306-0048","49583" => "512-337-3410","49584" => "512-520-0189","50039" => "512-591-0973","9295" => "512-596-0610","47908" => "301-761-2054","23044" => "920-456-0155","2390" => "386-309-2505","1917" => "417-283-4864","729" => "920-570-7886","37672" => "682-267-9551","28530" => "831-316-3592","22849" => "209-300-0308","480" => "516-247-9486","15012" => "587-209-4600","25527" => "717-661-1279","47655" => "704-769-1425","11973" => "810-309-8782","49098" => "424-383-9066","36485" => "440-481-1330","1354" => "605-593-5714","2178" => "682-267-9553","39985" => "952-209-3611","41969" => "952-209-3612","17372" => "541-780-0320","31283" => "208-647-5088","19088" => "850-733-6170","36013" => "850-696-0856","2463" => "518-689-4013","1220" => "337-366-6359","27651" => "503-446-6314","11951" => "865-448-4700","11337" => "860-780-0004","35986" => "573-377-6062","36319" => "765-319-8740","42908" => "970-623-7493","23757" => "763-296-4649","39081" => "928-707-8881","16787" => "210-742-2706","41260" => "470-394-2726","747" => "918-438-7253","36166" => "828-397-9001","10932" => "608-839-0554","31336" => "702-659-9649","4953" => "424-300-0016","40315" => "909-341-6195","9872" => "470-412-5405","939" => "425-354-4163","1064" => "256-517-9282","36305" => "651-447-5124","628" => "936-283-4281","37138" => "413-282-7837","40999" => "502-251-1910","2478" => "551-277-0926","1048" => "717-895-9429","1091" => "323-797-8273","45299" => "714-617-1898","41179" => "952-209-3607","36490" => "218-520-0478","14886" => "605-569-5099","40314" => "424-300-0017","19091" => "201-949-8869","19092" => "551-800-3022","36625" => "830-214-7074","43611" => "618-881-1055","360" => "330-509-7604","878" => "651-273-2444","40059" => "587-747-4442","40924" => "609-277-2986","1005" => "717-454-3283","50090" => "406-404-7239","960" => "734-407-7316","49745" => "304-927-7023","536" => "636-331-7062","426" => "952-209-3614","42824" => "763-296-4654","1866" => "804-601-4535","459" => "608-466-2605","22708" => "318-666-9531","49972" => "218-270-4987","38525" => "704-749-8060","14799" => "530-230-3879","17166" => "304-397-0934","35928" => "810-294-4292","1044" => "717-947-3618","47676" => "587-747-2790","5875" => "712-340-0344","26452" => "252-260-2880","40005" => "909-415-4162","8297" => "971-762-4619","1936" => "385-448-4269","41211" => "719-623-7605","28204" => "503-383-9959","5029" => "951-465-8443","863" => "586-251-0898","43987" => "316-252-3180","974" => "316-252-3180","44357" => "475-333-0082","37101" => "570-290-7530","26649" => "814-299-5403","7450" => "402-205-7199","44800" => "203-871-1215","36528" => "937-529-8261","2336" => "573-303-5600","983" => "513-854-5565","31146" => "732-523-5435","35896" => "385-300-0139","50053" => "951-816-6305","44340" => "951-245-6159","1128" => "712-796-1088","19183" => "402-817-1445","24375" => "260-297-0231","40795" => "204-515-8871","18608" => "815-669-4836","20095" => "859-800-3954","28542" => "509-530-1992","43800" => "919-552-3384","30263" => "647-360-2730","617" => "252-596-0695","36473" => "712-717-7527","811" => "913-318-6418","951" => "814-961-2849","35907" => "620-275-6590","35911" => "920-600-0016","13102" => "714-930-7775","13267" => "951-460-0156","13268" => "949-667-7294","35914" => "802-391-8731","661" => "864-900-2211","24432" => "925-403-1943","981" => "661-418-2875","25508" => "626-406-1759","22983" => "717-790-1458","35961" => "717-970-7326","37081" => "717-430-0871","36840" => "717-351-7404","1101" => "586-229-1310","6055" => "309-279-5671","43656" => "515-528-7825","774" => "707-710-9775","19776" => "707-309-0077","43498" => "580-303-5033","36069" => "269-628-8172","35975" => "760-289-5972","35877" => "651-447-5198","29913" => "260-302-1293","919" => "267-715-2298","49914" => "509-902-6179","24374" => "509-902-6176","39853" => "509-581-5134","42044" => "509-423-7102","43970" => "785-670-6169","48226" => "714-860-7046","36" => "703-936-4154","30519" => "225-831-6729","18394" => "870-444-6813","36048" => "573-410-1279","5290" => "203-800-9575","25360" => "207-492-8011","49897" => "785-502-8673","37613" => "714-646-7277","37552" => "217-707-6133","600" => "309-291-3480","244" => "919-882-3242","592" => "910-900-5437","50035" => "217-441-6533","37058" => "210-742-6930","44796" => "906-212-6461","24431" => "505-207-6175","1805" => "801-938-5860","28047" => "559-358-2148","1786" => "512-537-0582","4501" => "818-741-3495","14996" => "780-851-8992","50052" => "940-315-7900","25458" => "204-515-8897","40195" => "916-572-8089","8303" => "503-388-5859","47661" => "609-380-3376","44343" => "406-404-7284","18612" => "978-705-4737","48552" => "407-426-4056","11500" => "256-980-3381","1046" => "605-368-3389","22981" => "949-667-7297","4676" => "949-396-1786","361" => "330-539-2137","445" => "909-660-7876","49487" => "847-696-6843","17196" => "780-851-8986","27337" => "650-250-1588","50102" => "434-218-5579","327" => "530-230-3589","47856" => "708-858-4300","39372" => "309-429-6819","36067" => "630-283-1965","35981" => "760-330-5798","48361" => "208-286-4059","43975" => "919-741-6237","48213" => "509-530-1990","49870" => "408-755-3217","1057" => "989-402-0991","35983" => "618-800-6283","47534" => "219-246-2757","48441" => "780-900-5532","2511" => "262-269-1243","6849" => "586-519-9601","14804" => "616-217-9135","897" => "763-296-4648","36125" => "309-650-6639","8446" => "717-482-0271","9661" => "509-590-1523","30169" => "419-504-0173","44807" => "910-401-2805","18666" => "816-400-4938","18667" => "816-400-4935","35974" => "913-318-6415","1591" => "701-765-8114","11940" => "478-273-3145","36401" => "804-729-5177","2360" => "951-428-3906","23706" => "616-369-1988","862" => "425-409-2744","42851" => "602-581-7889","720" => "978-320-3364","17424" => "831-269-3186","23927" => "843-823-5818","2415" => "267-458-2210","28253" => "920-545-1276","45269" => "920-945-5255","9734" => "262-269-1241","49484" => "412-218-0496","13047" => "608-851-2419","47948" => "979-977-3208");
			
				foreach($retailers as $key => $value){               
     //   echo $key." ".$value;
			 if($value!=""){
                  
                    $skus = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_id_api' AND meta_value=".$key);
                    //var_dump($skus);
                    if(count($skus) > 0){

                        $post_id = $skus[0]->post_id;

                        $phone_key = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id=".$post_id);
                        if(count($phone_key) > 0){
                            $update = "UPDATE {$wpdb->postmeta} set meta_value='".$value."'   WHERE  meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id = '".$post_id."'";
                            $wpdb->query( $update );
						   //echo $update;
                            $phoneupdated++;
                        }else{

                         $insert =  "insert into {$wpdb->postmeta} (post_id,meta_key,meta_value) values (".$post_id.",'wpsl_retailer_forwardingphnumber','".$value."') ";    
							//   var_dump( $insert);
                           $wpdb->query( $insert );
						 echo $post_id ;
						 //echo $insert;
                           $phoneupdated++;
                        }

                        

                    }
                    else{
                        var_dump("No recored found ".$key);    
                        $noretainlerfound++;
                        
                    }
                }
                 else{
                   // var_dump("No forwardign number");
                    $nophonenumbers++;
                } 
			
			
           
        }exit;
		  } */
   
   
   
if(isset($_GET['new_phone']) && $_GET['new_phone'] =1){
			//$retailers = array("25886"=>"978-770-6674","41972"=>"910-505-0508","39808"=>"850-972-0719")
            //$retailers = array("25886" => "978-770-6674","41972" => "910-505-0508","39808" => "850-972-0719","38453" => "908-374-6812","44755" => "516-261-2131","31156" => "413-707-0337","5643" => "470-394-2730","599" => "203-807-8985","39230" => "301-591-8775","25826" => "571-286-5144","49744" => "781-222-3293","7860" => "315-975-4378","2121" => "910-444-7104","1561" => "856-249-0940","358" => "401-234-2526","22638" => "833-551-9140","11301" => "850-338-9054","22952" => "865-999-4489","665" => "865-999-4487","36661" => "585-431-6975","39144" => "864-900-2212","820" => "937-506-0284","49857" => "614-362-0577","6232" => "317-739-8219","8269" => "918-438-7254","8146" => "440-290-6687","2343" => "216-438-0219","7988" => "585-577-7563","1410" => "651-964-6667","473" => "502-890-1399","44129" => "984-212-2021","49188" => "803-973-3594","1615" => "704-370-9952","19941" => "217-689-1021","35929" => "203-993-6656","48583" => "615-790-6420","24373" => "475-329-4895","466" => "240-850-3249","44536" => "937-476-7445","11623" => "614-362-1320","31" => "443-269-7431","48264" => "502-890-1386","6391" => "502-890-1411","36663" => "727-513-8889","24434" => "843-823-5819","11277" => "219-402-0138","20092" => "463-212-5078","39766" => "681-248-6607","1266" => "631-542-2034","11611" => "513-712-4883","38584" => "732-314-0489","40100" => "407-278-6909","24309" => "740-248-5556","2202" => "833-553-8157","35939" => "615-691-6021","35884" => "318-545-0706","1435" => "207-819-4091","35999" => "571-396-0229","39322" => "571-396-0221","29091" => "706-960-1018","13165" => "336-666-2703","50044" => "803-756-4142","39056" => "540-680-3018","30493" => "574-830-8760","36424" => "270-600-0150","1390" => "502-233-3585","47880" => "219-237-0664","1241" => "708-719-6644","10897" => "937-569-4636","18648" => "407-848-1433","932" => "317-853-6987","566" => "518-707-0673","24995" => "540-585-1339","40113" => "620-332-6729","44778" => "225-831-6724","1481" => "530-440-7161","23095" => "541-640-5592","40996" => "541-780-0319","40997" => "541-497-7979","38451" => "925-725-6288","1249" => "517-777-0789","29947" => "760-280-2118","1360" => "270-632-2221","31171" => "262-330-0248","23994" => "660-460-8208","25250" => "573-745-4059","25251" => "573-207-2870","25252" => "660-628-0902","28880" => "641-666-6160","726" => "470-460-5239","37749" => "757-509-7828","45424" => "267-732-2143","350" => "816-596-2321","505" => "573-375-4296","40002" => "410-934-4150","1709" => "636-383-4176","11850" => "267-458-2211","7453" => "402-817-1258","18376" => "607-340-3079","1195" => "860-746-9006","55" => "989-898-9269","14895" => "973-739-8617","36893" => "303-214-5936","36006" => "419-513-2927","13331" => "815-320-2056","2325" => "919-897-5062","374" => "951-363-5802","44638" => "949-503-0551","618" => "540-613-5759","19806" => "734-430-0326","47755" => "937-404-2965","23920" => "308-529-4008","24472" => "308-320-7205","24461" => "346-323-7203","402" => "833-553-5432","43112" => "803-756-4130","37097" => "306-803-5866","3995" => "909-660-7857","41734" => "510-280-2153","23009" => "207-466-1854","659" => "803-756-4135","25199" => "602-581-7449","37857" => "937-526-2777","37318" => "240-901-3247","20057" => "952-209-3605","20061" => "952-209-3609","20063" => "952-209-3613","20069" => "952-209-3610","13475" => "240-808-6620","26837" => "901-341-7694","41192" => "346-271-8409","49437" => "346-306-0048","49583" => "512-337-3410","49584" => "512-520-0189","50039" => "512-591-0973","9295" => "512-596-0610","47908" => "301-761-2054","23044" => "920-456-0155","2390" => "386-309-2505","1917" => "417-283-4864","729" => "920-570-7886","37672" => "682-267-9551","28530" => "831-316-3592","22849" => "209-300-0308","480" => "516-247-9486","15012" => "587-209-4600","25527" => "717-661-1279","47655" => "704-769-1425","11973" => "810-309-8782","49098" => "424-383-9066","36485" => "440-481-1330","1354" => "605-593-5714","2178" => "682-267-9553","39985" => "952-209-3611","41969" => "952-209-3612","17372" => "541-780-0320","31283" => "208-647-5088","19088" => "850-733-6170","36013" => "850-696-0856","2463" => "518-689-4013","1220" => "337-366-6359","27651" => "503-446-6314","11951" => "865-448-4700","11337" => "860-780-0004","35986" => "573-377-6062","36319" => "765-319-8740","42908" => "970-623-7493","23757" => "763-296-4649","39081" => "928-707-8881","16787" => "210-742-2706","41260" => "470-394-2726","747" => "918-438-7253","36166" => "828-397-9001","10932" => "608-839-0554","31336" => "702-659-9649","4953" => "424-300-0016","40315" => "909-341-6195","9872" => "470-412-5405","939" => "425-354-4163","1064" => "256-517-9282","36305" => "651-447-5124","628" => "936-283-4281","37138" => "413-282-7837","40999" => "502-251-1910","2478" => "551-277-0926","1048" => "717-895-9429","1091" => "323-797-8273","45299" => "714-617-1898","41179" => "952-209-3607","36490" => "218-520-0478","14886" => "605-569-5099","40314" => "424-300-0017","19091" => "201-949-8869","19092" => "551-800-3022","36625" => "830-214-7074","43611" => "618-881-1055","360" => "330-509-7604","878" => "651-273-2444","40059" => "587-747-4442","40924" => "609-277-2986","1005" => "717-454-3283","50090" => "406-404-7239","960" => "734-407-7316","49745" => "304-927-7023","536" => "636-331-7062","426" => "952-209-3614","42824" => "763-296-4654","1866" => "804-601-4535","459" => "608-466-2605","22708" => "318-666-9531","49972" => "218-270-4987","38525" => "704-749-8060","14799" => "530-230-3879","17166" => "304-397-0934","35928" => "810-294-4292","1044" => "717-947-3618","47676" => "587-747-2790","5875" => "712-340-0344","26452" => "252-260-2880","40005" => "909-415-4162","8297" => "971-762-4619","1936" => "385-448-4269","41211" => "719-623-7605","28204" => "503-383-9959","5029" => "951-465-8443","863" => "586-251-0898","43987" => "316-252-3180","974" => "316-252-3180","44357" => "475-333-0082","37101" => "570-290-7530","26649" => "814-299-5403","7450" => "402-205-7199","44800" => "203-871-1215","36528" => "937-529-8261","2336" => "573-303-5600","983" => "513-854-5565","31146" => "732-523-5435","35896" => "385-300-0139","50053" => "951-816-6305","44340" => "951-245-6159","1128" => "712-796-1088","19183" => "402-817-1445","24375" => "260-297-0231","40795" => "204-515-8871","18608" => "815-669-4836","20095" => "859-800-3954","28542" => "509-530-1992","43800" => "919-552-3384","30263" => "647-360-2730","617" => "252-596-0695","36473" => "712-717-7527","811" => "913-318-6418","951" => "814-961-2849","35907" => "620-275-6590","35911" => "920-600-0016","13102" => "714-930-7775","13267" => "951-460-0156","13268" => "949-667-7294","35914" => "802-391-8731","661" => "864-900-2211","24432" => "925-403-1943","981" => "661-418-2875","25508" => "626-406-1759","22983" => "717-790-1458","35961" => "717-970-7326","37081" => "717-430-0871","36840" => "717-351-7404","1101" => "586-229-1310","6055" => "309-279-5671","43656" => "515-528-7825","774" => "707-710-9775","19776" => "707-309-0077","43498" => "580-303-5033","36069" => "269-628-8172","35975" => "760-289-5972","35877" => "651-447-5198","29913" => "260-302-1293","919" => "267-715-2298","49914" => "509-902-6179","24374" => "509-902-6176","39853" => "509-581-5134","42044" => "509-423-7102","43970" => "785-670-6169","48226" => "714-860-7046","36" => "703-936-4154","30519" => "225-831-6729","18394" => "870-444-6813","36048" => "573-410-1279","5290" => "203-800-9575","25360" => "207-492-8011","49897" => "785-502-8673","37613" => "714-646-7277","37552" => "217-707-6133","600" => "309-291-3480","244" => "919-882-3242","592" => "910-900-5437","50035" => "217-441-6533","37058" => "210-742-6930","44796" => "906-212-6461","24431" => "505-207-6175","1805" => "801-938-5860","28047" => "559-358-2148","1786" => "512-537-0582","4501" => "818-741-3495","14996" => "780-851-8992","50052" => "940-315-7900","25458" => "204-515-8897","40195" => "916-572-8089","8303" => "503-388-5859","47661" => "609-380-3376","44343" => "406-404-7284","18612" => "978-705-4737","48552" => "407-426-4056","11500" => "256-980-3381","1046" => "605-368-3389","22981" => "949-667-7297","4676" => "949-396-1786","361" => "330-539-2137","445" => "909-660-7876","49487" => "847-696-6843","17196" => "780-851-8986","27337" => "650-250-1588","50102" => "434-218-5579","327" => "530-230-3589","47856" => "708-858-4300","39372" => "309-429-6819","36067" => "630-283-1965","35981" => "760-330-5798","48361" => "208-286-4059","43975" => "919-741-6237","48213" => "509-530-1990","49870" => "408-755-3217","1057" => "989-402-0991","35983" => "618-800-6283","47534" => "219-246-2757","48441" => "780-900-5532","2511" => "262-269-1243","6849" => "586-519-9601","14804" => "616-217-9135","897" => "763-296-4648","36125" => "309-650-6639","8446" => "717-482-0271","9661" => "509-590-1523","30169" => "419-504-0173","44807" => "910-401-2805","18666" => "816-400-4938","18667" => "816-400-4935","35974" => "913-318-6415","1591" => "701-765-8114","11940" => "478-273-3145","36401" => "804-729-5177","2360" => "951-428-3906","23706" => "616-369-1988","862" => "425-409-2744","42851" => "602-581-7889","720" => "978-320-3364","17424" => "831-269-3186","23927" => "843-823-5818","2415" => "267-458-2210","28253" => "920-545-1276","45269" => "920-945-5255","9734" => "262-269-1241","49484" => "412-218-0496","13047" => "608-851-2419","47948" => "979-977-3208");
                        $retailers = array("35976" => "931-800-3492","47944" => "757-378-6819","23172" => "780-851-8988","8398" => "724-471-8740","47779" => "724-426-8847","663" => "402-817-1426","19837" => "763-296-6837","1134" => "617-812-3182","1135" => "617-812-3183","36608" => "617-812-3184","47543" => "617-812-3185","23993" => "778-728-0394","39443" => "778-728-0865","38279" => "443-420-6068","1425" => "843-823-5817","1587" => "760-670-3379","977" => "937-476-7412","23772" => "989-295-3314","23771" => "989-321-2801","23773" => "989-898-9270","28535" => "609-842-5607","18776" => "989-402-0673","6877" => "616-369-1984","25886" => "978-770-6674","923" => "402-512-0890","41972" => "910-505-0508","44248" => "910-505-0519","47557" => "910-505-0530","41934" => "812-650-0812","18620" => "610-492-6232","44568" => "484-339-3754","28951" => "715-609-2436","47687" => "306-803-5948","1655" => "719-374-9204","3996" => "786-544-2671","7697" => "575-359-7240","6991" => "507-591-9418","39114" => "317-805-1292","39808" => "850-972-0719","23180" => "860-323-3154","23181" => "860-607-0594","1652" => "760-330-5796","15051" => "780-851-8997","39514" => "780-851-8998","39152" => "469-210-8355","8469" => "717-297-0129","11122" => "267-703-3573","28923" => "267-703-3571","28924" => "267-703-3572","40065" => "1-888-267-9588","47759" => "604-334-8213","37256" => "408-755-3213","11061" => "616-369-1985","49896" => "587-401-5995","40178" => "760-853-2229","11546" => "760-640-9923","38453" => "908-374-6812","24469" => "845-391-0520","15049" => "780-851-8994","24665" => "252-424-0380","23825" => "256-216-4714","47647" => "470-839-8892","49944" => "705-300-9423","49945" => "647-264-8465","49946" => "647-264-8356","15212" => "289-768-2898","15213" => "289-768-5078","15214" => "365-653-5032","15215" => "289-348-0332","15216" => "289-326-1057","15217" => "289-327-0740","15218" => "289-334-0907","15219" => "226-227-9699","15220" => "289-319-1014","15221" => "226-213-5410","15223" => "613-519-2381","15224" => "613-699-9425","15225" => "613-699-6849","15226" => "226-314-1720","15227" => "1-888-265-3804","29563" => "647-360-2875","15174" => "365-652-6438","44755" => "516-261-2131","11630" => "336-916-1136","11631" => "336-496-0074","2272" => "661-371-4945","29128" => "559-785-9764","30515" => "346-299-1875","42198" => "346-332-0185","30516" => "346-703-1304","30517" => "936-283-4280","14930" => "587-747-4438","14967" => "647-360-2998","1571" => "701-502-0971","3985" => "256-459-2849","24598" => "253-656-4252","2479" => "551-900-2051","7546" => "551-295-0760","49767" => "413-526-6152","31156" => "413-707-0337","5643" => "470-394-2730","1286" => "636-735-7379","42159" => "618-607-4048","47531" => "618-607-4049","2451" => "814-826-3717","48260" => "337-435-6264","12020" => "850-972-0781","36438" => "850-972-0797","20002" => "517-619-0578","39070" => "517-619-0579","599" => "203-807-8985","50040" => "928-492-7413","31473" => "831-296-4173","37696" => "831-269-3186","36748" => "989-303-0307","4006" => "318-666-9524","40160" => "425-354-4521","49533" => "360-488-2360","5511" => "941-666-7632","42056" => "253-656-4364","43801" => "336-489-8018","12007" => "628-888-1890","41212" => "618-230-4598","24577" => "503-388-9673","25111" => "971-762-4621","42174" => "503-694-9978","45420" => "503-477-5881","2235" => "267-704-4562","43082" => "571-371-1787","689" => "470-900-0049","35987" => "628-207-4312","49001" => "609-981-8085","43803" => "609-981-8086","48912" => "609-981-8087","11679" => "781-350-5415","38769" => "780-851-8985","40407" => "424-358-4708","39230" => "301-591-8775","50043" => "604-373-6609","47791" => "470-394-2724","23055" => "470-394-2725","1409" => "609-622-3548","11182" => "609-536-9034","11183" => "856-229-0358","11184" => "609-380-9051","11185" => "609-545-2793","11186" => "732-440-9943","11187" => "732-703-7126","11455" => "302-467-3578","23875" => "267-768-8326","23917" => "267-485-5761","39257" => "484-806-1942","40167" => "856-272-7879","40168" => "848-241-0521","42115" => "267-590-9214","25826" => "571-286-5144","22607" => "714-922-3572","29159" => "701-922-5940","49744" => "781-222-3293","41422" => "209-300-0305","7860" => "315-975-4378","1186" => "603-505-8328","5999" => "219-232-8990","1843" => "989-292-6138","2121" => "910-444-7104","41182" => "636-265-6012","45423" => "717-661-1280","1828" => "715-609-2437","11997" => "575-315-0008","35906" => "323-815-7966","44786" => "928-350-6606","31465" => "330-510-1655","49795" => "330-840-5404","1807" => "401-795-3735","1561" => "856-249-0940","39429" => "870-306-2198","4264" => "870-306-2199","50080" => "870-417-2087","50081" => "870-306-2200","50082" => "573-901-0544","50083" => "870-412-4038","50084" => "870-576-2160","50085" => "870-306-2202","50086" => "870-306-2203","50087" => "870-759-7246","50088" => "870-306-2197","50079" => "573-551-0194","358" => "401-234-2526","359" => "401-250-5811","4075" => "435-292-8059","27312" => "303-536-5787","856" => "304-621-6332","851" => "269-580-8713","22638" => "833-551-9140","23166" => "443-292-9101","24646" => "240-732-1189","49288" => "443-292-9103","40000" => "231-668-6016","17050" => "346-369-7094","31452" => "970-409-4684","11307" => "417-501-3768","11498" => "747-291-0502","36030" => "762-226-2202","28203" => "254-613-7549","41167" => "580-303-5015","36761" => "714-860-7048","1087" => "207-494-4145","1918" => "812-758-8062","23792" => "812-250-2589","2348" => "913-318-6417","761" => "831-204-9927","812" => "267-890-7668","2301" => "320-300-5029","48895" => "636-229-1658","2100" => "630-225-7065","37374" => "630-423-5058","9740" => "414-485-0463","49765" => "551-295-0761","15050" => "780-851-8989","44806" => "507-403-7590","1200" => "256-517-9281","38133" => "317-252-1161","1216" => "336-666-2702","7167" => "406-845-7461","38405" => "603-570-6497","47764" => "714-633-7889","43628" => "562-330-2781","49306" => "714-587-3658","26021" => "541-590-7520","40023" => "605-593-5680","547" => "863-401-4372","2528" => "801-980-7218","8435" => "814-317-9957","36727" => "747-291-0490","2220" => "502-890-1448","31177" => "864-757-4015","144" => "727-275-9282","145" => "727-513-8892","146" => "813-497-7707","147" => "727-597-4258","148" => "941-800-1130","150" => "941-666-7635","151" => "727-513-8894","152" => "941-666-7630","153" => "863-250-1568","2446" => "813-497-7708","2447" => "352-340-2205","2448" => "727-275-9293","2449" => "727-275-9336","2450" => "813-820-1534","35962" => "727-513-8893","48103" => "813-553-3241","48298" => "352-619-1534","11986" => "520-999-8032","29389" => "520-999-8028","2181" => "530-270-9845","35967" => "717-297-0128","23930" => "203-896-7841","1359" => "860-926-0627","9048" => "979-415-3092","50089" => "435-233-7349","2300" => "833-551-5661","36046" => "812-668-6400","47946" => "308-870-7754","22628" => "918-419-0223","17168" => "269-503-8426","26897" => "513-334-2223","1020" => "484-339-3753","1276" => "484-401-7476","11067" => "610-492-6142","40066" => "661-383-2864","11301" => "850-338-9054","44875" => "623-246-7330","11488" => "402-947-0350","23639" => "208-725-3927","23101" => "208-423-8997","44642" => "208-725-3929","41572" => "205-920-2535","27091" => "205-994-7354","6995" => "507-616-0381","8201" => "405-724-2788","295" => "520-999-8033","22952" => "865-999-4489","665" => "865-999-4487","36661" => "585-431-6975","39144" => "864-900-2212","26242" => "406-262-8913","37495" => "406-551-4364","46473" => "509-822-6284","49353" => "208-618-2967","35905" => "574-376-2616","7849" => "845-316-1496","37307" => "845-316-1500","2177" => "641-390-9670","40089" => "570-689-1170","36047" => "573-433-3300","820" => "937-506-0284","49857" => "614-362-0577","2340" => "408-766-3803","1808" => "734-800-3285","806" => "904-758-8115","31136" => "469-907-4986","31137" => "903-352-3052","44497" => "469-210-8373","35900" => "571-424-1686","29760" => "608-409-3353","35949" => "478-201-9762","7433" => "701-765-8115","8147" => "440-739-3773","6232" => "317-739-8219","1928" => "512-580-3798","1078" => "470-397-9818","37620" => "803-847-5890","8269" => "918-438-7254","22711" => "501-392-8269","40307" => "260-245-1320","47540" => "318-666-9525","9887" => "361-450-3178","13071" => "207-389-3015","24630" => "714-912-9481","11938" => "575-200-1180","8146" => "440-290-6687","2343" => "216-438-0219","47912" => "330-249-7274","45235" => "440-901-0691","1577" => "530-350-5203","24438" => "775-689-0946","18635" => "920-350-4050","7988" => "585-577-7563","1410" => "651-964-6667","1639" => "978-231-9089","48650" => "978-231-9090","1021" => "508-815-3547","649" => "207-387-1462","36644" => "704-370-9683","473" => "502-890-1399","44129" => "984-212-2021","22966" => "828-527-6850","49188" => "803-973-3594","573" => "774-371-4250","36207" => "774-371-4251","50060" => "626-360-4833","49318" => "580-808-4765","1752" => "571-376-7286","987" => "574-307-5275","36031" => "602-581-7884","1615" => "704-370-9952","44640" => "980-680-6158","23010" => "704-703-1268","19941" => "217-800-5193","11411" => "804-442-7444","756" => "412-212-7381","24270" => "406-281-7413","46432" => "509-530-1993","41529" => "509-381-1451","46433" => "509-822-2315","1700" => "616-212-1444","895" => "913-318-6419","23969" => "833-553-4885","637" => "479-974-1488","30422" => "831-264-8752","24424" => "501-273-1001","35929" => "203-993-6656","41544" => "203-993-6661","36728" => "401-468-0392","38920" => "602-581-7450","48434" => "760-546-2596","25667" => "410-803-5567","41477" => "410-657-0606","19989" => "704-703-1267","1319" => "336-536-0577","1374" => "346-323-7202","6293" => "913-318-6413","19864" => "913-346-2595","19865" => "816-400-4933","19867" => "913-318-6420","19868" => "913-318-6412","19869" => "816-775-4100","2536" => "317-743-1120","21733" => "330-840-5405","2216" => "636-283-2568","2227" => "619-416-3571","48583" => "615-790-6420","41526" => "615-933-5588","41121" => "303-214-5935","44804" => "303-214-0366","23043" => "707-273-4728","24484" => "352-444-1754","4516" => "818-732-6891","48287" => "580-634-7551","49316" => "918-438-7251","49317" => "405-621-3500","49319" => "580-771-4198","589" => "951-396-4314","18623" => "442-263-0007","4625" => "442-263-0008","1992" => "303-214-9137","1994" => "303-214-0360","2496" => "303-214-0353","2498" => "303-214-0356","2499" => "303-217-9884","19567" => "303-214-0429","22627" => "719-374-9206","22629" => "303-214-1060","22631" => "970-305-5591","22632" => "303-214-5934","22633" => "970-400-1163","22721" => "307-274-3406","22977" => "303-214-0768","24500" => "719-374-9205","24501" => "303-214-5933","24502" => "303-214-0352","49900" => "303-214-0369","24372" => "631-209-7208","691" => "631-657-2141","1368" => "956-666-1359","31546" => "931-368-4051","668" => "484-326-3952","24373" => "475-329-4895","466" => "240-850-3249","1280" => "920-456-0154","39828" => "701-543-8253","38297" => "406-510-6700","36541" => "346-299-1876","44536" => "937-476-7445","6080" => "217-619-8181","309" => "757-901-0356","1972" => "626-349-1527","39764" => "626-225-0911","11623" => "614-362-1320","36964" => "614-362-1305","35902" => "443-543-9166","31" => "443-269-7431","36015" => "502-735-0363","49397" => "828-516-4181","1185" => "603-421-6540","1180" => "443-589-7098","37830" => "520-999-8031","11524" => "937-695-8516","31159" => "503-863-0601","25460" => "219-706-2843","41479" => "661-363-0646","36992" => "661-371-4921","2420" => "908-791-5715","37420" => "908-791-5857","13129" => "785-789-3888","15210" => "647-360-2985","6669" => "240-531-8005","319" => "785-670-6395","320" => "660-460-8213","322" => "308-244-4120","324" => "620-591-2004","44651" => "507-237-6341","13130" => "660-220-2146","44942" => "785-670-6402","323" => "785-741-8446","22927" => "620-207-8173","317" => "580-297-6827","6989" => "507-666-6023","1739" => "424-383-8954","9410" => "571-662-5924","48264" => "502-890-1386","6391" => "502-890-1411","35908" => "325-777-1394","1864" => "870-330-7224","1325" => "424-387-3985","1326" => "818-666-6561","36953" => "907-302-5160","36200" => "502-890-1389","7236" => "919-391-9347","24447" => "424-328-5101","2501" => "424-383-9211","36663" => "727-513-8889","5621" => "727-513-8890","5455" => "813-497-7703","29311" => "780-851-8991","27982" => "929-205-8456","24434" => "843-823-5819","36765" => "425-620-3880","11277" => "219-402-0138","41546" => "219-402-0137","584" => "424-500-0014","20092" => "463-212-5078","18789" => "443-637-6694","35982" => "610-492-6144","808" => "269-580-8718","36942" => "309-434-5597","1756" => "217-619-8179","1564" => "815-216-4325","37039" => "309-434-5613","46479" => "217-883-4121","47530" => "779-601-2095","43077" => "779-601-2096","1658" => "217-800-5196","22605" => "309-285-8991","44353" => "309-279-5644","1718" => "309-434-5618","1960" => "217-441-6531","39766" => "681-248-6607","352" => "201-254-8883","23042" => "907-302-5880",
                        "36398" => "907-202-5656","1266" => "631-542-2034","15546" => "619-416-3572","15547" => "858-935-4560","18580" => "408-755-3620","18583" => "650-230-6169","18586" => "925-587-3422","35940" => "510-343-9374","11611" => "513-712-4883","39150" => "330-451-6943","39816" => "330-451-6941","1234" => "402-817-1463","13167" => "402-819-6967","44508" => "209-300-0307","4883" => "209-684-7435","35979" => "701-543-8254","11466" => "414-439-2376","43805" => "262-290-3425","46446" => "414-436-0228","46484" => "262-455-9343","46490" => "262-800-0073","48443" => "262-735-8428","49469" => "414-485-0464","48121" => "414-436-4123","49464" => "262-455-9341","48122" => "262-290-3678","49118" => "262-800-0074","38584" => "732-314-0489","47923" => "929-262-6686","40100" => "407-278-6909","19126" => "714-646-7672","36535" => "385-497-6147","1958" => "252-299-5759","1258" => "443-456-4208","42974" => "410-989-4081","35958" => "336-405-8119","27633" => "951-534-5852","991" => "419-329-4848","50046" => "567-331-3080","50045" => "419-379-3102","50047" => "419-491-7607","22592" => "816-775-4102","39421" => "587-747-4406","1600" => "702-659-5398","4931" => "530-764-5088","44499" => "602-581-7885","48169" => "916-562-3395",
                        "1817" => "262-553-6028","39984" => "608-409-3354","8845" => "731-202-9254","15097" => "226-400-4465","50103" => "226-227-9698","6762" => "734-234-1724","39431" => "928-412-8388","48382" => "231-668-6044","206" => "430-243-0054","210" => "903-472-0989","1729" => "951-465-8669","5536" => "352-619-1535","26019" => "417-429-1165","44691" => "760-507-2125","621" => "207-466-4009","15011" => "587-747-4411","40177" => "587-747-4424","48304" => "1-888-256-8774","28838" => "587-802-0419","976" => "937-900-0673","2024" => "619-375-1196","41434" => "833-560-0059","11682" => "616-369-1989","1281" => "717-316-0234","34836" => "717-316-0548","23806" => "984-265-3807","43806" => "423-417-1693","43988" => "802-391-8730","27183" => "757-337-5017","30036" => "971-915-7804","25813" => "443-282-9504","43471" => "443-282-9503","36070" => "872-205-0788","38770" => "309-305-3332","24309" => "740-248-5556","2261" => "920-340-8062","2202" => "833-553-8157","50100" => "619-375-0813","6185" => "260-888-2270","49331" => "628-888-1891","47686" => "587-747-4430","35939" => "615-691-6021","35884" => "318-545-0706","19048" => "314-764-6438","17035" => "580-771-4197","1172" => "513-275-6869","31470" => "432-219-6428","2116" => "864-900-2214","36039" => "201-972-6968","1435" => "207-819-4091","5520" => "941-315-4638","43865" => "941-315-4693","41981" => "913-353-4020","23866" => "571-365-7856","35999" => "571-396-0229","39322" => "571-396-0221","43779" => "571-662-5923","45417" => "571-371-1784","29091" => "706-960-1018","1116" => "308-562-2591","13165" => "336-666-2703","37089" => "660-869-6413","543" => "218-655-8078","43048" => "308-708-2724","1617" => "270-561-7131","39729" => "270-386-1076","30457" => "843-273-6268","48966" => "206-426-4766","29440" => "574-622-2007","732" => "270-600-0149","19253" => "805-635-8910","15534" => "346-248-3191","15535" => "936-283-4279","41080" => "346-298-7983","36001" => "360-812-6486","1993" => "303-214-0384","50044" => "803-756-4142","7455" => "402-942-6173","2267" => "812-565-9752","39056" => "540-680-3018","30493" => "574-830-8760","42959" => "520-999-8027","1248" => "616-369-1986","35942" => "816-988-4200","11410" => "304-907-6128","23078" => "470-394-2727","50071" => "269-433-8120","36986" => "587-747-4112","44743" => "423-243-0257","1040" => "425-448-1341","11841" => "206-426-3864","11842" => "253-954-3206","22272" => "425-409-2407","24574" => "971-762-4622","30179" => "503-607-8512","30180" => "503-285-5927","30218" => "360-604-2725","35978" => "503-388-9342","41536" => "253-656-4251","47689" => "971-762-4617","47690" => "503-360-1809","19171" => "260-888-2267","37233" => "260-888-2268","38464" => "704-243-9812","35893" => "719-931-9528","19961" => "303-214-0364","40976" => "706-903-8051","1946" => "623-246-7329","36424" => "270-600-0150","1390" => "502-233-3585","1538" => "509-792-3670","43614" => "541-640-5596","43615" => "406-897-7137","27374" => "509-581-5133","27375" => "541-667-4252","27373" => "509-761-4877","27376" => "509-593-0660","2140" => "801-996-4553","42062" => "801-938-5035","36703" => "804-593-0370","325" => "660-240-0846","5789" => "470-863-3028","1226" => "715-609-2433","36679" => "715-609-2434","34820" => "830-215-1072","22839" => "570-228-2368","44772" => "570-228-2384","28179" => "252-644-6073","5691" => "470-400-3537","3984" => "907-302-5883","39777" => "484-549-2073","40003" => "610-492-6231","9615" => "509-940-2128","24619" => "236-302-0001","47880" => "219-237-0664","1241" => "708-719-6644","10897" => "937-569-4636","882" => "850-972-0638","18648" => "407-848-1433","5386" => "407-316-2875","8351" => "302-894-7798","36886" => "302-894-7823","11486" => "833-557-1523","37435" => "302-515-7842","43710" => "623-246-7325","2516" => "833-556-2772","2537" => "610-492-6230","14938" => "587-747-4433","16574" => "361-200-6051","23068" => "956-267-9592","23069" => "956-731-0228","24356" => "210-742-9078","41440" => "505-207-6174","43638" => "512-596-5661","44737" => "361-761-0240","7329" => "910-444-7101","16712" => "204-515-8893","122" => "716-458-7610","123" => "716-458-7042","124" => "716-458-7631","127" => "716-458-7614","128" => "716-458-7616","129" => "716-526-6274","35954" => "716-740-2020","1508" => "417-313-9436","22968" => "435-258-7966","36769" => "602-581-7452","932" => "317-853-6987","31474" => "931-221-2983","36005" => "406-792-7361","1050" => "607-361-1387","1742" => "210-742-8343","43823" => "210-742-6342","39105" => "760-507-2123","1777" => "479-279-1706","43766" => "717-778-4376","41938" => "319-988-6552","43746" => "682-477-3640","41411" => "206-426-4901","1672" => "904-431-7738","40187" => "734-407-7302","50072" => "248-533-0977","41492" => "470-394-2729","30335" => "470-601-6761","38492" => "762-226-2203","43771" => "762-226-2204","15054" => "587-315-1215","29018" => "587-355-0010","24314" => "236-361-0893","37901" => "254-431-2635","36990" => "224-970-1700","566" => "518-707-0673","11020" => "405-561-3254","7097" => "417-241-6818","2270" => "616-369-1983","13423" => "331-236-0801","13424" => "708-866-4200","35964" => "630-410-1071","597" => "206-426-3524","2169" => "833-452-2799","1716" => "508-233-8569","1759" => "334-310-9920","37112" => "251-288-5807","677" => "336-234-1284","15003" => "587-747-4418","37458" => "616-209-9515","11484" => "616-369-1978","31253" => "616-369-1980","38493" => "616-369-1981","39050" => "616-369-1979","48830" => "616-209-9773","48834" => "616-209-9766","24995" => "540-585-1339","1520" => "509-590-1524","44498" => "509-590-1543","6238" => "219-312-2633","44650" => "219-964-0248","40113" => "620-332-6729","318" => "660-386-3099","321" => "417-680-0138","326" => "620-207-8174","47548" => "660-220-2145","18457" => "760-483-4904","44778" => "225-831-6724","2111" => "240-556-5673","39197" => "443-214-0959","1507" => "484-549-2072","9275" => "979-977-3210","24318" => "254-304-9213","11861" => "254-304-9214","39866" => "254-314-8898","39867" => "979-977-3209","24327" => "254-314-8926","1481" => "530-440-7161","43841" => "419-419-1815","4016" => "603-586-0087","11607" => "980-446-2355","39164" => "760-621-9200","23095" => "541-640-5592","40996" => "541-780-0319","40997" => "541-497-7979","38451" => "925-725-6288","40071" => "269-999-1354","1249" => "517-777-0789","24331" => "985-262-2698","29947" => "760-280-2118","1084" => "386-205-6950","47539" => "337-417-9614","1355" => "318-666-9522","1360" => "270-632-2221","8669" => "401-404-4869","1788" => "757-930-5143","24437" => "405-888-8429","30132" => "947-207-0389","45419" => "920-212-2079","31171" => "262-330-0248","3999" => "229-258-8870","23994" => "660-460-8208","25250" => "573-745-4059","25251" => "573-207-2870","25252" => "660-628-0902","17413" => "847-725-0798","22115" => "469-907-4987","43753" => "469-907-4988","5891" => "641-390-9669","28880" => "641-666-6160","24516" => "667-701-1300","24283" => "323-843-2910","790" => "715-303-4718","35971" => "813-497-7706","726" => "470-460-5239","38413" => "715-609-2430","14955" => "613-519-2376","48271" => "803-615-4606","467" => "980-680-6157","37749" => "757-509-7828","42777" => "757-856-5983","7414" => "252-648-0455","45424" => "267-732-2143","19794" => "570-979-3152","23877" => "530-261-8476","48706" => "912-244-4187","28222" => "660-235-9158","350" => "816-596-2321","28223" => "660-220-2142","28224" => "833-541-2703","28225" => "912-244-4196","36079" => "660-235-9157","42134" => "515-528-7822","47770" => "515-528-7824","9863" => "706-530-9396","22990" => "478-207-5004","35945" => "256-244-8006","130" => "256-814-0482","22909" => "530-657-0195","48894" => "636-410-7266","48896" => "636-779-4089","49117" => "314-835-9517","481" => "314-858-6578","482" => "314-764-6448","23007" => "314-764-6451","24476" => "314-782-5737","41466" => "636-779-4770","14894" => "317-813-9559","505" => "573-375-4296","15032" => "604-373-0016","24415" => "604-373-0017","39469" => "604-373-0024","4648" => "909-654-7845","1738" => "470-394-2721","37093" => "470-394-2722","47793" => "470-394-2723","24872" => "252-375-3450","40002" => "410-934-4150","1709" => "636-383-4176","110" => "704-833-8675","11850" => "267-458-2211","7453" => "402-817-1258","44760" => "704-370-9874","683" => "707-710-9773","17186" => "628-236-1130","541" => "505-207-6177","13476" => "505-207-6176","39327" => "479-322-7140","44406" => "479-322-7141","15088" => "778-402-8640","28596" => "236-599-8010","22834" => "604-425-0993","15085" => "604-239-3111","23162" => "236-361-0895","22835" => "778-726-0447","37036" => "604-373-6610","28495" => "778-402-8647","22832" => "604-243-0052","22836" => "604-337-0909","22837" => "778-728-0243","23113" => "604-334-8215","26595" => "204-515-8883","18376" => "607-340-3079","18377" => "607-353-1136","1195" => "860-746-9006","2238" => "757-300-5566","1109" => "802-764-8055","55" => "989-898-9269","14895" => "973-739-8617","24546" => "631-343-5383","49320" => "469-460-6321","1925" => "248-717-1670","36860" => "604-373-6607","24492" => "778-730-0415","43840" => "702-659-5405","36893" => "303-214-5936","36020" => "251-288-5474","920" => "470-460-5241","36006" => "419-513-2927","422" => "973-870-4688","38734" => "201-299-6932","38735" => "973-826-4506","38736" => "551-301-0379","38738" => "973-814-2149","38739" => "201-949-8913","13331" => "815-320-2056","39368" => "506-800-8072","2325" => "919-897-5062","841" => "401-543-2449","931" => "865-999-4490","9270" => "833-553-5829","43782" => "386-516-3955","374" => "951-363-5802","49355" => "360-504-0342","49356" => "360-504-0353","619" => "251-270-4954","44638" => "949-503-0551","49903" => "289-312-0978","1750" => "605-273-7907","47937" => "442-255-2775","2458" => "667-777-0316","43330" => "657-400-4199","40120" => "562-380-2571","39187" => "904-602-6204","12053" => "701-639-7261","36033" => "724-320-3568","554" => "910-463-5189","618" => "540-613-5759","1456" => "518-245-6671","564" => "270-267-4569","48256" => "609-491-2125","29346" => "402-360-7186","514" => "470-460-5238","1076" => "973-705-7026","38460" => "321-985-5011","19806" => "734-430-0326","47755" => "937-404-2965","23920" => "308-529-4008","24472" => "308-320-7205","17057" => "346-803-2536","24461" => "346-323-7203","45254" => "346-358-3228","596" => "270-971-4497","19070" => "267-715-2299","80" => "470-472-0021","35935" => "470-472-0022","402" => "833-553-5432","28636" => "925-412-3018","43112" => "803-756-4130","37097" => "306-803-5866","1430" => "541-640-5595","18578" => "816-400-4936","3995" => "909-660-7857","41734" => "510-280-2153","13096" => "714-656-2995","44793" => "424-300-0018","36053" => "434-218-5409","22583" => "207-466-1854","23009" => "207-466-1990","659" => "803-756-4135","36880" => "304-461-8197","25199" => "602-581-7449","6557" => "774-415-7082","37857" => "937-526-2777","47501" => "207-407-9010","36996" => "443-550-7512","928" => "207-407-9011","1616" => "443-550-7516","25955" => "443-659-2188","47962" => "918-233-2649","46499" => "850-739-6291","138" => "805-586-3363","18995" => "805-842-5104","44563" => "805-834-8695","44673" => "805-342-2592","46436" => "805-666-2600","2139" => "816-287-3356","36044" => "803-973-3592","1304" => "919-991-5492","6872" => "616-369-1987","37318" => "240-901-3247","23760" => "440-588-8102","27210" => "289-768-5023","31296" => "309-434-5564","31297" => "217-800-5194","11680" => "409-600-8299","20057" => "952-209-3605","20061" => "952-209-3609","20062" => "612-421-7013","20063" => "952-209-3613","20069" => "952-209-3610","41205" => "612-421-7015","43714" => "763-296-4655","20066" => "952-209-3604","41207" => "612-421-7014","41208" => "612-428-0188","43977" => "763-296-4652","43978" => "612-428-0187","911" => "765-767-7991","28685" => "463-212-5085","2014" => "812-565-9758","2017" => "502-890-1452","2018" => "502-890-1441","18472" => "502-890-1455","18473" => "812-274-5096","24293" => "859-800-3952","39592" => "785-670-6411","44570" => "785-670-6418","13475" => "240-808-6620","19086" => "240-808-6619","43670" => "903-352-3051","23776" => "843-874-6094","26256" => "226-214-1623","26837" => "901-341-7694","2136" => "402-347-5095","41192" => "346-271-8409","49437" => "346-306-0048","37610" => "478-273-3144","49583" => "512-337-3410","49584" => "512-520-0189","50039" => "512-591-0973","9295" => "512-596-0610","23807" => "352-492-1355","37835" => "240-616-2218","47908" => "301-761-2054","18866" => "240-474-0257","22761" => "301-460-1852","39312" => "443-545-7268","39735" => "717-790-1457","23044" => "920-456-0155","37491" => "920-322-7793","27720" => "262-256-0028","39958" => "262-800-0076","40916" => "570-910-2317","2390" => "386-309-2505","1361" => "302-644-5589","1917" => "417-283-4864","729" => "920-570-7886","37672" => "682-267-9551","39219" => "321-294-4071","49357" => "772-261-0027","49358" => "954-953-6680","28530" => "831-316-3592","47901" => "782-827-5255","1524" => "402-819-6937","40017" => "346-299-1894","40018" => "346-299-1893","40019" => "346-299-1895","40020" => "346-299-1887","41252" => "346-202-7173","41253" => "346-299-1886","41254" => "346-299-1892","41971" => "346-273-4673","43467" => "346-299-1899","43468" => "346-299-1896","43469" => "346-299-1898","44493" => "346-299-1901","44494" => "346-299-9163","44495" => "346-299-1897","44496" => "346-299-9166","44603" => "346-299-9164","45253" => "346-299-1890","46482" => "346-333-2763","48011" => "346-325-1246","48427" => "346-209-6929","49364" => "346-299-1891","49363" => "346-299-1889","30445" => "910-361-2949","48148" => "910-335-8121","48308" => "910-548-7227","42095" => "587-768-0278","2041" => "910-367-5795","22849" => "209-300-0308","49247" => "417-374-2303","41449" => "774-901-4102","42030" => "774-317-7034","366" => "484-496-6152","26394" => "856-528-8128","480" => "516-247-9486","47803" => "501-293-0004",
                        "2186" => "757-702-4035","7416" => "252-672-2390","36090" => "484-806-1944","38482" => "587-747-4436","16593" => "317-743-1168","43599" => "330-294-0789","50013" => "440-732-3548","15012" => "587-209-4600","25527" => "717-661-1279","49517" => "850-812-4048","47655" => "704-769-1425","11973" => "810-309-8782","1669" => "518-245-6689","23941" => "518-223-9571","1407" => "848-217-3794","38463" => "407-792-6444","1199" => "919-991-5488","45023" => "424-358-4709","49098" => "424-383-8972","19236" => "424-358-4710","41371" => "424-383-9066","36485" => "440-481-1330","1354" => "605-593-5714","39508" => "715-609-2438","2274" => "915-995-7067","6751" => "734-345-1647","36021" => "516-268-4807","577" => "270-267-4566","2189" => "712-214-8562","2178" => "682-267-9553","1659" => "734-480-8874","24709" => "516-246-6717","39985" => "952-209-3611","41969" => "952-209-3612","48908" => "801-905-8603","18079" => "334-409-1130","17372" => "541-780-0320","7879" => "315-605-7079","35916" => "865-590-6007","13491" => "815-205-6455","43078" => "224-484-7184","1815" => "619-750-0487","37167" => "620-440-6317","31283" => "208-647-5088","22886" => "419-419-1816","42189" => "239-264-3178","19088" => "850-733-6170","36013" => "850-696-0856","1757" => "251-200-4022","44831" => "856-861-8005","47773" => "856-861-8006","47774" => "484-484-9089","47776" => "908-223-5188","47777" => "732-515-4135","47778" => "484-496-6151","47780" => "856-528-8127","47781" => "856-292-5672","47783" => "201-299-6936","47785" => "610-285-1355","47787" => "302-608-0894","47788" => "610-400-8652","47789" => "856-270-7355","740" => "856-861-8007","36332" => "856-861-8010","47510" => "302-561-9430","7577" => "856-861-8008","43789" => "856-861-8009","47786" => "215-809-1415","2463" => "518-689-4013","31200" => "208-423-8991","682" => "833-559-4176","2384" => "630-427-4308","1214" => "833-558-9408","1220" => "337-366-6359","2212" => "254-304-9204","27651" => "503-446-6314","11951" => "865-448-4700","11337" => "860-780-0004","686" => "814-338-9387","35986" => "573-377-6062","40899" => "501-214-9114","36475" => "628-800-7480","36319" => "765-319-8740","42908" => "970-623-7493","23757" => "763-296-4649","48259" => "203-800-9621","42208" => "843-951-9475","1996" => "360-610-4026","1997" => "253-650-0823","1998" => "253-993-4925","1999" => "425-620-3883","2000" => "360-812-6487","2001" => "360-485-0662","2002" => "360-536-4112","2003" => "425-409-2541","2004" => "425-336-7491","2005" => "206-426-3353","49369" => "360-553-0154","2006" => "509-530-1994","2007" => "509-822-2316","2008" => "208-758-8027","41218" => "425-620-3885","41219" => "360-312-7516","41220" => "206-426-3568","2023" => "208-996-1628","49368" => "509-581-5132","47524" => "208-996-1631","47527" => "208-618-2966","24353" => "509-902-6177","44117" => "616-251-1152","742" => "630-423-5059","11333" => "630-338-0935","11334" => "331-244-0387","39081" => "928-707-8881","16787" => "210-742-2706","3952" => "334-744-5002","41260" => "470-394-2726","44490" => "470-839-8891","35948" => "979-417-2883","9221" => "940-310-3500","36255" => "940-310-3600","747" => "918-438-7253","4337" => "602-581-7883","2175" => "704-703-1266","1090" => "361-248-1381","43660" => "704-833-8673","36166" => "828-397-9001","10932" => "608-844-2508","31336" => "702-659-9649","4953" => "424-300-0016","40315" => "909-341-6195","9872" => "470-412-5405","1427" => "239-260-4583","40900" => "239-264-3176","470" => "740-853-4189","19788" => "610-492-6233","939" => "425-354-4163","1064" => "256-517-9282","14990" => "778-736-0210","36305" => "651-724-9057","628" => "936-283-4281","37138" => "413-282-7837","6545" => "413-282-7875","48683" => "919-351-5611","994" => "320-316-1523","40999" => "502-251-1910","11615" => "336-447-3425","6306" => "316-252-3142","2478" => "551-277-0926","48400" => "484-452-2147","18562" => "740-200-8502","1680" => "320-208-5697","7000" => "320-208-5696","1048" => "717-895-9429","660" => "704-495-6751","36275" => "828-536-2022","26733" => "587-418-4625","48846" => "714-617-1898","1091" => "323-797-8273","45299" => "714-617-2059","23222" => "952-213-4918","41179" => "952-209-3607","39204" => "239-264-3062","39205" => "239-349-6892","39206" => "941-876-8196","36490" => "218-520-0478","14886" => "605-569-5099","40314" => "424-300-0017","48426" => "916-538-7287","35980" => "864-586-4968","47717" => "406-897-7136","19091" => "201-949-8869","19092" => "551-800-3022","36625" => "830-214-7074","754" => "267-574-9002","43611" => "618-881-1055","360" => "330-509-7604","9583" => "360-740-2008","878" => "651-273-2444","883" => "618-594-1164","36054" => "270-558-0295","7193" => "406-319-6087","40059" => "587-747-4442","6252" => "812-260-1182","43493" => "812-260-1181","40924" => "609-277-2986","1005" => "717-454-3283","50090" => "406-404-7239","14860" => "620-207-8176","47536" => "620-207-8172","207" => "430-558-6944","47810" => "430-558-6945","960" => "734-407-7316","11593" => "207-709-0013","40953" => "304-699-4375","48015" => "740-371-3017","49745" => "304-927-7023","536" => "636-331-7062","426" => "952-209-3614","42824" => "763-296-4654","1866" => "804-601-4535","459" => "608-466-2605","2184" => "850-812-4047","36491" => "319-449-0721","22708" => "318-666-9531","11669" => "920-570-7885","2090" => "505-207-6178","4096" => "813-497-7705","36970" => "813-497-7704","2150" => "941-666-7634","36045" => "859-374-2787","48106" => "859-374-2786","48431" => "859-374-2788","23112" => "330-520-2371","49972" => "218-270-4987","38525" => "704-749-8060","14799" => "530-230-3879","36040" => "724-203-9419","30185" => "724-338-8454","47802" => "248-574-9130","17166" => "304-397-0934","794" => "850-812-4049","2206" => "541-640-5593","44761" => "218-396-3534","39871" => "508-815-3552","35928" => "810-294-4292","1044" => "717-947-3618","41353" => "509-678-1316","41100" => "604-425-0992","2229" => "937-709-0352","47676" => "587-747-2790","50069" => "443-420-6069","37356" => "443-420-6070","11454" => "859-800-3953","5875" => "712-340-0344","26452" => "252-260-2880","884" => "252-621-5145","36435" => "252-260-2886","42873" => "252-260-2874","40005" => "909-415-4162","8297" => "971-762-4619","18470" => "503-388-5862","1936" => "385-448-4269","41211" => "719-374-9208","50097" => "308-244-4121","40030" => "616-369-1977","801" => "205-728-1184","24625" => "205-768-1003","5618" => "813-820-1552","42971" => "518-622-4157","28204" => "503-383-9959","5029" => "951-465-8443","37649" => "318-460-1659","582" => "513-334-2203","2339" => "574-306-5462","863" => "586-251-0898","4628" => "760-332-1476","23777" => "760-925-3431","37056" => "760-332-1477","974" => "316-252-3199","43987" => "316-252-3180","40724" => "757-930-5144","972" => "570-689-1171","42238" => "440-472-2481","42118" => "440-597-1560","44357" => "475-333-0082","41511" => "541-780-0323","37101" => "570-290-7530","20113" => "541-780-0321","31627" => "541-780-0322","36023" => "469-612-6379","26649" => "814-299-5403","7450" => "402-205-7199","43745" => "260-572-9131","15351" => "260-233-2263","18353" => "260-572-9132","38596" => "401-561-2005","20097" => "502-890-1461","44800" => "203-871-1215","1495" => "615-502-1444","49814" => "541-229-2196","38523" => "260-888-2269","38522" => "260-376-0880","7066" => "636-249-2631","8366" => "971-412-4783","36528" => "937-529-8261","2336" => "573-303-5600","9001" => "325-221-3244","36809" => "417-680-0137","983" => "513-854-5565","49201" => "513-854-5566","39978" => "604-373-6608","40320" => "251-288-5624","31146" => "732-523-5435","35977" => "717-912-0020","22940" => "330-451-6947","6327" => "502-890-1419","1332" => "570-761-6265","24311" => "989-279-0165","7440" => "701-639-2638","8181" => "567-333-9490","35896" => "385-300-0139","50053" => "951-816-6305","44340" => "951-245-6159","1128" => "712-796-1088","19183" => "402-817-1445","45019" => "318-666-9530","30476" => "971-915-7801","23168" => "470-400-3536","23169" => "470-460-5240","47673" => "256-517-9284","49251" => "256-517-9283","24375" => "260-297-0231","40795" => "204-515-8868","20043" => "204-515-8871","18608" => "815-669-4836","410" => "816-400-4937","613" => "317-813-9771","614" => "317-827-0129","20095" => "859-800-3954","43507" => "502-735-0364","35888" => "708-572-8392","28542" => "509-530-1992","1001" => "346-277-0096","39326" => "910-335-8122","43800" => "919-552-3384","1502" => "413-831-8318","8642" => "484-806-1943","43045" => "419-956-4681","27692" => "587-747-4417","30263" => "647-360-2730","16611" => "463-212-5083","16798" => "706-524-8507","28454" => "334-591-1615","20162" => "325-777-1393","617" => "252-596-0695","11397" => "239-349-6761","36912" => "239-349-6763","36473" => "712-717-7527","957" => "262-228-8370","811" => "913-318-6418","951" => "814-961-2849","917" => "954-866-2307",
                        "35907" => "620-275-6590","36402" => "424-500-0015","28920" => "403-907-0735","35911" => "920-600-0016","13102" => "714-930-7775","13267" => "951-460-0156","13268" => "949-667-7294","13103" => "909-670-2587","13266" => "714-519-6852","13269" => "714-922-5135","13271" => "909-667-0852","13273" => "949-667-7293","22626" => "714-617-9149","43607" => "949-667-7295","43634" => "714-695-5952","43635" => "949-667-7296","4998" => "619-272-4096","35914" => "802-391-8731","1388" => "502-702-7614","41499" => "864-900-2213","661" => "864-900-2211","15406" => "803-973-3593","24432" => "925-403-1943","981" => "661-418-2875","25508" => "626-406-1759","11471" => "231-769-0722","739" => "724-835-2092","36331" => "724-835-2093","6804" => "269-218-3562","6239" => "574-564-5107","2276" => "231-769-0756","48538" => "740-647-4957","35990" => "408-755-3621","346" => "818-651-7649","727" => "484-913-9003","529" => "828-348-8055","22964" => "828-435-2212","510" => "801-905-8602","474" => "323-591-2674","35926" => "561-234-4589","23901" => "252-672-2389","48702" => "435-228-5334","47784" => "813-497-7701","47798" => "813-497-7702","5381" => "407-316-2519","36603" => "904-431-7737","845" => "810-354-7643","44766" => "289-432-1098","24070" => "318-513-3144","5605" => "786-668-5855","35894" => "423-377-0439","13504" => "323-794-7730","42057" => "704-255-4365","6550" => "978-627-7588","2126" => "440-296-3830","36008" => "440-494-6032","1088" => "804-699-1572","11558" => "346-275-4502","11559" => "346-251-7685","24700" => "346-298-7984","44692" => "236-700-4765","20107" => "517-317-8795","48417" => "505-663-6945","50096" => "912-387-4215","449" => "515-528-7821","50011" => "318-666-9523","25195" => "219-225-6936","11077" => "812-329-2269","1704" => "508-281-4637","40193" => "413-707-0339","43825" => "413-707-0338","9614" => "509-596-2259","47768" => "208-725-3928","18327" => "501-305-0771","27797" => "870-306-2204","18605" => "562-203-8060","22983" => "717-790-1458","22984" => "717-254-4785","39383" => "717-775-6259","5024" => "909-570-2938","43472" => "949-667-7299","29550" => "435-292-8058","11129" => "714-592-1990","48799" => "763-296-4650","2292" => "763-296-4651","13407" => "231-259-5456","41500" => "760-346-3207","15056" => "587-801-2766","6851" => "586-580-0612","48133" => "631-256-4804","19065" => "317-296-8152","24354" => "317-742-0163","46493" => "587-206-2388","10862" => "208-996-1625","1529" => "801-938-5033","6338" => "859-374-2789","18723" => "781-530-4916","22586" => "919-694-1195","6765" => "734-257-8057","461" => "602-581-7891","6841" => "906-262-0092","35961" => "717-970-7326","40146" => "323-795-4114","39939" => "352-431-2106","37754" => "385-389-3389","37638" => "613-703-2414","1100" => "860-348-5297","7599" => "856-390-4094","23038" => "571-371-1786","2311" => "815-219-7366","13350" => "920-212-2047","2182" => "386-492-0207","371" => "860-969-2266","36445" => "906-205-5121","48282" => "405-338-7731","7464" => "833-560-0162","37028" => "618-230-4597","16813" => "925-359-7838","16814" => "925-412-3019","26050" => "209-454-1676","41156" => "843-800-4572","50048" => "262-800-0071","2512" => "262-800-0072","36056" => "570-225-9354","800" => "601-385-8258","1284" => "336-234-1282","41957" => "513-445-4319","42849" => "937-280-5468","429" => "513-854-5568","430" => "513-712-4884","431" => "859-818-0061","432" => "513-854-5567","433" => "513-854-5571","434" => "513-854-5570","435" => "937-900-0680","436" => "937-476-7629","437" => "513-854-5569","38344" => "325-400-5063","38286" => "325-400-5287","5341" => "904-431-7736","22584" => "602-581-7886","1522" => "603-636-7368","6754" => "313-757-4880","29431" => "947-222-1963","29432" => "734-335-4942","39189" => "248-847-0421","2387" => "615-815-1814","45" => "812-308-9407","22888" => "405-378-4931","31164" => "812-758-8059","396" => "270-267-4567","48936" => "508-388-5065","38782" => "253-650-0826","17081" => "661-491-2652","38432" => "805-696-2510","10811" => "714-587-3958","1569" => "833-562-7874","27234" => "289-271-7901","1535" => "863-250-1567","576" => "913-845-5658","24954" => "478-273-3142","37209" => "478-304-3392","36423" => "605-273-7908","7419" => "828-800-8051","714" => "314-944-5911","3968" => "636-200-8202","2309" => "317-922-2894","37817" => "317-922-2907","2388" => "417-501-3762","23799" => "931-651-6161","43496" => "613-702-2301","1034" => "508-556-1947","48436" => "603-273-9104","1398" => "918-438-7249","1401" => "918-438-7252","22713" => "918-558-0723","22714" => "918-537-1141","22716" => "316-252-3212","44329" => "918-801-4043","48349" => "405-338-7032","1400" => "405-767-0097","49232" => "580-289-3672","49235" => "405-561-3255","49236" => "405-888-8426","49237" => "405-724-2790","49789" => "918-438-7255","48350" => "918-438-7256","22706" => "571-371-1785","36032" => "571-396-0228","768" => "650-410-3294","31408" => "620-207-8175","484" => "405-724-2791","14929" => "514-400-6817","22861" => "450-328-3150","22862" => "514-400-6818","8879" => "931-450-3298","30064" => "623-246-7331","24313" => "470-407-7158","42776" => "470-407-7159","35969" => "518-689-4347","1146" => "641-569-8656","36018" => "920-389-8221","1167" => "732-655-9165","44633" => "724-221-7671","44634" => "412-517-6641","778" => "724-203-9416","779" => "412-212-7588","780" => "412-701-6279","781" => "724-387-9345","22603" => "412-218-1160","22604" => "412-748-2368","24428" => "724-888-5946","36358" => "412-701-6249","39376" => "724-558-5300","35992" => "631-270-6591","11285" => "605-291-5686","41210" => "605-250-6665","42868" => "605-956-9827","27541" => "910-725-4165","35953" => "803-845-2102","25325" => "716-705-5300","1433" => "475-882-2045","533" => "256-208-3014","9800" => "307-417-6483","6815" => "517-312-0339","9475" => "540-569-9639","39771" => "970-599-7007","44621" => "605-653-5329","5850" => "712-717-7526","42117" => "815-345-2157","8796" => "615-933-4563","43654" => "217-703-7040","36464" => "734-390-9616","13418" => "208-996-1627","93" => "208-960-7136","94" => "208-996-1613","40533" => "208-960-7137","39996" => "828-634-7917","36828" => "571-424-1691","48950" => "978-226-4771","47757" => "330-845-5146","48172" => "330-441-5256","38301" => "925-420-4619","48389" => "925-420-4618","37451" => "727-513-8891","36851" => "615-283-9358","24315" => "972-236-5289","15512" => "231-930-5210","11855" => "515-528-7823","19613" => "833-557-3767","30687" => "402-819-6897","43870" => "469-598-0451","44127" => "763-296-4653","31509" => "507-200-2690","4985" => "626-412-1125","442" => "951-484-0035","4733" => "714-860-7049","11690" => "434-548-0794","23060" => "443-495-6030","23061" => "443-557-3126","23062" => "410-650-8611","23064" => "410-267-3839","23065" => "410-807-3662","38187" => "410-907-3290","48288" => "217-619-8180","24978" => "618-422-8008","24691" => "618-684-7053","1833" => "304-769-9355","44091" => "541-237-0757","8342" => "541-200-0294","43485" => "602-581-7890","5110" => "707-296-1922","37081" => "717-430-0871","50074" => "619-416-3575","36022" => "619-416-3574","42119" => "816-400-4934","48157" => "816-287-3363","14844" => "231-668-6149","27020" => "360-610-4026","11858" => "208-758-8026","3983" => "765-374-6421","29636" => "587-747-4407","15205" => "778-726-0448","16686" => "778-765-0023","37558" => "587-200-1164","40056" => "236-599-8008","50051" => "236-361-0892","15244" => "1-888-258-8041","15247" => "587-771-0850","15248" => "587-801-2765","15250" => "587-749-0258","15469" => "236-836-2489","15538" => "587-713-0660","14939" => "1-888-257-4764","15239" => "1-888-258-1064","15233" => "587-601-0731","15245" => "236-700-4765","31501" => "1-888-259-2998","50023" => "306-801-0716","2428" => "660-220-2143","44631" => "559-336-8195","49327" => "609-545-2788","9902" => "408-755-3214","2280" => "256-517-9285","29533" => "267-589-8846","31471" => "267-589-8845","19782" => "604-409-0419","48134" => "315-800-5335","14954" => "613-702-2300","23869" => "812-645-2111","39055" => "419-419-1814","38637" => "912-205-2590","37499" => "812-308-9406","50012" => "470-394-2728","43808" => "470-275-1039","36976" => "925-412-3021","19836" => "206-426-3123","2386" => "937-550-3796","35972" => "701-543-8252","48910" => "609-981-8084","36038" => "302-467-3575","5335" => "386-387-2142","9496" => "757-901-0355","23116" => "702-659-6318","18007" => "780-851-8999","11112" => "567-239-4825","2477" => "631-206-5511","538" => "207-387-1450","23833" => "503-446-3478","38746" => "971-762-4618","39139" => "971-762-4620","43964" => "360-553-1916","2082" => "778-735-0850","26055" => "251-288-5744","50094" => "719-374-9202","30689" => "719-374-9203","23780" => "913-353-4890","13355" => "716-304-0573","48022" => "716-371-1054","16797" => "226-214-1625","47707" => "469-210-8356","47708" => "973-629-0215","47709" => "469-409-3188","47706" => "469-210-8374","6591" => "833-560-0258","36621" => "352-619-1533","36619" => "570-261-8053","6860" => "989-317-2247","11979" => "947-207-0098","1721" => "248-963-0041","29861" => "947-207-0127","23086" => "301-761-4553","23087" => "301-778-0467","48104" => "240-200-4339","35973" => "240-623-9499","760" => "715-609-2431","395" => "878-208-4684","11908" => "501-293-0001","1069" => "240-549-5377","27120" => "970-305-4161","48916" => "434-333-0148","1619" => "406-315-4254","1662" => "406-404-7262","26515" => "406-792-7146","1620" => "406-404-7263","36036" => "406-272-1116","1618" => "406-510-6555","39876" => "406-510-6600","40046" => "406-404-7265","23626" => "336-497-3726","31442" => "336-234-1285","49873" => "778-860-5002","1940" => "435-233-7582","50029" => "435-429-6622","49321" => "720-502-8774","1926" => "484-401-7486","47528" => "307-240-9861","23058" => "352-900-3227","47662" => "323-591-2673","35891" => "570-409-7907","9755" => "608-409-3355","35913" => "508-281-9418","11030" => "240-549-5375","39779" => "920-212-2080","39934" => "919-848-6850","44784" => "541-238-7554","40098" => "859-999-7092","41101" => "509-423-7104","50030" => "541-229-2197","40077" => "937-900-0672","36017" => "386-492-0208","38956" => "306-803-5957","41970" => "306-803-5963","37550" => "587-844-2686","965" => "504-224-7503","26005" => "561-266-4198","22935" => "786-544-2670","2466" => "585-488-0471","31525" => "423-343-5617","22883" => "440-472-2396","22884" => "440-588-8103","2484" => "440-671-0574","1558" => "602-581-7892","47932" => "856-244-8334","35925" => "470-412-5419","896" => "833-452-2797","41042" => "870-306-2196","2397" => "509-822-2317","36840" => "717-351-7404","29727" => "317-943-7390","36739" => "443-457-2004","1947" => "470-655-1924","4080" => "385-279-4906","2344" => "607-218-4852","36034" => "309-717-0017","22887" => "778-726-0435","39428" => "778-726-0439","49854" => "409-600-8292","41110" => "907-302-5879","41000" => "218-396-3535","11062" => "319-449-0723","36009" => "319-519-4791","36974" => "209-300-0306","41433" => "760-670-3099","22783" => "424-310-0655","49401" => "407-278-6635","49402" => "833-553-8226","24387" => "240-850-3248","1292" => "803-973-3591","910" => "914-704-3085","8029" => "419-540-0104","26429" => "442-232-3097","658" => "267-748-0926","30360" => "314-782-5754","47680" => "587-747-4410","23149" => "408-755-3215","44762" => "715-502-2577","40060" => "816-368-1881","39438" => "417-288-4959","383" => "856-301-2780","36101" => "856-301-2858","1642" => "806-502-6062","36248" => "507-237-6343","26512" => "760-301-5039","23079" => "248-920-8117","645" => "913-318-6414","36876" => "336-405-8163","1175" => "319-427-2725","6490" => "318-666-9527","37783" => "318-666-9526","22610" => "616-371-4746","23826" => "323-880-4957","45422" => "323-880-4956","24368" => "205-768-1004","19826" => "346-299-1877",
                        "38130" => "346-323-7207","47635" => "346-273-0521","607" => "346-299-1882","609" => "346-358-3199","19827" => "346-279-0921","19829" => "936-283-4277","19830" => "346-299-1878","42185" => "346-299-1885","47953" => "346-299-1883","8927" => "903-487-4395","41964" => "559-785-9763","39012" => "559-413-0089","36043" => "616-747-7270","45248" => "469-745-8295","23053" => "859-374-2791","43751" => "859-374-2790","22805" => "567-510-0382","37747" => "331-800-0224","2383" => "541-649-4321","22606" => "657-400-4200","1101" => "586-229-1310","6055" => "309-279-5671","47659" => "863-250-1569","35930" => "863-250-1570","2258" => "417-202-0031","1644" => "603-369-4331","41724" => "240-549-5376","949" => "267-890-7669","43656" => "515-528-7826","41202" => "515-528-7825","48413" => "816-988-4198","774" => "707-710-9775","19776" => "707-309-0077","40678" => "585-673-7080","10875" => "781-355-6936","27595" => "781-355-6355","2288" => "517-619-0581","50104" => "681-484-0040","39118" => "916-975-2516","39119" => "279-333-1392","39120" => "408-755-3216","38840" => "916-573-1380","2199" => "828-383-8001","43498" => "580-303-5033","2317" => "681-484-0041","31464" => "540-691-2103","42101" => "469-210-8357","36069" => "269-628-8172","49569" => "269-359-7985","36531" => "269-377-3251","6390" => "502-890-1443","11344" => "502-890-1388","27340" => "778-728-0378","557" => "859-681-0382","14" => "661-491-2648","15072" => "226-314-1721","29940" => "226-314-1722","11346" => "909-515-3189","35975" => "760-289-5972","767" => "262-709-2240","996" => "570-266-7861","15016" => "705-300-9427","43989" => "206-426-3741","35877" => "651-447-5198","2279" => "619-416-3573","29913" => "260-302-1293","25214" => "833-559-2833","19104" => "346-299-1880","16687" => "713-535-9831","36933" => "574-307-5269","38136" => "540-642-0313","2203" => "317-680-2101","745" => "941-315-4714","664" => "708-589-7034","37375" => "207-707-1331","6718" => "207-707-1347","41415" => "854-854-5543","15033" => "780-851-8990","409" => "916-562-2895","19191" => "209-684-0071","49461" => "517-513-6750","17425" => "319-277-0937","5849" => "712-717-7528","36000" => "803-615-4605","49349" => "585-432-0573","50077" => "236-361-0894","29631" => "402-819-6881","544" => "765-987-4549","35985" => "470-431-0514","8074" => "330-623-6450","1298" => "318-666-9528","43648" => "318-666-9529","43495" => "251-281-8975","19122" => "814-616-3298","8270" => "918-438-7250","18724" => "919-897-5063","47544" => "601-514-0984","36871" => "601-255-2196","7345" => "252-549-4090","9868" => "260-535-0918","24595" => "248-372-9549","39780" => "623-246-7327","919" => "267-715-2298","14992" => "289-351-1926","49041" => "218-405-7578","14975" => "587-849-4055","822" => "346-273-0522","22959" => "346-298-7990","565" => "270-267-4568","16610" => "707-710-9774","47529" => "217-441-6530","29889" => "909-515-3190","35889" => "616-369-1982","38749" => "541-640-5594","38748" => "509-790-6018","49914" => "509-902-6179","24374" => "509-902-6176","39853" => "509-581-5134","42044" => "509-423-7102","48188" => "509-902-6178","38745" => "509-902-6180","38766" => "509-940-2129","39768" => "509-790-6019","41019" => "828-800-0019","877" => "704-380-9024","36428" => "704-380-9106","43970" => "785-670-6169","48226" => "714-860-7047","36463" => "714-860-7046","36" => "703-936-4154","498" => "631-343-9081","30519" => "225-831-6729","1601" => "423-793-7206","6465" => "337-717-1128","6471" => "337-485-4117","6501" => "337-607-0546","6526" => "337-348-0656","13454" => "337-607-0547","30115" => "337-999-0021","47807" => "225-480-3558","6386" => "502-890-1387","22924" => "417-248-1703","23856" => "417-213-8123","36558" => "417-374-2304","18394" => "870-444-6813","36048" => "573-410-1279","47797" => "615-716-9865","4044" => "510-250-8746","49240" => "253-650-0822","757" => "936-283-4278","5290" => "203-800-9575","25360" => "207-492-8011","36478" => "833-551-0608","49897" => "785-502-8673","39775" => "778-728-0390","4119" => "907-302-5878","31616" => "253-656-4253","47769" => "208-994-3451","31615" => "425-336-7493","1285" => "256-445-6963","24312" => "717-977-3890","2371" => "208-826-0036","11447" => "833-638-2507","943" => "860-783-1744","37613" => "714-646-7277","39180" => "386-492-0206","1282" => "574-564-5106","161" => "262-419-3489","37552" => "217-707-6133","600" => "309-291-3480","19754" => "604-373-0008","843" => "630-359-8031","244" => "919-882-3242","36024" => "919-694-1182","245" => "336-405-8006","592" => "910-900-5437","37131" => "940-386-6106","50035" => "217-441-6532","23013" => "217-441-6533","50009" => "970-623-7494","37058" => "210-742-6930","44796" => "906-212-6461","19760" => "833-452-2803","24431" => "505-207-6175","2048" => "732-663-9604","15058" => "709-800-7960","1783" => "812-271-0831","1805" => "801-938-5860","38255" => "808-913-1533","30693" => "269-767-8660","28047" => "559-358-2148","8867" => "615-326-4980","35892" => "417-501-3765","1786" => "512-537-0582","4501" => "818-741-3495","978" => "256-980-3380","9033" => "936-649-4027","14959" => "226-214-1621","14996" => "780-851-8992","506" => "571-297-1965","36172" => "571-424-1719","50052" => "940-315-7900","1061" => "207-610-6753","22982" => "704-703-1269","25458" => "204-515-8897","37224" => "570-801-6506","40195" => "916-572-8089","50099" => "334-310-9917","8303" => "503-388-5859","16714" => "352-354-3037","42130" => "352-900-3226","11588" => "443-292-9247","1995" => "407-848-1316","30084" => "386-205-6951","43600" => "386-205-6952","43601" => "407-316-2491","47661" => "609-380-3376","48446" => "303-214-0370","44343" => "406-404-7284","44345" => "406-389-0140","18612" => "978-705-4737","48552" => "407-426-4056","10912" => "810-355-4540","38681" => "419-763-6394","1009" => "919-924-0251","6992" => "320-321-6081","45426" => "424-300-0015","1695" => "470-900-0048","45352" => "854-854-5544","11500" => "256-980-3381","11548" => "541-233-5797","23623" => "289-348-0286","1324" => "971-915-7803","1046" => "605-368-3389","2141" => "337-388-2421","6236" => "463-212-5079","24471" => "587-805-1781","37145" => "780-851-9040","48860" => "706-960-1019","1397" => "479-502-9174","36700" => "570-280-2719","22981" => "949-667-7297","4676" => "949-396-1786","22980" => "949-667-7298","361" => "330-539-2137","445" => "909-660-7876","1422" => "708-981-1593","50054" => "701-543-8255","49487" => "847-696-6843","24409" => "847-696-6843","31154" => "508-263-0417","17196" => "780-851-8986","5820" => "229-258-8869","14988" => "604-425-0937","27337" => "650-250-1588","308" => "530-338-1564","50102" => "434-218-5579","36004" => "512-591-0740","327" => "530-230-3589","47856" => "708-858-4300","7217" => "910-400-1004","39372" => "309-429-6819","36067" => "630-283-1965","38834" => "973-574-3572","35981" => "760-330-5798","39229" => "760-670-3485","2513" => "262-455-9342","1487" => "574-971-2125","48361" => "208-996-1618","43975" => "919-741-6237","11510" => "417-658-1057","49340" => "410-927-3560","1204" => "509-530-1990","48213" => "509-530-1991","13105" => "951-460-0161","13276" => "714-587-2993","23977" => "714-860-7050","49515" => "623-246-7328","49870" => "408-755-3217","975" => "828-435-2215","47801" => "262-800-0075","1057" => "989-402-0991","8655" => "401-543-2450","35983" => "618-800-6283","47534" => "219-246-2757","764" => "916-883-2634","48441" => "780-900-5532","2511" => "262-269-1243","23996" => "585-673-7081","41495" => "585-673-7082","6849" => "586-519-9601","1011" => "219-706-2844","728" => "870-306-2201","14804" => "616-217-9135","897" => "763-296-4648","46443" => "585-371-5920","48421" => "352-340-2661","47943" => "206-426-3920","36125" => "309-650-6639","14934" => "782-827-5256","43512" => "506-799-3653","39366" => "782-827-5254","47760" => "506-799-3654","885" => "941-666-7633","11074" => "608-620-7726","39296" => "385-279-4908","18987" => "208-656-1378","18988" => "385-298-1553","35995" => "208-417-3257","8446" => "717-482-0271","9661" => "509-590-1523","13048" => "626-225-0875","47902" => "863-401-4385","30169" => "419-504-0173","40029" => "908-736-4281","1171" => "704-452-9968","136" => "563-265-3423","135" => "973-314-1003","35959" => "973-314-1004","41375" => "239-264-3063","26234" => "239-264-3177","39731" => "808-913-1534","39732" => "808-913-1531","1464" => "561-295-1362","18477" => "772-521-9401","1736" => "772-521-9403","19814" => "772-521-9402","49871" => "561-295-1363","48105" => "772-521-9404","44807" => "910-900-5439","18666" => "816-400-4938","18667" => "816-400-4935","35974" => "913-318-6415","50066" => "816-775-4103","1591" => "701-765-8114","11940" => "478-273-3145","36401" => "804-729-5177","2359" => "760-621-9376","2360" => "951-428-3906","43663" => "858-284-4962","44247" => "949-503-0547","48200" => "949-503-0549","29667" => "231-742-8306","23706" => "616-369-1988","2298" => "385-497-6146","862" => "425-409-2744","42851" => "602-581-7887","13500" => "602-581-7889","720" => "978-320-3364","11140" => "734-245-8648","37283" => "704-489-3262","44736" => "587-747-4422","17424" => "831-272-0967","36003" => "803-597-2752","22589" => "831-216-6732","9871" => "307-224-2872","1879" => "385-233-0630","1486" => "330-249-7273","13369" => "757-901-0354","43848" => "701-543-8256","23927" => "843-823-5818","1305" => "815-277-1312","45421" => "248-716-8764","37632" => "334-310-9919","36055" => "916-562-2577","19131" => "780-851-8987","26260" => "813-820-1556","43711" => "941-666-7631","43712" => "813-497-7709","48621" => "989-625-3101","7633" => "732-515-4162","13411" => "609-362-5225","39793" => "973-567-3588","2415" => "267-458-2210","28253" => "920-545-1276","45269" => "920-945-5255","9734" => "262-269-1241","464" => "219-232-9066","11250" => "806-503-4925","44626" => "432-219-6433","48001" => "806-502-6061","19926" => "952-209-3606","48000" => "412-748-2362","49484" => "412-218-0496","1174" => "520-999-8030","13047" => "608-851-2419","45017" => "330-623-6450","47948" => "979-977-3208");


			
				foreach($retailers as $key => $value){               
       
			 if($value!=""){
                  
                    $skus = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_id_api' AND meta_value=".$key);
                    //var_dump($skus);
                    if(count($skus) > 0){

                        $post_id = $skus[0]->post_id;

                        $phone_key = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id=".$post_id);
                        if(count($phone_key) > 0){
                            $update = "UPDATE {$wpdb->postmeta} set meta_value='".$value."'   WHERE  meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id = '".$post_id."'";
                            $wpdb->query( $update );
						   echo $update;
                            $phoneupdated++;
                        }else{

                         $insert =  "insert into {$wpdb->postmeta} (post_id,meta_key,meta_value) values (".$post_id.",'wpsl_retailer_forwardingphnumber','".$value."') ";    
							//   var_dump( $insert);
                           $wpdb->query( $insert );
						 echo $post_id ;
						 //echo $insert;
                           $phoneupdated++;
                        }

                        

                    }
                    else{
                        var_dump("No recored found ".$key);    
                        $noretainlerfound++;
                        
                    }
                }
                 else{
                   // var_dump("No forwardign number");
                    $nophonenumbers++;
                } 
			
			
           
        }exit;
		  }



function insertMissingPages(){
    global $wpdb;
$input = '[{
        "locationId": 104219,
        "retailerId": 22558,
        "locationCode": "49188",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "CAROLINA WHOLESALE FLOORS",
        "address1": "95 Sunbelt Blvd D",
        "address2": null,
        "city": "Columbia",
        "state": "SC",
        "country": "US",
        "postalCode": "29203",
        "phone": "803-786-8055",
        "forwardingPhone": "803-973-3594",
        "email": "acdflooring@gmail.com",
        "licenseNumber": null,
        "url": null,
        "locationLogo": null,
        "adPackage": "$250 Package",
        "about": null,
        "lat": 33.9948198,
        "lng": -81.0544733,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 71604,
                "locationId": 104219,
                "type": "carpet",
                "seo": "sc/columbia/29203/carolina-wholesale-floors/49188/carpet",
                "sem": "sc/columbia/29203/carolina-wholesale-floors/49188/carpet/sale"
            },
            {
                "urlId": 71603,
                "locationId": 104219,
                "type": "coupon",
                "seo": "sc/columbia/29203/carolina-wholesale-floors/49188/flooring-all",
                "sem": "sc/columbia/29203/carolina-wholesale-floors/49188/flooring-all/sale"
            },
            {
                "urlId": 71605,
                "locationId": 104219,
                "type": "waterproof",
                "seo": "sc/columbia/29203/carolina-wholesale-floors/49188/waterproof-coretec",
                "sem": "sc/columbia/29203/carolina-wholesale-floors/49188/waterproof-coretec/sale"
            }
        ],
        "hours": null,
        "products": [
            "coretecVinyl",
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "false",
            "aligned": "false",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 50000,
        "searchBudget": 30000,
        "socialBudget": 20000,
        "coretec": true
    }]';
   /*  $input = '[{
        "locationId": 104666,
        "retailerId": 22942,
        "locationCode": "1996",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "1250 Swan Dr",
        "address2": null,
        "city": "Burlington",
        "state": "WA",
        "country": "US",
        "postalCode": "98233-3325",
        "phone": "360-757-4600",
        "forwardingPhone": "360-610-4026",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 48.4647645,
        "lng": -122.3408574,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37034,
                "locationId": 104666,
                "type": "carpet",
                "seo": "wa/burlington/98233/great-floors/1996/carpet",
                "sem": "wa/burlington/98233/great-floors/1996/carpet/sale"
            },
            {
                "urlId": 37033,
                "locationId": 104666,
                "type": "coupon",
                "seo": "wa/burlington/98233/great-floors/1996/flooring-all",
                "sem": "wa/burlington/98233/great-floors/1996/flooring-all/sale"
            },
            {
                "urlId": 41556,
                "locationId": 104666,
                "type": "store",
                "seo": "wa/burlington/98233/great-floors/1996/carpet",
                "sem": null
            },
            {
                "urlId": 37035,
                "locationId": 104666,
                "type": "waterproof",
                "seo": "wa/burlington/98233/great-floors/1996/waterproof-coretec",
                "sem": "wa/burlington/98233/great-floors/1996/waterproof-coretec/sale"
            }
        ],
        "hours": null,
        "products": [
            "atCarpet",
            "atHardwood",
            "coretecVinyl",
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": true
    },
    {
        "locationId": 104667,
        "retailerId": 22942,
        "locationCode": "1997",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "6818 Tacoma Mall Blvd",
        "address2": null,
        "city": "Tacoma",
        "state": "WA",
        "country": "US",
        "postalCode": "98409-9030",
        "phone": "253-474-9034",
        "forwardingPhone": "253-650-0823",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.1940661,
        "lng": -122.4639965,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37037,
                "locationId": 104667,
                "type": "carpet",
                "seo": "wa/tacoma/98409/great-floors/1997/carpet",
                "sem": "wa/tacoma/98409/great-floors/1997/carpet/sale"
            },
            {
                "urlId": 37036,
                "locationId": 104667,
                "type": "coupon",
                "seo": "wa/tacoma/98409/great-floors/1997/flooring",
                "sem": "wa/tacoma/98409/great-floors/1997/flooring/sale"
            },
            {
                "urlId": 41561,
                "locationId": 104667,
                "type": "store",
                "seo": "wa/tacoma/98409/great-floors/1997/carpet",
                "sem": null
            },
            {
                "urlId": 37038,
                "locationId": 104667,
                "type": "waterproof",
                "seo": "wa/tacoma/98409/great-floors/1997/waterproof-flooring",
                "sem": "wa/tacoma/98409/great-floors/1997/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104668,
        "retailerId": 22942,
        "locationCode": "1998",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "30820 Pacific Hwy S",
        "address2": null,
        "city": "Federal Way",
        "state": "WA",
        "country": "US",
        "postalCode": "98003-4902",
        "phone": "253-839-2142",
        "forwardingPhone": "253-993-4925",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.32525070000001,
        "lng": -122.3127448,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37040,
                "locationId": 104668,
                "type": "carpet",
                "seo": "wa/federal-way/98003/great-floors/1998/carpet",
                "sem": "wa/federal-way/98003/great-floors/1998/carpet/sale"
            },
            {
                "urlId": 37039,
                "locationId": 104668,
                "type": "coupon",
                "seo": "wa/federal-way/98003/great-floors/1998/flooring",
                "sem": "wa/federal-way/98003/great-floors/1998/flooring/sale"
            },
            {
                "urlId": 41566,
                "locationId": 104668,
                "type": "store",
                "seo": "wa/federal-way/98003/great-floors/1998/carpet",
                "sem": null
            },
            {
                "urlId": 37041,
                "locationId": 104668,
                "type": "waterproof",
                "seo": "wa/federal-way/98003/great-floors/1998/waterproof-flooring",
                "sem": "wa/federal-way/98003/great-floors/1998/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104669,
        "retailerId": 22942,
        "locationCode": "1999",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "12802 Bel Red Rd",
        "address2": null,
        "city": "Bellevue",
        "state": "WA",
        "country": "US",
        "postalCode": "98005-2603",
        "phone": "425-455-8332",
        "forwardingPhone": "425-620-3883",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.6217087,
        "lng": -122.1694578,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37043,
                "locationId": 104669,
                "type": "carpet",
                "seo": "wa/bellevue/98005/great-floors/1999/carpet",
                "sem": "wa/bellevue/98005/great-floors/1999/carpet/sale"
            },
            {
                "urlId": 37042,
                "locationId": 104669,
                "type": "coupon",
                "seo": "wa/bellevue/98005/great-floors/1999/flooring",
                "sem": "wa/bellevue/98005/great-floors/1999/flooring/sale"
            },
            {
                "urlId": 41571,
                "locationId": 104669,
                "type": "store",
                "seo": "wa/bellevue/98005/great-floors/1999/carpet",
                "sem": null
            },
            {
                "urlId": 37044,
                "locationId": 104669,
                "type": "waterproof",
                "seo": "wa/bellevue/98005/great-floors/1999/waterproof-flooring",
                "sem": "wa/bellevue/98005/great-floors/1999/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104670,
        "retailerId": 22942,
        "locationCode": "2000",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "346 W Bakerview Rd",
        "address2": null,
        "city": "Bellingham",
        "state": "WA",
        "country": "US",
        "postalCode": "98226-8105",
        "phone": "360-738-3599",
        "forwardingPhone": "360-812-6487",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 48.7903026,
        "lng": -122.4932212,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37046,
                "locationId": 104670,
                "type": "carpet",
                "seo": "wa/bellingham/98226/great-floors/2000/carpet",
                "sem": "wa/bellingham/98226/great-floors/2000/carpet/sale"
            },
            {
                "urlId": 37045,
                "locationId": 104670,
                "type": "coupon",
                "seo": "wa/bellingham/98226/great-floors/2000/flooring",
                "sem": "wa/bellingham/98226/great-floors/2000/flooring/sale"
            },
            {
                "urlId": 41576,
                "locationId": 104670,
                "type": "store",
                "seo": "wa/bellingham/98226/great-floors/2000/carpet",
                "sem": null
            },
            {
                "urlId": 37047,
                "locationId": 104670,
                "type": "waterproof",
                "seo": "wa/bellingham/98226/great-floors/2000/waterproof-flooring",
                "sem": "wa/bellingham/98226/great-floors/2000/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104671,
        "retailerId": 22942,
        "locationCode": "2001",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "7800 Martin Way E",
        "address2": null,
        "city": "Lacey",
        "state": "WA",
        "country": "US",
        "postalCode": "98516-5718",
        "phone": "360-438-3900",
        "forwardingPhone": "360-485-0662",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.0554383,
        "lng": -122.7765076,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37049,
                "locationId": 104671,
                "type": "carpet",
                "seo": "wa/lacey/98516/great-floors/2001/carpet",
                "sem": "wa/lacey/98516/great-floors/2001/carpet/sale"
            },
            {
                "urlId": 37048,
                "locationId": 104671,
                "type": "coupon",
                "seo": "wa/lacey/98516/great-floors/2001/flooring",
                "sem": "wa/lacey/98516/great-floors/2001/flooring/sale"
            },
            {
                "urlId": 41581,
                "locationId": 104671,
                "type": "store",
                "seo": "wa/lacey/98516/great-floors/2001/carpet",
                "sem": null
            },
            {
                "urlId": 37050,
                "locationId": 104671,
                "type": "waterproof",
                "seo": "wa/lacey/98516/great-floors/2001/waterproof-flooring",
                "sem": "wa/lacey/98516/great-floors/2001/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104672,
        "retailerId": 22942,
        "locationCode": "2002",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "3200 NW Randall Way",
        "address2": null,
        "city": "Silverdale",
        "state": "WA",
        "country": "US",
        "postalCode": "98383-7952",
        "phone": "360-692-7732",
        "forwardingPhone": "360-536-4112",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.6593469,
        "lng": -122.6935875,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37052,
                "locationId": 104672,
                "type": "carpet",
                "seo": "wa/silverdale/98383/great-floors/2002/carpet",
                "sem": "wa/silverdale/98383/great-floors/2002/carpet/sale"
            },
            {
                "urlId": 37051,
                "locationId": 104672,
                "type": "coupon",
                "seo": "wa/silverdale/98383/great-floors/2002/flooring",
                "sem": "wa/silverdale/98383/great-floors/2002/flooring/sale"
            },
            {
                "urlId": 41586,
                "locationId": 104672,
                "type": "store",
                "seo": "wa/silverdale/98383/great-floors/2002/carpet",
                "sem": null
            },
            {
                "urlId": 37053,
                "locationId": 104672,
                "type": "waterproof",
                "seo": "wa/silverdale/98383/great-floors/2002/waterproof-flooring",
                "sem": "wa/silverdale/98383/great-floors/2002/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104673,
        "retailerId": 22942,
        "locationCode": "2003",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "5501 196th St SW",
        "address2": null,
        "city": "Lynnwood",
        "state": "WA",
        "country": "US",
        "postalCode": "98036-6149",
        "phone": "425-771-1477",
        "forwardingPhone": "425-409-2541",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.8221596,
        "lng": -122.3075137,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37055,
                "locationId": 104673,
                "type": "carpet",
                "seo": "wa/lynnwood/98036/great-floors/2003/carpet",
                "sem": "wa/lynnwood/98036/great-floors/2003/carpet/sale"
            },
            {
                "urlId": 37054,
                "locationId": 104673,
                "type": "coupon",
                "seo": "wa/lynnwood/98036/great-floors/2003/flooring",
                "sem": "wa/lynnwood/98036/great-floors/2003/flooring/sale"
            },
            {
                "urlId": 41591,
                "locationId": 104673,
                "type": "store",
                "seo": "wa/lynnwood/98036/great-floors/2003/carpet",
                "sem": null
            },
            {
                "urlId": 37056,
                "locationId": 104673,
                "type": "waterproof",
                "seo": "wa/lynnwood/98036/great-floors/2003/waterproof-flooring",
                "sem": "wa/lynnwood/98036/great-floors/2003/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104674,
        "retailerId": 22942,
        "locationCode": "2004",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "9021 S 180th St",
        "address2": null,
        "city": "Kent",
        "state": "WA",
        "country": "US",
        "postalCode": "98032-1091",
        "phone": "425-251-0200",
        "forwardingPhone": "425-336-7491",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.4405284,
        "lng": -122.2187469,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37058,
                "locationId": 104674,
                "type": "carpet",
                "seo": "wa/kent/98032/great-floors/2004/carpet",
                "sem": "wa/kent/98032/great-floors/2004/carpet/sale"
            },
            {
                "urlId": 37057,
                "locationId": 104674,
                "type": "coupon",
                "seo": "wa/kent/98032/great-floors/2004/flooring",
                "sem": "wa/kent/98032/great-floors/2004/flooring/sale"
            },
            {
                "urlId": 41596,
                "locationId": 104674,
                "type": "store",
                "seo": "wa/kent/98032/great-floors/2004/carpet",
                "sem": null
            },
            {
                "urlId": 37059,
                "locationId": 104674,
                "type": "waterproof",
                "seo": "wa/kent/98032/great-floors/2004/waterproof-flooring",
                "sem": "wa/kent/98032/great-floors/2004/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104675,
        "retailerId": 22942,
        "locationCode": "2005",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "1251 1st Ave S",
        "address2": null,
        "city": "Seattle",
        "state": "WA",
        "country": "US",
        "postalCode": "98134-1207",
        "phone": "206-624-7800",
        "forwardingPhone": "206-426-3353",
        "email": "dchadderdon@greatfloors.com",
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.5907703,
        "lng": -122.3347821,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37061,
                "locationId": 104675,
                "type": "carpet",
                "seo": "wa/seattle/98134/great-floors/2005/carpet",
                "sem": "wa/seattle/98134/great-floors/2005/carpet/sale"
            },
            {
                "urlId": 37060,
                "locationId": 104675,
                "type": "coupon",
                "seo": "wa/seattle/98134/great-floors/2005/flooring",
                "sem": "wa/seattle/98134/great-floors/2005/flooring/sale"
            },
            {
                "urlId": 41601,
                "locationId": 104675,
                "type": "store",
                "seo": "wa/seattle/98134/great-floors/2005/carpet",
                "sem": null
            },
            {
                "urlId": 37062,
                "locationId": 104675,
                "type": "waterproof",
                "seo": "wa/seattle/98134/great-floors/2005/waterproof-flooring",
                "sem": "wa/seattle/98134/great-floors/2005/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104676,
        "retailerId": 22943,
        "locationCode": "2006",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "231 E Francis Ave",
        "address2": null,
        "city": "Spokane",
        "state": "WA",
        "country": "US",
        "postalCode": "99208-1035",
        "phone": "509-482-0839",
        "forwardingPhone": "509-530-1994",
        "email": null,
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.7156451,
        "lng": -117.4064988,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37064,
                "locationId": 104676,
                "type": "carpet",
                "seo": "wa/spokane/99208/great-floors/2006/carpet",
                "sem": "wa/spokane/99208/great-floors/2006/carpet/sale"
            },
            {
                "urlId": 37063,
                "locationId": 104676,
                "type": "coupon",
                "seo": "wa/spokane/99208/great-floors/2006/flooring",
                "sem": "wa/spokane/99208/great-floors/2006/flooring/sale"
            },
            {
                "urlId": 41606,
                "locationId": 104676,
                "type": "store",
                "seo": "wa/spokane/99208/great-floors/2006/carpet",
                "sem": null
            },
            {
                "urlId": 37065,
                "locationId": 104676,
                "type": "waterproof",
                "seo": "wa/spokane/99208/great-floors/2006/waterproof-flooring",
                "sem": "wa/spokane/99208/great-floors/2006/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104677,
        "retailerId": 22943,
        "locationCode": "2007",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "13708 E Indiana Ave",
        "address2": null,
        "city": "Spokane Valley",
        "state": "WA",
        "country": "US",
        "postalCode": "99216-1147",
        "phone": "509-535-4603",
        "forwardingPhone": "509-822-2316",
        "email": null,
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.6751723,
        "lng": -117.2203521,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37067,
                "locationId": 104677,
                "type": "carpet",
                "seo": "wa/spokane-valley/99216/great-floors/2007/carpet",
                "sem": "wa/spokane-valley/99216/great-floors/2007/carpet/sale"
            },
            {
                "urlId": 37066,
                "locationId": 104677,
                "type": "coupon",
                "seo": "wa/spokane-valley/99216/great-floors/2007/flooring",
                "sem": "wa/spokane-valley/99216/great-floors/2007/flooring/sale"
            },
            {
                "urlId": 41611,
                "locationId": 104677,
                "type": "store",
                "seo": "wa/spokane-valley/99216/great-floors/2007/carpet",
                "sem": null
            },
            {
                "urlId": 37068,
                "locationId": 104677,
                "type": "waterproof",
                "seo": "wa/spokane-valley/99216/great-floors/2007/waterproof-flooring",
                "sem": "wa/spokane-valley/99216/great-floors/2007/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104678,
        "retailerId": 22943,
        "locationCode": "2008",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "3800 N Government Way",
        "address2": null,
        "city": "Coeur D Alene",
        "state": "ID",
        "country": "US",
        "postalCode": "83815-8300",
        "phone": "208-765-6014",
        "forwardingPhone": "208-758-8027",
        "email": null,
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.7133615,
        "lng": -116.7856141,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37070,
                "locationId": 104678,
                "type": "carpet",
                "seo": "id/coeur-d-alene/83815/great-floors/2008/carpet",
                "sem": "id/coeur-d-alene/83815/great-floors/2008/carpet/sale"
            },
            {
                "urlId": 37069,
                "locationId": 104678,
                "type": "coupon",
                "seo": "id/coeur-d-alene/83815/great-floors/2008/flooring",
                "sem": "id/coeur-d-alene/83815/great-floors/2008/flooring/sale"
            },
            {
                "urlId": 41616,
                "locationId": 104678,
                "type": "store",
                "seo": "id/coeur-d-alene/83815/great-floors/2008/carpet",
                "sem": null
            },
            {
                "urlId": 37071,
                "locationId": 104678,
                "type": "waterproof",
                "seo": "id/coeur-d-alene/83815/great-floors/2008/waterproof-flooring",
                "sem": "id/coeur-d-alene/83815/great-floors/2008/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 104682,
        "retailerId": 22945,
        "locationCode": "2023",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "2855 E Fairview Ave",
        "address2": null,
        "city": "Meridian",
        "state": "ID",
        "country": "US",
        "postalCode": "83642-7347",
        "phone": "208-884-1975",
        "forwardingPhone": "208-996-1628",
        "email": null,
        "licenseNumber": null,
        "url": "http://www.greatfloors.com/",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 43.6189056,
        "lng": -116.360227,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 37082,
                "locationId": 104682,
                "type": "carpet",
                "seo": "id/meridian/83642/great-floors/2023/carpet",
                "sem": "id/meridian/83642/great-floors/2023/carpet/sale"
            },
            {
                "urlId": 37081,
                "locationId": 104682,
                "type": "coupon",
                "seo": "id/meridian/83642/great-floors/2023/flooring",
                "sem": "id/meridian/83642/great-floors/2023/flooring/sale"
            },
            {
                "urlId": 41636,
                "locationId": 104682,
                "type": "store",
                "seo": "id/meridian/83642/great-floors/2023/carpet",
                "sem": null
            },
            {
                "urlId": 37083,
                "locationId": 104682,
                "type": "waterproof",
                "seo": "id/meridian/83642/great-floors/2023/waterproof-flooring",
                "sem": "id/meridian/83642/great-floors/2023/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 105598,
        "retailerId": 22943,
        "locationCode": "41219",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "7800 Martin Way E",
        "address2": null,
        "city": "Lacey",
        "state": "WA",
        "country": "US",
        "postalCode": "98516-5718",
        "phone": "360-438-3900",
        "forwardingPhone": "360-312-7516",
        "email": null,
        "licenseNumber": null,
        "url": "http://www.greatfloors.com",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 47.0554383,
        "lng": -122.7765076,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 47677,
                "locationId": 105598,
                "type": "carpet",
                "seo": "wa/lacey/98516/great-floors/41219/carpet",
                "sem": "wa/lacey/98516/great-floors/41219/carpet/sale"
            },
            {
                "urlId": 47676,
                "locationId": 105598,
                "type": "coupon",
                "seo": "wa/lacey/98516/great-floors/41219/flooring",
                "sem": "wa/lacey/98516/great-floors/41219/flooring/sale"
            },
            {
                "urlId": 57371,
                "locationId": 105598,
                "type": "store",
                "seo": "wa/lacey/98516/great-floors/41219/carpet",
                "sem": null
            },
            {
                "urlId": 47678,
                "locationId": 105598,
                "type": "waterproof",
                "seo": "wa/lacey/98516/great-floors/41219/waterproof-flooring",
                "sem": "wa/lacey/98516/great-floors/41219/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "false",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 105894,
        "retailerId": 22942,
        "locationCode": "49369",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "2200 NE Andresen Rd Ste A",
        "address2": null,
        "city": "Vancouver",
        "state": "WA",
        "country": "US",
        "postalCode": "98661-7350",
        "phone": "360-695-1231",
        "forwardingPhone": "360-553-0154",
        "email": "m.haasl@greatfloors.com",
        "licenseNumber": null,
        "url": "https://www.greatfloors.com/vancouverflooringstore",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 45.6384109,
        "lng": -122.6010489,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 58686,
                "locationId": 105894,
                "type": "carpet",
                "seo": "wa/vancouver/98661/great-floors/49369/carpet",
                "sem": "wa/vancouver/98661/great-floors/49369/carpet/sale"
            },
            {
                "urlId": 58685,
                "locationId": 105894,
                "type": "coupon",
                "seo": "wa/vancouver/98661/great-floors/49369/flooring",
                "sem": "wa/vancouver/98661/great-floors/49369/flooring/sale"
            },
            {
                "urlId": 84327,
                "locationId": 105894,
                "type": "store",
                "seo": "wa/vancouver/98661/great-floors/49369/carpet",
                "sem": null
            },
            {
                "urlId": 58687,
                "locationId": 105894,
                "type": "waterproof",
                "seo": "wa/vancouver/98661/great-floors/49369/waterproof-flooring",
                "sem": "wa/vancouver/98661/great-floors/49369/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "false",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 105893,
        "retailerId": 23317,
        "locationCode": "49368",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS",
        "address1": "7220 W Okanogan Place",
        "address2": null,
        "city": "Kennewick",
        "state": "WA",
        "country": "US",
        "postalCode": "99336",
        "phone": "509-491-5100",
        "forwardingPhone": "509-581-5132",
        "email": "d.palmer@greatfloors.com",
        "licenseNumber": null,
        "url": "https://www.greatfloors.com/kennewickflooringstore",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 46.2206812,
        "lng": -119.2198477,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 58683,
                "locationId": 105893,
                "type": "carpet",
                "seo": "wa/kennewick/99336/great-floors/49368/carpet",
                "sem": "wa/kennewick/99336/great-floors/49368/carpet/sale"
            },
            {
                "urlId": 58682,
                "locationId": 105893,
                "type": "coupon",
                "seo": "wa/kennewick/99336/great-floors/49368/flooring",
                "sem": "wa/kennewick/99336/great-floors/49368/flooring/sale"
            },
            {
                "urlId": 84322,
                "locationId": 105893,
                "type": "store",
                "seo": "wa/kennewick/99336/great-floors/49368/carpet",
                "sem": null
            },
            {
                "urlId": 58684,
                "locationId": 105893,
                "type": "waterproof",
                "seo": "wa/kennewick/99336/great-floors/49368/waterproof-flooring",
                "sem": "wa/kennewick/99336/great-floors/49368/waterproof-flooring/sale"
            }
        ],
        "hours": null,
        "products": [
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "false",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": false
    },
    {
        "locationId": 105147,
        "retailerId": 23317,
        "locationCode": "24353",
        "retailerCode": null,
        "manufacturerCode": "shaw",
        "type": "store",
        "name": "GREAT FLOORS LLC",
        "address1": "1508 E Nob Hill Blvd",
        "address2": null,
        "city": "Yakima",
        "state": "WA",
        "country": "US",
        "postalCode": "98901-3657",
        "phone": "509-575-1741",
        "forwardingPhone": "509-902-6177",
        "email": null,
        "licenseNumber": null,
        "url": "http://www.greatfloors.com/",
        "locationLogo": null,
        "adPackage": null,
        "about": null,
        "lat": 46.58476,
        "lng": -120.4811221,
        "active": true,
        "sale": true,
        "paidSearch": true,
        "urls": [
            {
                "urlId": 39410,
                "locationId": 105147,
                "type": "carpet",
                "seo": "wa/yakima/98901/great-floors-llc/24353/carpet",
                "sem": "wa/yakima/98901/great-floors-llc/24353/carpet/sale"
            },
            {
                "urlId": 39409,
                "locationId": 105147,
                "type": "coupon",
                "seo": "wa/yakima/98901/great-floors-llc/24353/flooring-all",
                "sem": "wa/yakima/98901/great-floors-llc/24353/flooring-all/sale"
            },
            {
                "urlId": 45516,
                "locationId": 105147,
                "type": "store",
                "seo": "wa/yakima/98901/great-floors-llc/24353/carpet",
                "sem": null
            },
            {
                "urlId": 39411,
                "locationId": 105147,
                "type": "waterproof",
                "seo": "wa/yakima/98901/great-floors-llc/24353/waterproof-coretec",
                "sem": "wa/yakima/98901/great-floors-llc/24353/waterproof-coretec/sale"
            }
        ],
        "hours": null,
        "products": [
            "atCarpet",
            "atHardwood",
            "coretecVinyl",
            "pcommCarpet",
            "pcommVinyl",
            "shawCarpet",
            "shawHardwood",
            "shawTile",
            "shawVinyl"
        ],
        "contacts": null,
        "attributes": {
            "ansoPremier": "true",
            "aligned": "true",
            "homeCenter": "false",
            "shawFlooringGalleries": "false",
            "designCenter": "false"
        },
        "totalBudget": 100000,
        "searchBudget": 60000,
        "socialBudget": 40000,
        "coretec": true
    }]'; */
    
/* 
    $sale_templates = array(

        'flooring'=>'template-retailer-flooring-sale.php',
        'flooring all'=>'template-all-retailer-flooring-sale.php',
        'carpet' => 'template-carpet-sale.php',
        'waterproof flooring' => 'template-waterproof-flooring-sale.php',
        'waterproof coretec'=>'template-waterproof-coretec-sale.php'
        
    );

    $flooring_templates = array(
        'flooring'=>'template-retailer-flooring.php',
        'flooring all'=>'template-retailer-flooring.php',
        'carpet' => 'template-carpet.php',
        'waterproof flooring' => 'template-waterproof-flooring.php',
        'waterproof coretec'=>'template-waterproof-flooring.php'
    ); */
    $retailer_data = json_decode($input);
  //  echo "<pre>";
   // var_dump($retailer_data);exit;

    //$retailer_data = $wpdb->get_results( "SELECT * FROM coupon_sale where id =900" );

    echo "in<pre>";

    //
    foreach($retailer_data as $data){
        
        if($data->active){
            echo "HERER";
            createPages($data);
            //update address
            updateMetaData($data->locationCode,'wpsl_address',$data->address1." ".$data->address2);
            //update address
            updateMetaData($data->locationCode,'wpsl_retailer_forwardingphnumber',$data->forwardingPhone);
           
        }
        else{
            //add all pages in draft
        }
    }
        
   
    
}

function createPages($data){

    
    $state_details =  check_page_exists_by_title($data->state);
    
    if($state_details == FALSE){
         $state_details = create_retailer_page($data->state ,  'template-state.php' );
    }

    $state_page_id  = $state_details['id'];
    $state_page_url  = $state_details['url'];

    $city_details = check_page_exists_by_title($data->city , $state_page_id );

    if($city_details == FALSE){
         $city_details= create_retailer_page($data->city ,  'template-city.php' ,$state_page_id ,   $state_page_url);
    }
   
    $city_page_id  = $city_details['id'];
    $city_page_url  = $city_details['url'];

    $zip = $data->postalCode;
    if(strstr($data->postalCode,'-')){
        $zip = explode('-',$data->postalCode);
        $zip = $zip[0];
    }

    //echo $zip; echo $city_page_id;exit;
    
    $zip_details = check_page_exists_by_title($zip , $city_page_id);
    
    
    if($zip_details == FALSE){
         $zip_details= create_retailer_page($zip,  'template-zipcode.php' ,$city_page_id ,   $city_page_url);
    }

    $zip_page_id  = $zip_details['id'];
    $zip_page_url  = $zip_details['url'];

    var_dump($data->name , $zip_page_id );
    $updateslug = (explode("/",$data->urls[0]['seo']));
    $store_details = check_page_exists_by_title($data->name , $zip_page_id,false,true,$updateslug[3] );
    

     
    if($store_details == FALSE){
         $store_details= create_retailer_page($data->name,  'single-stores-contact.php' ,$zip_page_id ,  $zip_page_url);
    }

    $store_page_id  = $store_details['id'];
    $store_page_url  = $store_details['url'];


    $location_details = check_page_exists_by_title($data->locationCode , $store_page_id );
     
    if($location_details == FALSE){
         $location_details= create_retailer_page($data->locationCode,  'template-location-id.php' ,$store_page_id ,  $store_page_url);
    }

    $location_page_id  = $location_details['id'];
    $location_page_url  = $location_details['url'];

    $sale_templates = array(

        'flooring'=>'template-retailer-flooring-sale.php',
        'flooring all'=>'template-all-retailer-flooring-sale.php',
        'carpet' => 'template-carpet-sale.php',
        'waterproof flooring' => 'template-waterproof-flooring-sale.php',
        'waterproof coretec'=>'template-waterproof-coretec-sale.php'
        
    );

    $flooring_templates = array(
        'flooring'=>'template-retailer-flooring.php',
        'flooring all'=>'template-retailer-flooring.php',
        'carpet' => 'template-carpet.php',
        'waterproof flooring' => 'template-waterproof-flooring.php',
        'waterproof coretec'=>'template-waterproof-flooring.php'
    );
    
    
    //wpsl-retailer_coretecVinyl
    
    
    if($data->sale){
        
        updateMetaData($data->locationCode,'wpsl_retailer_sale',$data->sale);
    }
    if($data->paidSearch){
        
        updateMetaData($data->locationCode,'wpsl_retailer_paid_search',$data->paidSearch);
    }

    //SEO Pages
    foreach($data->urls as $url){
        //var_dump($url);exit;
        $url = (object)$url;
        if($url->type == "carpet"){
            updateMetaData($data->locationCode,'wpsl_retailer_carpeturl',$url->seo);
        }
        else if($url->type == "coupon"){
            updateMetaData($data->locationCode,'wpsl_retailer_couponSeoUrl',$url->seo);
            if( in_array("coretecVinyl",$data->products)){
                updateMetaData($data->locationCode,'wpsl_retailer_coretecVinyl',"1");
                echo "<br>";
                $storelink = str_replace('flooring-all',"",$url->seo);
                updateMetaData($data->locationCode,'wpsl_retailer_storeurl',$storelink);
                
                echo "<br>";
                    
            }
            else{
                $storelink = str_replace('flooring',"",$url->seo);
                updateMetaData($data->locationCode,'wpsl_retailer_storeurl',$storelink);
                    
            }
            //wpsl_retailer_storeurl
        }
        else if($url->type == "waterproof"){
            updateMetaData($data->locationCode,'wpsl_retailer_waterproofurl',$url->seo);
            //wpsl_retailer_storeurl
        }
        
        


        if(in_array($url->type,array('carpet','coupon','waterproof'))){
            if($data->coretec){
                if($url->type == "coupon"){
                    $url->type = "flooring all";
                      
                }
                if($url->type == "waterproof"){
                    $url->type = "Waterproof coretec";
                }
            }else{
                if($url->type == "coupon"){
                    $url->type = "flooring";
                }
                if($url->type == "waterproof"){
                    $url->type = "Waterproof flooring";
                }

            }
            

            $couponSeoUrl_details = check_page_exists_by_title($url->type , $location_page_id );
            
            if($couponSeoUrl_details == FALSE){
                $couponSeoUrl_details= create_retailer_page($url->type, $flooring_templates[strtolower($url->type)]  ,$location_page_id ,  $location_page_url);
               
           }
           $couponSeoUrl_page_id  = $couponSeoUrl_details['id'];
            $couponSeoUrl_page_url  = $couponSeoUrl_details['url'];

            if($url->sem != null || $url->sem != "")
            {
                $couponSemUrl_details = check_page_exists_by_title('sale' , $couponSeoUrl_page_id );
                if($couponSemUrl_details == FALSE){
                    $couponSemUrl_details= create_retailer_page('sale', $sale_templates[strtolower($url->type)]  ,$couponSeoUrl_page_id ,  $couponSeoUrl_page_url);
               }
       
               $couponSemUrl_page_id  = $couponSemUrl_details['id'];
               $couponSemUrl_page_url  = $couponSemUrl_details['url'];
            }
            else{
                //This for remove pages
                echo "HERE";
                $couponSemUrl_details = check_page_exists_by_title('sale' , $couponSeoUrl_page_id ,true);
            }
        }
    }
//flooring SEO
     echo "TEH END";
}

function check_page_exists_by_title($title , $parent = "",$isremove=false,$update_slug=false,$post_name=""){
    global $wpdb;
     
    $sql = ' SELECT * FROM wp_posts WHERE post_title ="' .$title. '" AND post_type = "page" ';
    
    if($parent !=""){
        $sql .= " AND post_parent =".$parent ;
    } 
    
    $sql .=" LIMIT 1 ";
    $page = $wpdb->get_results( $sql );
    echo $sql;
    if($isremove) {
        echo $page[0]->ID;
        $post = array( 'ID' => $page[0]->ID, 'post_status' => 'draft');
       // wp_update_post($post);
        global $wpdb;
        $insert = "UPDATE wp_posts set post_status = 'draft' WHERE ID =".$page[0]->ID;
        
        $wpdb->query( $insert);
        
    }              
    else if($page[0]->post_status != 'publish'){
        $post = array( 'ID' => $page[0]->ID, 'post_status' => 'publish');
        //wp_update_post($post);
        global $wpdb;
        $insert = "UPDATE wp_posts set post_status = 'publish' WHERE ID =".$page[0]->ID;
        $wpdb->query( $insert);
    
    }
    else  if($update_slug == true && $post_name !=""){
        global $wpdb;
        $insert = 'UPDATE wp_posts set post_name = "'.$post_name.'" WHERE ID ='.$page[0]->ID;
        $wpdb->query( $insert);
        
        
    }
    if($page[0]->ID){
        $data = array('id' => $page[0]->ID , 'url' => $page[0]->guid );
        return $data;
    }else{
        return FALSE;
    }
}

function create_retailer_page($title , $template , $parent='', $parent_url='https://stg.flooringyouwell.com/' ){
   /*  $post_args = array(
        'post_title' => $title,
        'post_status' => 'publish',
        'post_type'   => 'page',
        //'post_name' => sanitize_title($title),
        
        'guid' => $parent_url."".sanitize_title($title)."/"
    ); */

    
    echo sanitize_title($title);
    echo "<br>";
    $post_args = array(
        'post_title' => $title,
        'post_status' => 'publish',
        'post_type'   => 'page',
        'post_name' => sanitize_title($title),
        
        'guid' => $parent_url."".sanitize_title($title)."/"
    );


    if($parent !=''){
        $post_args['post_parent']= $parent;

    }
    $id =  wp_insert_post( $post_args);

    add_post_meta($id , '_wp_page_template' , $template);
    add_post_meta($id , '_wp_is_seo_page' , 'yes');

    echo "<br> inserted post id=".$id."(".$title.") <br>";

    echo sanitize_title($title) ."::". $id ."::". $parent;
    echo "<br>";
   echo  wp_unique_post_slug( sanitize_title($title), $id, 'publish', 'page', $parent ); 
   echo "<br>";
    $post = array( 'ID' => $id, 'post_name' => wp_unique_post_slug( sanitize_title($title), $id, 'publish', 'page', $parent ));
    echo "<br> Updatd post id=".$id."(".$title.") <br>";
    global $wpdb;
    $insert = "UPDATE wp_posts set post_name = '".sanitize_title($title)."' WHERE ID =".$id;
    $wpdb->query( $insert);
    $data = array('id' => $id , 'url' => $post_args['guid']);
    return $data;
}


function getRetailerInfoNEW($input){
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>"test",'client_secret'=>'test');
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
    
    
    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){

        
      
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $location = array(13104,17532,17607,29036,29037,29038,29039,29040,29041,39121,44645,48107,48202,48316,49311,49354,49370,49459,49467,49501,49502,49503,49504,49555,49751,49796,49910,49996,50034,50056,50064,50073,50078,50191,25408,29177,41206,41567,41988,42125,42877,42882,43976,44354,44644,45007,47556,47710,47957,48204,48920,49322,49383,49404,49498,49851,49947,49968,49995,50033,50065,72,874,875,1455,1640,1796,1797,2064,2429,2544,4267,6162,6167,6167,6467,6503,10878,11015,15086,15087,16634,17766,17938,21330,24365,24410,24557,24597,25197,25499,26462,26580,27138,27158,27269,27385,27513,27828,28012,28152,28272,28281,28822,28973,29144,31127,31337);
      //  $location = $input;
        //$location =array(31458,31587,31617,31618,31621,31622,31623,35931,37017,37213,38296,38500,38783,39072,39074,40572,40581,40660,41011,41030,41031,41032,41033,41034,41036,41037,41038,41039,41040,41041,41223,41224,41226,41227,41228,41229,41230,41231,41232,41234,41235,41236,41237,41239,41240,41241,41243,41244,41245,41247,41442,41562,43616,43683,43684,43793,43794,43860,43877,43899,44748,45081,47525,47538,47712,47794,47795,48158,48224,48302,48303,48373,48705,48851,48853,48855,48876,48913,48957,49119,49151,49184,49185,49186,49209,49326,49359,49449,49457,49471,49496,49545,49557,49740,49741,49742,49743,49892,49969,50024,50031);
        
        //$retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/location/","GET",$inputs,$headers);
        //$retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/locations","GET",$inputs,$headers);
        echo "<pre>";
        //print_r($location);
        $i=0;
        foreach($location as $code){
          
            //if($i==1){exit;}
            echo $i." ";
            $data = $apiObj->call("https://leadgen.mm-api.agency/fyw/location/shaw/".$code,"GET",$inputs,$headers);
            if($data['success']){
                print_r($data['result']);
                $data = (object)$data['result'];
                //echo $data->locationCode;exit;
                 if($data->active){
                    echo "HERER";
                    createPages($data);
                    //update address
                    updateMetaData($data->locationCode,'wpsl_address',$data->address1." ".$data->address2);
                    //update address
                    updateMetaData($data->locationCode,'wpsl_retailer_forwardingphnumber',$data->forwardingPhone);
                   
                }
            }
            else{
                echo $code." Blank for API <br>";
            }
            $i++;
        }
        
        
        
    }
}


function getSingleRetailerInfoAPI($retailerid){
      //CALL Authentication API:
      $apiObj = new APICaller;
      $inputs = array('grant_type'=>'client_credentials','client_id'=>"test",'client_secret'=>'test');
      $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
      
      
      if(isset($result['error'])){
          $msg =$result['error'];                
          $_SESSION['error'] = $msg;
          $_SESSION["error_desc"] =$result['error_description'];
          
      }
      else if(isset($result['access_token'])){
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/retailer/".$retailerid,"GET",$inputs,$headers);
        return ($retailers);
      }
      

}
//wpsl_retailer_forwardingphnumber
//wpsl_address

function updateMetaData($locationCode,$key,$value){
    
    global $wpdb;
    $skus = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_id_api' AND meta_value=".$locationCode);
    echo "<br>";
    if(count($skus) > 0){

        $post_id = $skus[0]->post_id;

        $phone_key = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = '".$key."' AND post_id=".$post_id);
        if(count($phone_key) > 0){
            $update = "UPDATE {$wpdb->postmeta} set meta_value='".$value."'   WHERE  meta_key = '".$key."' AND post_id = '".$post_id."'";
           $wpdb->query( $update );
            echo $update."<br>";
            $phoneupdated++;
        }else{

         $insert =  "insert into {$wpdb->postmeta} (post_id,meta_key,meta_value) values (".$post_id.",'".$key."','".$value."') ";    
               echo $insert."<br>";;
           $wpdb->query( $insert );
           $phoneupdated++;
        }

        

    }
    else{
        echo "No recored found ".$locationCode."<br>";    
        $noretainlerfound++;
        
    }
}

if(@$_GET['sale']== 1){
     //insertMissingPages();    
    // getRetailerInfoNEW($input);
    //var_dump(getRetailerInfoNEW());
    
   exit;
 }

 function check_product_group_redirection(){
    global $wpdb;
    $table_redirect_group = $wpdb->prefix.'redirection_groups';
    $datum = $wpdb->get_results("SELECT * FROM $table_redirect_group WHERE name = 'Products'");
    if($wpdb->num_rows > 0) {

        write_log('check_product_group_redirection - Product Group Exists');

    }else{        

        $wpdb->insert($table_redirect_group, array(
            'name' => 'Products', //replaced non-existing variables $lq_name, and $lq_descrip, with the ones we set to collect the data - $name and $description
            'tracking' => '1',
            'module_id' => '1',
            'status' => 'enabled',
            'position' => '2'
            ),
            array( '%s','%s','%s','%s','%s') //replaced %d with %s - I guess that your description field will hold strings not decimals
        );

    }

  }


