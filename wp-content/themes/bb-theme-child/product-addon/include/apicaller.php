<?php



class APICaller{

    function __construct(){
        
    }

    function call($url,$method,$inputs = array(),$headers = array(),$host=BASEURL){
        
        if($method == "POST"){
            $response = wp_remote_post($host.$url,array(
                'method' => $method,
                'timeout' => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'headers' => $headers,// array('Authorization' => 'Basic ' . base64_encode( userna. ':' .pass ),), 
                'blocking' => true,               
                'cookies' => array(),
                'body' =>$inputs
                )
            );
            
        }
        else if($method == "GET"){
           $response = wp_remote_get($url,array(
            'httpversion' => '1.0',
            'timeout' => 45,
            'headers' => $headers,// array('Authorization' => 'Basic ' . base64_encode( userna. ':' .pass ),), 
          
            )
        );
        }

        
        if ( is_wp_error( $response ) ) {
            $error_message = $response->get_error_message();
            echo "Something went wrong: $error_message";
            exit;
 
         } else {
            
            $return = json_decode($response['body'], true);
            return $return;
 
         }
        

    }
    
}