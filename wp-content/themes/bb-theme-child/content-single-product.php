<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$collection = get_field('collection');
$image = swatch_image_product(get_the_ID(),'600','400');	
?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">
	<div class="fl-post-content clearfix grey-back ddfd" itemprop="text">
        <div class="container">
			<div class="product-detail-top">
				<div class="row">
					<div class="col-md-8 col-xs-12 product-swatch">   
						<?php get_template_part('includes/product-swatch-images'); ?>	
					</div>
				</div>
				<div class="col-md-4 col-xs-12 product-box">
					<div class="product-box-inner <?php if($collection == 'Floorte Exquisite' || $collection == 'Floorte Magnificent'){ echo "floorte-inner";}?>">
						<div class="productDiscription">
							<?php get_template_part('includes/product-brand-logos'); ?>
							<div class="descDetails">
								<h3 class="fl-post-title-collection" itemprop="name"><?php the_field('collection'); ?></h3>
								<h1 class="fl-post-title-style" itemprop="name"><?php the_field('style'); ?></h1>
								<h3 class="fl-post-title-color" itemprop="name">Color: <?php the_field('color'); ?></h3>
							</div>
						</div>
						<div class="button-wrapper">
							<a href="/flooring-coupon/" class="button alt">GET COUPON</a>
							<a href="/find-sfn-retailer/"  class="button">FIND A RETAILER</a>
						</div>
					
						<div class="product-gallery">
							<?php get_template_part('includes/product-gallery-images'); ?>	
						</div>    
					</div>
				</div>
			</div>
				<div class="clearfix"></div>
		</div>
			<div class="clearfix"></div>

		<?php get_template_part('includes/product-color-slider');  ?>
			<div class="clearfix"></div>
		
		<?php get_template_part('includes/product-attributes'); ?>
			<div class="clearfix"></div>
	</div>
</article>