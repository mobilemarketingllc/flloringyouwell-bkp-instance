<?php get_header(); ?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
        <h2>FLOORING INSPIRATION VIDEOS </h2>
		<?php
			$videotype = get_field('video_type');

				if($videotype=='Embed'){

					echo '<div class="embed-container">';
					 the_field('video_embed_code');					
					echo '</div>';
					echo '<h3>'.get_the_title().'</h3>';
				}else{

					echo '<video controls poster="'.get_field('video_placeholder_image').'">
								<source src="'.get_field('local_file_source').'" type="video/mp4">.
						</video>';
						echo '<h3>'.get_the_title().'</h3>';
				}

			
		?>

			<?php
				echo do_shortcode('[fl_builder_insert_layout id="1639"]');
			?>
		</div>

		
	</div>
</div>

<?php get_footer(); ?>
