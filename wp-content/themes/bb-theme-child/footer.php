<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>

	<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>

	<div class="header_store_listing hidden"><?php  echo do_shortcode('[storelisting]'); ?></div>

	
</div><!-- .fl-page -->
<script>
jQuery(window).load(function() {
    jQuery('.parentLocator').on( "click", ">div" ,function(){
        jQuery(this).siblings().removeClass('activeMarker');
		jQuery(this).addClass('activeMarker');
		// $(this).next('img').attr('src','/wp-content/uploads/2019/04/purple_my_marker@2x.png')
        // jQuery(this).siblings().next('img').attr('src','/wp-content/uploads/2019/04/my_marker@2x.png')
	})
})
jQuery(document).ready(function(){
	jQuery('.toggle-image-thumbnails .toggle-image-holder a').click(function(e) {
		var url = jQuery(this).attr("data-pinurl");
		var getpinHref = jQuery('.product-swatch a[data-pin-href]').data('pin-href');
		if(getpinHref=="" || getpinHref=="undefined" || getpinHref==undefined){ } else{
			getpinHref = getpinHref.split('&media=');
			var getpinHrefFinal = getpinHref[1].split('&description=');
			jQuery('.product-swatch a[data-pin-href]').attr('data-pin-href', getpinHref[0]+'&media=https://'+url+'&description='+getpinHrefFinal[1]);
		}
    });
});
</script>
<?php



do_action( 'fl_body_close' );

FLTheme::footer_code();


// for($i=0;$testarray.count();$i++){

// 	update_post_meta($testarray[$i], '_yoast_wpseo_meta-robots-noindex',      '1' );
// 	update_post_meta( $testarray[$i], '_yoast_wpseo_meta-robots-nofollow', '1' );

// }
// global $wpdb;
// $queryPost= "SELECT post_id FROM wp_postmeta WHERE meta_value = 'single-stores-contact.php'";

// $queryResults = $wpdb->get_results(  $queryPost ); 

// //print_r($queryResults);
	   
// $queryfinal = array();

// foreach( $queryResults as $res ) {
	 
//  $queryfinal[]= $res->id;

//  	update_post_meta($res->post_id, '_yoast_wpseo_meta-robots-noindex','0' );
// 	 update_post_meta( $res->post_id, '_yoast_wpseo_meta-robots-nofollow', '0' );
// 	 //echo 'updated';

// }

wp_footer();
//print_r($_SERVER);

wp_delete_post(92389, true); 
?>
</body>
</html>
