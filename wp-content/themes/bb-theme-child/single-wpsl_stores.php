<?php get_header(); ?>

<?php
	global $post;

	$queried_object = get_queried_object();

		$address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
		$city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
		$state          = get_post_meta( $queried_object->ID, 'wpsl_state', true );
		$phone       = get_post_meta( $queried_object->ID, 'wpsl_phone', true );
		$url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
		$destination   = $address . ',' . $city . ',' . $country;
		$direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";

	?>

<div class="container fl-content-full">

<h1 class="entry-title sfnstoretitle"><?php single_post_title(); ?> in <?php echo $city;?>, <?php echo $state;  ?></h1>   

	<div class="row">	

	<div class="fl-sidebar fl-sidebar-left col-md-4">
        <ul class="sfnstoreinfo">
	      <li><div class="sfnstoreadd"><i class="fas fa-map-marker"></i><?php echo $address;?></div></li>
		  <li><a href="<?php echo $direction_url; ?>" class="getdirect"> GET DIRECTIONS</a></li>
		  <li><div class="sfnstoreph"><i class="fa fa-phone" aria-hidden="true"></i><?php echo $phone;?></div></li>
		  <li><div class="sfnstoreweb"><i class="fa fa-globe" aria-hidden="true"></i><a href="<?php echo $url;?>"><?php echo $url;?></a></div></li>

		  <li><a href="#" class="getdirect"> APPLY FOR FINANCING</a></li>
		  <li><a href="#" class="getdirect"> ASK OUR EXPERTS</a></li>
		  <li><a href="#" class="button"> GET COUPON</a></li>
		  <li><a href="#" class="sfn-button-black"> CONTACT STORE</a></li>
		</ul>
         
		 <div class="storehours">
         <h4>Store Hours</h4>
		 <?php echo do_shortcode( '[wpsl_hours]' ); 
		//  [wpsl_address id="'.$queried_object->ID.'" name="true" address="true" address2="false" 
		//  city="true" state="false" zip="true" country="true" phone="true" 
		//  fax="false" email="true" url="true"]
		 ?>

		 </div>

    </div>

		<div class="fl-content fl-content-right col-md-8">
			<?php
			
                    
			// Add the map shortcode
			echo do_shortcode( '[wpsl_map]' );
			
			// Add the content
			$post = get_post( $queried_object->ID );
			setup_postdata( $post );
			
			wp_reset_postdata( $post );
			
			

			?>
		</div>		
		<div class="row fl-row fl-row-full-width">
		<div class="fl-content col-md-12 ">

		<div class="storedesc">

		    <h1 class="entry-title sfnstoretitle">About <?php single_post_title(); ?></h1> 

			<?php echo the_content();?></div>
		</div>

			<?php echo do_shortcode('[fl_builder_insert_layout id="1451"]'); ?>

			<?php echo do_shortcode('[fl_builder_insert_layout id="1450"]'); ?>

		</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
