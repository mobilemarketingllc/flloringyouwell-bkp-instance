<?php if(is_active_sidebar('footer-sale-sidebar')) : ?>
<div class="footer-top-sidebar">
	<?php dynamic_sidebar('footer-sale-sidebar'); ?>
</div>
<?php endif; ?>
<div class="fl-page-footer-widgets">
	<div class="fl-page-footer-widgets-container container">
		<div class="fl-page-footer-widgets-row row">
		<?php FLTheme::display_footer_widgets(); ?>
		</div>
	</div>
</div><!-- .fl-page-footer-widgets -->
