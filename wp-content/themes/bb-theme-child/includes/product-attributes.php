<?php  $collection = get_field('collection');?>
<div class="product-attributes-section">
    <div class="row">
        <div class="col-md-6 product-video">
           
            <?php //if(get_field('video_url')) { ?>
                <!-- <iframe width="560" height="315" src="<?php the_field('video_url'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
            <?php //} else { ?>
                <?php
                // check if the repeater field has rows of data

                if($collection == 'Floorte Exquisite' || $collection == 'Floorte Magnificent'){ ?>

                     <h3 class="floorte-prohead">Floorté Waterproof Hardwoods make life easy!</h5>
                     <h6 class="floorte-subprohead">By combining genuine hardwood with innovative waterproof advancements, Shaw has created the ultimate flooring. Durable, waterproof, and easy to clean, Floorté Hardwood gives you the high performance and high style you’ve always desired.</h6>

             <?php   }

                if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { 

                ?>

<iframe src="https://player.vimeo.com/video/348663860" width="547" height="307" frameborder="0" title="Coretec Waterproof flooring" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                <?php

                }elseif($collection == 'Floorte Exquisite' || $collection == 'Floorte Magnificent') { 

                    ?>
    
        <iframe src="https://www.youtube.com/embed/onBjhDQR-Xk?rel=0"  width="547" height="307" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
                    <?php
    
                }else{
                if(get_field('gallery_room_images')){
                    $gallery_images = get_field('gallery_room_images');
                    $gallery_img = explode("|",$gallery_images);
                    // loop through the rows of data
                    foreach($gallery_img as  $key=>$value) {

                        $room_image =  high_gallery_images_in_loop($value);
                        $room_image_thumb =  thumb_gallery_images_in_loop($value);
                ?>
                    <img src="<?php echo $room_image; ?>" alt="<?php the_title_attribute(); ?>" title="<?php the_title_attribute(); ?>" class="product-att-gallery-img" />
                <?php
                    break;
                    }
                }
            }
                ?>
            <?php //} ?>
        </div>
        <div class="col-md-6">
            <div class="product-attributes">
                <h3>Product Attributes</h3>
                <table class="table">
                <tbody>
                        <?php if(get_field('parent_collection')) { ?>
                        <tr>
                            <th scope="row">Main Collection</th>
                            <td><?php the_field('parent_collection'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('collection')) { ?>
                        <tr>
                            <th scope="row">Collection</th>
                            <td><?php the_field('collection'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('sku')) { ?>
                        <tr>
                            <th scope="row">SKU</th>
                            <td><?php the_field('sku'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('plank_dimensions')) { ?>
                        <tr>
                            <th scope="row">Plank Dimensions</th>
                            <td><?php the_field('plank_dimensions'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('flooring_type')) { ?>
                        <tr>
                            <th scope="row">Flooring Type</th>
                            <td><?php the_field('flooring_type'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('edge_profile')) { ?>
                        <tr>
                            <th scope="row">Edge Profile</th>
                            <td><?php the_field('edge_profile'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('color')) { ?>
                        <tr>
                            <th scope="row">Color</th>
                            <td><?php the_field('color'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('construction')) { ?>
                        <tr>
                            <th scope="row">Construction</th>
                            <td><?php the_field('construction'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('color_variation')) { ?>
                        <tr>
                            <th scope="row">Color Variation</th>
                            <td><?php the_field('color_variation'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('shade') && $collection != 'Coretec Colorwall') { ?>
                        <tr>
                            <th scope="row">Shade</th>
                            <td><?php the_field('shade'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('core')) { ?>
                        <tr>
                            <th scope="row">Core</th>
                            <td><?php the_field('core'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('shape')) { ?>
                        <tr>
                            <th scope="row">Shape</th>
                            <td><?php the_field('shape'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('species')) { ?>
                        <tr>
                            <th scope="row">Species</th>
                            <td><?php the_field('species'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('fiber_type')) { ?>
                        <tr>
                            <th scope="row">Fiber Type</th>
                            <td><?php the_field('fiber_type'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('surface_type')) { ?>
                        <tr>
                            <th scope="row">Surface Type</th>
                            <td><?php the_field('surface_type'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('surface_texture_facet')) { ?>
                        <tr>
                            <th scope="row">Surface Texture</th>
                            <td><?php the_field('surface_texture_facet'); ?></td>
                        </tr>
                        <?php } ?>
                        <?php if(get_field('style')) { ?>
                        <tr>
                            <th scope="row">Style</th>
                            <td><?php the_field('style'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('edge')) { ?>
                        <tr>
                            <th scope="row">Edge</th>
                            <td><?php the_field('edge'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('application')) { ?>
                        <tr>
                            <th scope="row">Application</th>
                            <td><?php the_field('application'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('size')) { ?>
                        <tr>
                            <th scope="row">Size</th>
                            <td><?php the_field('size'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('width')) { ?>
                        <tr>
                            <th scope="row">Width</th>
                            <td><?php the_field('width'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('length')) { ?>
                        <tr>
                            <th scope="row">Length</th>
                            <td><?php the_field('length'); ?></td>
                        </tr>
                        <?php } ?>
                    
                        <?php if(get_field('thickness')) { ?>
                        <tr>
                            <th scope="row">Thickness</th>
                            <td><?php the_field('thickness'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('attached_underlayment')) { ?>
                        <tr>
                            <th scope="row">Attached Underlayment</th>
                            <td><?php the_field('attached_underlayment'); ?></td>
                        </tr>
                        <?php } ?>
        
                        <?php if(get_field('installation')) { ?>
                        <tr>
                            <th scope="row">Installation Method</th>
                            <td><?php the_field('installation'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('installation_method')) { ?>
                        <tr>
                            <th scope="row">Installation Method</th>
                            <td><?php the_field('installation_method'); ?></td>
                        </tr>
                        <?php } ?>

                        <?php if(get_field('installation_level')) { ?>
                        <tr>
                            <th scope="row">Installation Level</th>
                            <td><?php the_field('installation_level'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('location')) { ?>
                        <tr>
                            <th scope="row">Location</th>
                            <td><?php the_field('location'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('backing_facet')) { ?>
                        <tr>
                            <th scope="row">Backing</th>
                            <td><?php the_field('backing_facet'); ?></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if(get_field('look')) { ?>
                        <tr>
                            <th scope="row">Look</th>
                            <td><?php the_field('look'); ?></td>
                        </tr>
                        <?php } ?>
                        
                       

                    </tbody>
                </table>
            </div>
        </div>
            <div class="clearfix"></div>
    </div>
</div>