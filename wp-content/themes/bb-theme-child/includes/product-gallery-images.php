<?php
    $image = swatch_image_product(get_the_ID(),'600','400');	
?>
    <div class="toggle-image-thumbnails">
        <?php if (!empty($image)){ ?>
            <div class="toggle-image-holder"><a href="javascript:void(0)" class="product_gallery_img active" data-background="<?php echo $image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a></div>
        <?php } ?>
        <?php
        // check if the repeater field has rows of data
        if(get_field('gallery_room_images')){
            $gallery_images = get_field('gallery_room_images');   
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {

                $room_image =  high_gallery_images_in_loop($value);
                $room_image_small =  thumb_gallery_images_in_loop($value);
        ?>
            <div class="toggle-image-holder">
                <a class="product_gallery_img" href="javascript:void(0)" data-background="<?php echo $room_image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $room_image_small; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
            </div>
        <?php
            }
          }
        ?>
    </div>