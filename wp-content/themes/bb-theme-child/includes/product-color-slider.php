<?php
    global $post;
    $flooringtype = $post->post_type;
    $collection = get_field('collection');
    $familysku = get_post_meta($post->ID, 'collection', true);
    $familycolor = get_post_meta($post->ID, 'style', true);  

    if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { 
    $args = array(
        'post_type'      => $flooringtype,
        'posts_per_page' => -1,
        'post_status'    => 'publish',
        'meta_query'     => array(
            array(
                'key'     => 'style',
                'value'   => $familycolor,
                'compare' => '='
            ),
            array(
                'key' => 'swatch_image_link',
                'value' => '',
                'compare' => '!='
                )
        )
    ); 
    }else{

        $args = array(
            'post_type'      => $flooringtype,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => 'collection',
                    'value'   => $familysku,
                    'compare' => '='
                ),
                array(
                    'key' => 'swatch_image_link',
                    'value' => '',
                    'compare' => '!='
                    )
            )
        );

    }
    $the_query = new WP_Query($args);
    if($the_query->have_posts()) { 
?>
<div class="product-variations">
    <h3><?php echo $the_query->found_posts; ?> Colors</h3>
    <div class="color_variations_slider">
        <div class="slides">
            <?php
                while ($the_query->have_posts()) {
                $the_query->the_post();
            ?>
            <div class="slide col-md-2 col-sm-3 col-xs-6 color-box">
                <a href="<?php the_permalink(); ?>">
                    <?php
                    if (get_field('swatch_image_link')) {
                        $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                    } ?>
                    <img src="<?php echo $image; ?>" style="padding: 5px;" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                </a>
                <br />
                <small><?php the_field('color'); ?></small>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
</div>
<?php
    }
    wp_reset_postdata();
?> 