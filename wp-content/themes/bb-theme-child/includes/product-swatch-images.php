<?php
    $collection = get_field('collection');
?>
<div id="product-images-holder" <?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' || $collection == 'Floorte Magnificent') { ?>class="colorwall-exlusive-batch"<?php } ?>>
    <?php
       $image = swatch_image_product(get_the_ID(),'600','400');	

        if (!empty($image)){
    ?>
    <div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');" data-responsive="<?php echo $image; ?>" data-src="<?php echo $image; ?>">
        <a href="javascript:void(0)" class="popup-overlay-link" data-src="<?php echo $image; ?>"></a>
        <span class="main-imgs"><img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
    </div>
    <?php } else{ ?>
    <div class="img-responsive toggle-image" style="background-image:url('http://placehold.it/168x123?text=COMING+SOON');background-size: cover;background-position:center">
        <a href="http://placehold.it/168x123?text=COMING+SOON" class="popup-overlay-link"></a>
        <span class="main-imgs"><img src="http://placehold.it/168x123?text=COMING+SOON" class="img-responsive" alt="<?php the_title_attribute(); ?>" /></span>
    </div>
    <?php } ?>
    <?php /*if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {  ?>
    <span class="exlusive-badge"><img src="https://staging.flooringyouwell.com/wp-content/uploads/2019/07/exclusive-products-icon-copy.png" alt="<?php the_title(); ?>" /></span>
    <?php }*/ ?> 
    <?php
        // check if the repeater field has rows of data
        if(get_field('gallery_room_images')){
            $gallery_images = get_field('gallery_room_images');           
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {

                $room_image =  high_gallery_images_in_loop($value);
                $room_image_thumb =  thumb_gallery_images_in_loop($value);
        ?>
            <div class="popup-imgs-holder hidden" data-responsive="<?php echo $room_image; ?>" data-src="<?php echo $room_image; ?>"><a href="javascript:void(0)" title="<?php the_title_attribute(); ?>"><img src="<?php echo $room_image; ?>" alt="<?php the_title_attribute(); ?>" /></a></div>
        <?php
            }
        }
        ?>