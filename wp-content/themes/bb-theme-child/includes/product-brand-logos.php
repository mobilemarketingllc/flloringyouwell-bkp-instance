<?php
$collection = get_field('collection');
	if (get_field('brand_facet') == 'Shaw Floors') { 
		if($collection == 'Floorte Exquisite' || $collection == 'Floorte Magnificent'){	?>
		    <span class="floorte_brand">SHAW FLOORS</span>
			<img class="floorte_brandlogo" src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/Floorte-and-WaterproofHW-Logo.png" alt="floorte" class="product-logo" />
	 <?php }else{
		?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/shaw_logo.png" alt="Shaw Floors" class="product-logo" />
<?php }} elseif (get_field('brand_facet') == 'Anderson Tuftex') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/anderson_tuftex_logo.png" alt="Anderson Tuftex" class="product-logo" />
<?php } elseif (get_field('brand_facet') == 'Karastan'){ ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/karastan_logo.png" alt="Karastan" class="product-logo" />
<?php }elseif (get_field('brand_facet') == 'Armstrong') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/armstrong_logo.png" alt="Armstrong" class="product-logo" />
<?php } elseif (get_field('brand_facet') == 'Dream Weaver') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/dreamweaver_logo.png" alt="Dream Weaver" class="product-logo" />
<?php }elseif (get_field('brand_facet') == 'Philadelphia Commercial') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/philadelphia_commercial_logo.png" alt="Philadelphia Commercial" class="product-logo" />
<?php }elseif (get_field('brand_facet') == 'COREtec') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/coretec_logo.png" alt="COREtec" class="product-logo" />
<?php }  elseif (get_field('brand_facet') == 'Daltile') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/daltile_logo.png" alt="Daltile" class="product-logo" />
<?php } elseif (get_field('brand_facet') == 'Floorscapes') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/floorscapes.jpg" alt="Floorscapes" class="product-logo" />
<?php }  elseif (get_field('brand_facet') == 'Mohawk') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/mohawk_logo.png" alt="Mohawk" class="product-logo" />
<?php } elseif (get_field('brand_facet') == 'Bruce') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/bruce_logo.png" alt="Bruce" class="product-logo" />
<?php } elseif (get_field('brand_facet') == 'Mannington') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/brand_logo/mannington_logo.png" alt="Mannington" class="product-logo" />
<?php }  else { ?> 
    <?php the_field('brand_facet'); ?>
<?php } ?>