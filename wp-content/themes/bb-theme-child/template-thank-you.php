<?php

/*
Template Name: Flooring Coupon Thank You 

*/

//Retailer Landing Page thank you after coupon submitted

get_header(); ?>

<?php
  if(isset( $_GET['post_id'])){
      $post   = get_post( $_GET['post_id'] );
      
  }
  global $post;
 
  $sale = get_post_meta($post->ID,'wpsl_retailer_sale')[0];
  $paid_search = get_post_meta($post->ID,'wpsl_retailer_paid_search')[0];
  $coretecVinyl = get_post_meta($post->ID,'wpsl_retailer_coretecVinyl')[0];
  

  if($sale =="" && $paid_search =="" && $coretecVinyl ==""){
      echo do_shortcode('[fl_builder_insert_layout slug="all-flooring-no-coretec-coupon-thank-you"]');
  }else if($sale =="" && $paid_search =="" && $coretecVinyl =="1"){
      echo do_shortcode('[fl_builder_insert_layout slug="flooring-all-coupon-thank-you"]');
  }else if($sale =="1" && $paid_search =="" && $coretecVinyl ==""){
      echo do_shortcode('[fl_builder_insert_layout slug="all-flooring-no-coretec-coupon-thank-you"]');
  }else if($sale =="1" && $paid_search =="" && $coretecVinyl =="1"){
      echo do_shortcode('[fl_builder_insert_layout slug="flooring-all-coupon-thank-you"]');
  }else if($sale =="1" && $paid_search =="1" && $coretecVinyl ==""){
      echo do_shortcode('[fl_builder_insert_layout slug="all-flooring-no-coretec-coupon-thank-you"]');
  }else if($sale =="1" && $paid_search =="1" && $coretecVinyl =="1"){
      echo do_shortcode('[fl_builder_insert_layout slug="flooring-all-coupon-thank-you"]');
  }
  ?>

			


<?php get_footer(); ?>
 